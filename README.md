直接以dockerfile创建独立镜像

### npm

```shell
# 创建TopService镜像
docker build -t topservice:1.0 .
# 创建TopService容器,对外暴露8044端口
#docker run -d --name=topservice -p 8044:80  topservice:1.0
#添加ENV环境变量,设定AGENT环境路径
docker run -itd --name=topservice -p 8044:80 --env PROXY_IP=10.0.10.105 -e PROXY_HOST=8090 topservice:1.0

# 查看
docker images
docker ps -a
```
