FROM nginx:perl
MAINTAINER evan.shi@580440.com

ENV PROXY_IP 10.0.10.105;
ENV PROXY_HOST 8090;

RUN env
COPY topservice/ /usr/share/nginx/html/   
#COPY nginx.conf /etc/nginx/
COPY conf.d/default.conf /etc/nginx/conf.d/
#REPLACE CONF 替换配置文件
RUN rm /etc/nginx/nginx.conf
ADD nginx.conf /etc/nginx/

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]