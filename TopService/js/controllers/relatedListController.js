angular.module("n580440").controller("relatedListCtrl", ["$scope", "$http", "$timeout", "$modal", "ngProgress", "$filter", "$sce", "$state", "$stateParams",
  function($scope, $http, $timeout, $modal, ngProgress, $filter, $sce, $state, $stateParams) {
    ngProgress.start();
    $scope.dataList = [];
    let today = new Date();
    $scope.list = [];
    $scope.Page = {
      currentPage: 1,
      totalItems: 0,
      PaginationisShow: false
    };
    $scope.isNoDetail = false;
    $scope.SearchParams = {
      Filtration: ''
    };
    //获取关联企业列表
    $scope.getContractedList = function(currentPage) {
      $scope.Page.currentPage = currentPage;
      $http({
        url: `${ApiMapper.ccApi}/g/${$scope.userInfo.apiType}contracted/${currentPage}`,
        method: "POST",
        contentType: "application/json",
        headers: {
          Passport: $scope.userInfo.Passport
        },
        data: JSON.stringify($scope.SearchParams)
      }).success(function(res) {
        if (res[2] > 0) {
          $scope.Page.totalItems = res[2];
          $scope.Page.PaginationisShow = res[0] > 1 ? true : false;
          $scope.dataList = res[1];
          $scope.isNoDetail = false;
        } else {
          $scope.isNoDetail = true;
        }
        ngProgress.complete();
      }).error(res => {
        ngProgress.complete();
      });
      ngProgress.complete();
    };
    //view绑定变量，html标签支援
    $scope.deliberatelyTrustDangerousSnippet = function(snippet) {
      return $sce.trustAsHtml(snippet);
    };

    //接收父控制器广播，刷新列表
    $scope.$on("reloadList", function (event) { 
      $scope.getContractedList(1);
    });
    $scope.getContractedList(1);
    //获取关联企业负责设备列表
    $scope.getEquipList = function(v, company) {
      if (typeof company.equips == "undefined" && v) {
        var params = {
          companyId: company.CompanyId
        };
        if ($scope.userInfo.SigninType == 2) {
          params = {
            MaintainerId: company.MaintainerId
          };
        }
        company.equips = [];
        //面板展开获取设备列表
        $http({
          url: `${ApiMapper.ccApi}/g/${$scope.userInfo.apiType}contracted/equipments/${$scope.Page.currentPage}`,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: $scope.userInfo.Passport
          },
          data: JSON.stringify(params)
        }).success(function(res) {
          if (res[2] > 0) {
            company.equips = res[1];
          }
        }).error(res => {});
      }
    };
    //新增关联企业
    $scope.relatedAdd = function(item) {
      var modalInstance = $modal.open({
        keyboard: false,
        backdrop: "static",
        templateUrl: "RelatedCompanyAdd.html",
        size: "l",
        controller: "relatedCompanyAddCtrl",
        resolve: {
          ParamsItem: () => {
            return item;
          }
        }
      });
      modalInstance.result.then(res => {
        //调用API
      });
    };
    // 修改关联企业信息
    $scope.relatedModify = item => {
      var modalInstance = $modal.open({
        keyboard: false,
        backdrop: "static",
        templateUrl: "RelatedCompanyAdd.html",
        size: "l",
        controller: "relatedCompanyModifyCtrl",
        resolve: {
          ParamsItem: () => {
            return item;
          }
        }
      });
      modalInstance.result.then(res => {
        //调用API
      });
    };
    $scope.pageChanged = function(e) {
      $scope.Page.currentPage = e;
      $scope.getContractedList($scope.Page.currentPage);
    };
    $scope.openEquidsList = function(id,name){
      window.open("#/related/"+ id +"/" + encodeURIComponent(name));
    }
  }
])
.controller("relatedCompanyAddCtrl", ['$scope', '$http', '$modalInstance', "i18n", "ParamsItem",
  function($scope, $http, $modalInstance, i18n, ParamsItem) {
    $scope.TitleStr = "新增关联企业信息";
    $scope.SigninType = Cookies.get("SigninType");
    var apiType = $scope.SigninType == 3 ? 's' : 'p';
    $scope.isNewCompany = true;
    $scope.Company = {
      CompanyName: '',
      NickName: '',
      Liaison: []
    };
    $scope.Liaisons = {
      Name: '',
      PhoneNo: '',
      Email: ''
    }
    $scope.getCompanys = function() {
      $http({
        url: `${ApiMapper.ccApi}/g/${apiType}company/all`,
        method: "Get",
        contentType: "application/json",
        headers: {
          Passport: Cookies.get("Passport")
        }
      }).success(function(res) {
        $scope.companyList = [];
        _(res).each(function(item) {
          var Item = {
            'id': item.CompanyName,
            'text': item.CompanyName,
            'CompanyId': item.CompanyId,
            'Nickname': item.Nickname,
            'Liaisons': item.Liaisons
          };
          $scope.companyList.push(Item);
        });
        setTimeout(function() {
          $("[name=companyName]").select2({
            createSearchChoice: function(term, data) {
              if ($(data).filter(function() {
                  return this.text.localeCompare(term) === 0;
                }).length === 0) {
                return {
                  id: term,
                  text: term,
                  CompanyId: '',
                  Nickname: '',
                  Liaisons: ''
                };
              }
            },
            data: $scope.companyList
          }).on("change", function(e) {
            var added = e.added;
            if (added.CompanyId != '') {
              $scope.isNewCompany = false;
              $scope.Company = {
                CompanyId: added.CompanyId,
                CompanyName: added.text,
                NickName: added.Nickname,
                Liaison: added.Liaisons
              };
              if (added.Liaisons != null) {
                $scope.Liaisons = {
                  Name: added.Liaisons[0].Name,
                  PhoneNo: added.Liaisons[0].PhoneNo,
                  Email: added.Liaisons[0].Email
                }
              }
              $scope.$apply();
            } else {
              $scope.isNewCompany = true;
              $scope.Company = {
                CompanyId: '',
                CompanyName: added.text,
                NickName: '',
                Liaison: []
              };
              $scope.Liaisons = {
                Name: '',
                PhoneNo: '',
                Email: ''
              }
              $scope.$apply();
            }
          });
          $(".select2-container").css("z-index", 999);
        }, 0);
      }).error(res => {
        toastr["success"](res.Messages, "");
      });
    };
    $scope.getCompanys();
    $scope.submit4Store = function(company) {
      $http({
        url: `${ApiMapper.ccApi}/g/pcompany/99`,
        method: "POST",
        contentType: "application/json",
        headers: {
          Passport: Cookies.get("Passport")
        },
        data: JSON.stringify(company)
      }).success(function(res) {
        if (res.StatusCode == 1) {
          toastr["success"](res.Messages, "");
          $scope.$emit("reloadList");
          $modalInstance.close();
        }
      }).error(res => {
        toastr["error"](res.Messages, "");
      });
    }
    $scope.submit4Provider = function(company) {
      $http({
        url: `${ApiMapper.ccApi}/g/pcontracted/add`,
        method: "POST",
        contentType: "application/json",
        headers: {
          Passport: Cookies.get("Passport")
        },
        data: JSON.stringify(company)
      }).success(function(res) {
        if (res.StatusCode == 1) {
          toastr["success"](res.Messages, "");
          $scope.$emit("reloadList");
          $modalInstance.close();
        }
      }).error(res => {
        toastr["error"](res.Messages, "");
      });
    }
    $scope.ok = function() {
      var companyDate = {};
      if ($scope.SigninType == 2) {
        companyDate = {
          MaintainerId: $scope.Company.CompanyId,
          MaintainerName: $scope.Company.CompanyName,
          MaintainerNickname: $scope.Company.NickName,
          StrongholdName: $scope.Company.CompanyName,
          Liaison: [$scope.Liaisons]
        };
        $scope.submit4Store(companyDate);
      } else {
        companyDate = {
          CompanyId: $scope.Company.CompanyId,
          CompanyName: $scope.Company.CompanyName,
          NickName: $scope.Company.NickName,
          HrCode: $scope.Company.HrCode,
          Liaison: [$scope.Liaisons]
        };
        $scope.submit4Provider(companyDate);
      }
    };
    $scope.cancel = function() {
      $modalInstance.dismiss("cancel");
    };
  }
])
.controller("relatedCompanyModifyCtrl", ["$scope", "$http", "i18n", "ParamsItem",
  function($scope, $http, $modalInstance, i18n, ParamsItem) {
    $scope.TitleStr = "修改关联企业信息";
    $scope.Liaisons = {
      Name: '',
      PhoneNo: '',
      Email: ''
    }
    $scope.ok = function() {
      saveSearchParams();
      //$modalInstance.close();
    };
    $scope.cancel = function() {
      $modalInstance.dismiss("cancel");
    };
  }
])
.controller("relatedCompanyAdd4DockerCtrl", ['$scope', '$http', "i18n", function($scope, $http, $modalInstance, i18n) {
    $scope.TitleStr = "企业注册";
    $scope.SigninType = Cookies.get("SigninType");
    var apiType = $scope.SigninType == 3 ? 's' : 'p';
    $scope.isNewCompany = true;
    $scope.Company = {
      CompanyName: '',
      NickName: '',
      Liaison: []
    };
    $scope.Liaisons = {
      Name: '',
      PhoneNo: '',
      Email: ''
    }
    $scope.getCompanys = function() {
      $http({
        url: `${ApiMapper.ccApi}/g/${apiType}company/all`,
        method: "Get",
        contentType: "application/json",
        headers: {
          Passport: Cookies.get("Passport")
        }
      }).success(function(res) {
        $scope.companyList = [];
        _(res).each(function(item) {
          var Item = {
            'id': item.CompanyName,
            'text': item.CompanyName,
            'CompanyId': item.CompanyId,
            'Nickname': item.Nickname,
            'Liaisons': item.Liaisons
          };
          $scope.companyList.push(Item);
        });
        setTimeout(function() {
          $("[name=companyName]").select2({
            createSearchChoice: function(term, data) {
              if ($(data).filter(function() {
                  return this.text.localeCompare(term) === 0;
                }).length === 0) {
                return {
                  id: term,
                  text: term,
                  CompanyId: '',
                  Nickname: '',
                  Liaisons: ''
                };
              }
            },
            data: $scope.companyList
          }).on("change", function(e) {
            var added = e.added;
            if (added.CompanyId != '') {
              $scope.isNewCompany = false;
              $scope.Company = {
                CompanyId: added.CompanyId,
                CompanyName: added.text,
                NickName: added.Nickname,
                Liaison: added.Liaisons
              };
              if (added.Liaisons != null) {
                $scope.Liaisons = {
                  Name: added.Liaisons[0].Name,
                  PhoneNo: added.Liaisons[0].PhoneNo,
                  Email: added.Liaisons[0].Email
                }
              }
              $scope.$apply();
            } else {
              $scope.isNewCompany = true;
              $scope.Company = {
                CompanyId: '',
                CompanyName: added.text,
                NickName: '',
                Liaison: []
              };
              $scope.Liaisons = {
                Name: '',
                PhoneNo: '',
                Email: ''
              }
              $scope.$apply();
            }
          });
          $(".select2-container").css("z-index", 999);
        }, 0);
      }).error(res => {
        toastr["success"](res.Messages, "");
      });
    };
    $scope.getCompanys();
    $scope.submit4Store = function(company) {
      $http({
        url: `${ApiMapper.ccApi}/g/pcompany/99`,
        method: "POST",
        contentType: "application/json",
        headers: {
          Passport: Cookies.get("Passport")
        },
        data: JSON.stringify(company)
      }).success(function(res) {
        if (res.StatusCode == 1) {
          toastr["success"](res.Messages, "");
          $scope.$emit("reloadList");
          //$modalInstance.close();
          location.href = "#/infoBoard";
        }
      }).error(res => {
        toastr["error"](res.Messages, "");
      });
    }
    $scope.submit4Provider = function(company) {
      $http({
        url: `${ApiMapper.ccApi}/g/pcontracted/add`,
        method: "POST",
        contentType: "application/json",
        headers: {
          Passport: Cookies.get("Passport")
        },
        data: JSON.stringify(company)
      }).success(function(res) {
        if (res.StatusCode == 1) {
          toastr["success"](res.Messages, "");
          $scope.$emit("reloadList");
          location.href = "#/infoBoard";
          //$modalInstance.close();
        }
      }).error(res => {
        toastr["error"](res.Messages, "");
      });
    }
    $scope.ok = function() {
      var companyDate = {};
      if ($scope.SigninType == 2) {
        companyDate = {
          MaintainerId: $scope.Company.CompanyId,
          MaintainerName: $scope.Company.CompanyName,
          MaintainerNickname: $scope.Company.NickName,
          StrongholdName: $scope.Company.CompanyName,
          Liaison: [$scope.Liaisons]
        };
        $scope.submit4Store(companyDate);
      } else {
        companyDate = {
          CompanyId: $scope.Company.CompanyId,
          CompanyName: $scope.Company.CompanyName,
          NickName: $scope.Company.NickName,
          HrCode: $scope.Company.HrCode,
          Liaison: [$scope.Liaisons]
        };
        $scope.submit4Provider(companyDate);
      }
    };
    $scope.cancel = function() {
      //$modalInstance.dismiss("cancel");
    };
  }
])
.controller("relatedEquipmentListCtrl", ["$scope", "$http", "$timeout", "$modal", "ngProgress", "$filter", "$sce", "$state", "$stateParams",
  function($scope, $http, $timeout, $modal, ngProgress, $filter, $sce, $state, $stateParams) {
    ngProgress.start();
    $scope.CompanyName  = $stateParams.companyName;
    $scope.CompanyId = $stateParams.companyId;
    $scope.dataList = [];
    let today = new Date();
    $scope.list = [];
    $scope.Page = {
      currentPage: 1,
      totalItems: 0,
      PaginationisShow: false
    };
    $scope.isNoDetail = false;
    $scope.SearchParams = {
      Filtration: ''
    };
    

    //view绑定变量，html标签支援
    $scope.deliberatelyTrustDangerousSnippet = function(snippet) {
      return $sce.trustAsHtml(snippet);
    };
    //获取关联企业负责设备列表
    $scope.getEquipList = function() {
        var params = {
          companyId: $scope.CompanyId
        };
        if ($scope.userInfo.SigninType == 2) {
          params = {
            MaintainerId: $scope.CompanyId
          };
        }
        $scope.dataList = [];
        //面板展开获取设备列表
        $http({
          url: `${ApiMapper.ccApi}/g/${$scope.userInfo.apiType}contracted/equipments/${$scope.Page.currentPage}`,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: $scope.userInfo.Passport
          },
          data: JSON.stringify(params)
        }).success(function(res) {
        if (res[2] > 0) {
          $scope.Page.totalItems = res[2];
          $scope.Page.PaginationisShow = res[0] > 1 ? true : false;
          $scope.dataList = res[1];
          $scope.isNoDetail = false;
        } else {
          $scope.isNoDetail = true;
        }
        ngProgress.complete();
      }).error(res => {
        ngProgress.complete();
      });
      ngProgress.complete();
    };
    $scope.pageChanged = function(e) {
      $scope.Page.currentPage = e;
      $scope.getEquipList();
    };

    $scope.getEquipList();
  }
]);