n580440.controller('componentsCtrl', function ($scope, $modal, $http, $state, $timeout) {
    var Passport = _Passport;

    $scope.isShowSearch = {
        status: true
    };
    $scope.SearchFilter = {
        value: ''
    };
    $scope.SelectWarehouse = {
        WarehouseId: ''
    };
    $scope.WarehouseList = [];

    $scope.List = [];
    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.ItemsPerPage = 20;

    $scope.Step = {
        status: 0 // 0: 未选择仓库 , 1:选择仓库 ,  2:选择类型  , 3：查询 
    };
    // -1:无 0:零配件  1:整机 
    $scope.IsMachine = 0;

    function removeBlankOption() {
        setTimeout(function () {
            var Select2ResultLabels = document.querySelectorAll('.select2-results__option');
            for (var i = 0; i < Select2ResultLabels.length; i++) {
                if (Select2ResultLabels[i].textContent == '') {
                    Select2ResultLabels[i].remove();
                }
            }
        }, 0);
    };

    setTimeout(function () {
        $('[name=ItemsPerPage]').select2()
            .on("select2:opening", function () {
                removeBlankOption();
            })
            .on("select2:select", function (e) {
                $scope.ItemsPerPage = parseInt(e.target.value);
                $scope.Search(1);
                $scope.$apply();
            });
        $('[name=ItemsPerPage]').select2("val", $scope.ItemsPerPage)
        $('.select2-container').css('z-index', 999);
    }, 250);


    $scope.getWarehouse = function () {
        $http({
                url: ApiMapper.sApi + '/p/materials/warehouse/all',
                method: 'GET',
                contentType: 'application/json',
                headers: {
                    'Passport': Passport
                },
            }).success(function (data) {
                $scope.WarehouseList = data;
            })
            .error(function (error) {
                alert(error);
            });
    };
    $scope.getWarehouse();

    $scope.changeWarehouse = function (item) {
        $scope.Step.status = 1;
        $scope.SelectWarehouse = item;
        $scope.IsMachine = -1;
    };

    $scope.changeCreateType = function (IsMachine) {
        $scope.Step.status = 2;
        $scope.IsMachine = IsMachine;
        $scope.Search(1);
    };


    $scope.Search = function (page) {
        $scope.Step.status = 3;
        $scope.isLoading = true;
        $scope.isSearchError = false;
        $scope.currentPage = page;
        $scope.List = [];

        var Params = {
            Parameter: $scope.SearchFilter.value,
            WarehouseId: $scope.SelectWarehouse.WarehouseId,
            PageSize: $scope.ItemsPerPage,
            IsMachine: $scope.IsMachine
        };
        console.log(Params);

        $http({
                // url: ApiMapper.sApi + '/p/materials/searchStock/mainList/' + page,
                url: ApiMapper.sApi + '/p/materials/tobeStock/mainList/' + page,
                method: 'POST',
                contentType: 'application/json',
                headers: {
                    'Passport': Passport
                },
                data: JSON.stringify(Params)
            }).success(function (data) {
                $scope.isLoading = false;
                $scope.List = data[1];
                $scope.totalItems = data[2];
                $scope.PaginationisShow = data[2] > $scope.ItemsPerPage ? true : false;
                if (data[1].length == 0) {
                    $scope.isSearchError = true;
                } else {
                    $scope.isSearchError = false;
                };
            })
            .error(function (error) {
                $scope.isLoading = false;
                alert(error.Messages);
            });
    };

    $scope.pageChanged = function (e) {
        $scope.Search(e);
    };


    $scope.openDetail = function (MainItem) {
        var params = {
            WarehouseName: $scope.SelectWarehouse.Name,
            WarehouseId: $scope.SelectWarehouse.WarehouseId,
            ComponentName: MainItem.ComponentName,
            ComponentId: MainItem.ComponentId
        };

        var modalInstance = $modal.open({
            templateUrl: 'views/modal/inventory/InventoryModal.html',
            controller: 'InventoryDetailCtrl',
            size: 'xlg',
            resolve: {
                params: params
            }
        });

        modalInstance.result.then(function (result) {
            $scope.Search($scope.currentPage);
        }, function (reason) {
            // console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.pageChanged = function (e) {
        $scope.Search(e);
    };

})
.controller('InventoryDetailCtrl', function (params, $scope, $modalInstance ,$timeout, $stateParams, $http) {
    var Passport = _Passport;
    $scope.Data = params;
    $scope.List = [];
    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.ItemsPerPage = 20;

    function removeBlankOption() {
        setTimeout(function () {
            var Select2ResultLabels = document.querySelectorAll('.select2-results__option');
            for (var i = 0; i < Select2ResultLabels.length; i++) {
                if (Select2ResultLabels[i].textContent == '') {
                    Select2ResultLabels[i].remove();
                }
            }
        }, 0);
    };

    setTimeout(function () {
        $('[name=DItemsPerPage]').select2()
            .on("select2:opening", function () {
                removeBlankOption();
            })
            .on("select2:select", function (e) {
                $scope.ItemsPerPage = parseInt(e.target.value);
                $scope.Search(1);
                $scope.$apply();
            });
        $('[name=DItemsPerPage]').select2("val", $scope.ItemsPerPage)
        $('.select2-container').css('z-index', 999);
    }, 250);

    $scope.Search = function (page) {
        $scope.IsLoading = true;
        $scope.currentPage = page;
        $scope.List = [];

        var Params = {
            ComponentId: $scope.Data.ComponentId ,
            WarehouseId: $scope.Data.WarehouseId,
            PageSize: $scope.ItemsPerPage
        };

        $http({
                url: ApiMapper.sApi + '/p/materials/searchStock/subDetail/' + page,
                method: 'POST',
                contentType: 'application/json',
                headers: {
                    'Passport': Passport
                },
                data: JSON.stringify(Params)
            }).success(function (data) {
                $scope.IsLoading = false;
                $scope.List = data[1];
                $scope.totalItems = data[2];
                $scope.PaginationisShow = data[2] > $scope.ItemsPerPage ? true : false;
                if (data[1].length == 0) {
                    $scope.isSnoSelectError = true;
                } else {
                    $scope.isSnoSelectError = false;
                };
            })
            .error(function (error) {
                $scope.IsLoading = false;
                alert(error.Messages);
            });
    };
    $scope.Search(1);

    $scope.pageChanged = function (e) {
        $scope.Search(e);
    };

    $scope.Close = function (param) {
        $modalInstance.dismiss('cancel');
    };

})
;