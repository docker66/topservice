n580440.controller('componentsDetailCtrl', function(params, $scope, $modalInstance, $timeout, $stateParams, $http) {
  var Passport = _Passport;
  $scope.Data = params;
  $scope.List = [];
  $scope.currentPage = 1;
  $scope.totalItems = 0;
  $scope.ItemsPerPage = 20;
  $scope.SerachParames = {
    "ComponentId": "",
    "IsMachine": -1
  };

  function removeBlankOption() {
    setTimeout(function() {
      var Select2ResultLabels = document.querySelectorAll('.select2-results__option');
      for (var i = 0; i < Select2ResultLabels.length; i++) {
        if (Select2ResultLabels[i].textContent == '') {
          Select2ResultLabels[i].remove();
        }
      }
    }, 0);
  };
  setTimeout(function() {
    $('[name=DItemsPerPage]').select2().on("select2:opening", function() {
      removeBlankOption();
    }).on("select2:select", function(e) {
      $scope.ItemsPerPage = parseInt(e.target.value);
      $scope.Search(1);
      $scope.$apply();
    });
    $('[name=DItemsPerPage]').select2("val", $scope.ItemsPerPage)
    $('.select2-container').css('z-index', 999);
  }, 250);
  $scope.Search = function(page) {
    $scope.IsLoading = true;
    $scope.currentPage = page;
    $scope.List = [];
    var Params = {
      ComponentId: $scope.Data.ComponentId,
      WarehouseId: $scope.Data.WarehouseId,
      PageSize: $scope.ItemsPerPage
    };
    $http({
      url: ApiMapper.sApi + '/p/materials/searchStock/subDetail/' + page,
      method: 'POST',
      contentType: 'application/json',
      headers: {
        'Passport': Passport
      },
      data: JSON.stringify(Params)
    }).success(function(data) {
      $scope.IsLoading = false;
      $scope.List = data[1];
      $scope.totalItems = data[2];
      $scope.PaginationisShow = data[2] > $scope.ItemsPerPage ? true : false;
      if (data[1].length == 0) {
        $scope.isSnoSelectError = true;
      } else {
        $scope.isSnoSelectError = false;
      };
    }).error(function(error) {
      $scope.IsLoading = false;
      alert(error.Messages);
    });
  };
  $scope.Search(1);
  $scope.pageChanged = function(e) {
    $scope.Search(e);
  };
  $scope.Close = function(param) {
    $modalInstance.dismiss('cancel');
  };
});