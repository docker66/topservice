angular.module("n580440").controller("equipmentListController", ["$scope", "$http", "$timeout", "$modal", "ngProgress", "$filter", "$sce", "$state", "$stateParams", "DataMaintenanceFactory", "$q",
  function($scope, $http, $timeout, $modal, ngProgress, $filter, $sce, $state, $stateParams, DataMaintenanceFactory, $q) {
    ngProgress.start();
    $scope.currentPage = 0; // 0:还没查询过
    $scope.SearchParams = {
      EquipmentCodeOrName: "",
      Unit: "",
      UnitId: "",
      Brand: "",
      Status: "-2", // 1:Enable , 0:Disable
      EquipmentTypeClass: "-2",
      EquipmentTypeName: "",
      EquipmentSubTypeName: ""
    };
    $scope.EquipmentList = [];
    $scope.BrandList = [];
    $scope.EquipmentTypeList = {
      EquipmentTypeClass: [],
      EquipmentTypeName: [],
      EquipmentSubTypeName: []
    };
    $scope.CsvButtonDisabled = false;
    // 下载板模
    $scope.downloadExcel = DataMaintenanceFactory.downloadExcel;
    //获取关联企业列表
    $scope.getContractedList = function() {
      $http({
        url: `${ApiMapper.ccApi}/g/pcontracted/1`,
        method: "POST",
        contentType: "application/json",
        headers: {
          Passport: $scope.userInfo.Passport
        },
        data: JSON.stringify({
          Filtration: ''
        })
      }).success(function(res) {
        if (res[2] > 0) {
          $scope.dataList = res[1];
          $scope.SearchParams.CompanyId = $scope.dataList[0].CompanyId;
          $scope.Search(1);
        }
      }).error(res => {});
    };
    // select 样式与资料
    $scope.init = function() {
      setTimeout(function() {
        // 品牌
        $("[name=Brand]").select2();
        // 狀態
        $("[name=Status]").select2({
          minimumResultsForSearch: Infinity
        }).select2("val", $scope.SearchParams.Status);
        // 大类
        $("[name=TypeClass]").select2();
        $("[name=TypeClass]").select2("val", $scope.SearchParams.EquipmentTypeClass);
        // 中类
        $("[name=TypeName]").select2();
        // 小类
        $("[name=SubTypeName]").select2();
        $(".select2-container").css("z-index", 999);
      }, 0);
      var p1 = DataMaintenanceFactory.getBrand($scope.userInfo.Passport);
      var p2 = DataMaintenanceFactory.getEquipType($scope.userInfo.Passport);
      var all = $q.all([p1, p2]);
      all.then(function(res) {
        var BrandData = res[0].data;
        var TypeData = res[1].data;
        $scope.BrandList = BrandData;
        $scope.EquipmentTypeList.EquipmentTypeClass = TypeData.EquipmentTypeClass;
        $scope.EquipmentTypeList.EquipmentTypeName = TypeData.EquipmentTypeName;
        $scope.EquipmentTypeList.EquipmentSubTypeName = TypeData.EquipmentSubTypeName;
        ngProgress.complete();
      }).catch(function(e) {
        ngProgress.complete();
        alert(e.data.Message);
      });
      if ($scope.userInfo.SigninType == 2) {
        $scope.Search(1);
      } else {
        $scope.getContractedList();
      }
    };
    // 新开页面-刷新页面用
    window.ReloadEquipmentList = function() {
      if ($scope.currentPage !== 0) {
        $scope.Search($scope.currentPage);
      }
    };
    // 查询
    $scope.Search = function(PageNo) {
      $scope.currentPage = PageNo;
      ngProgress.start();
      $scope.EquipmentList = [];
      var Params = {
        EquipmentCodeOrName: $scope.SearchParams.EquipmentCodeOrName,
        UnitId: $scope.SearchParams.UnitId,
        Brand: $scope.SearchParams.Brand,
        Status: $scope.SearchParams.Status,
        EquipmentTypeClass: $scope.SearchParams.EquipmentTypeClass,
        EquipmentTypeName: $scope.SearchParams.EquipmentTypeName,
        EquipmentSubTypeName: $scope.SearchParams.EquipmentSubTypeName
      };
      DataMaintenanceFactory.getEquipList($scope.userInfo.Passport, PageNo, Params).success(function(data) {
        $scope.EquipmentList = data[1];
        //總數量
        $scope.totalItems = data[2];
        //是否顯示頁碼
        $scope.PaginationisShow = data[2] > 20 ? true : false;
        ngProgress.complete();
      }).error(function(error) {
        ngProgress.complete();
      });
    };
    $scope.pageChanged = function(e) {
      $scope.Search(e);
    };
    
    $scope.init();
    // 需求端选取门店
    $scope.setCompany = function() {
      var Info = {
        CompanyId: $scope.userInfo.CompanyId,
        EquipmentTypeClass: null,
        ApiUrl: ApiMapper.ccApi
      };
      var modalInstance = $modal.open({
        templateUrl: "unitsModal.html",
        controller: "ntOrgPicker",
        resolve: {
          Options: function() {
            return {
              Type: "S1",
              Source: "n580440"
            };
          },
          Info: function() {
            return Info;
          },
          UnitInfo: function() {
            return "";
          }
        }
      });
      modalInstance.result.then(function(Unit) {
        $scope.SearchParams.Unit = Unit.Unit[0];
        $scope.SearchParams.UnitId = Unit.UnitId[0];
      });
    };
    $scope.setCompany4Provider = function() {
      var storeModal = $modal.open({
        animation: true,
        templateUrl: 'storeModal.html',
        controller: 'storeModalCtrl',
        windowClass: 'margin-top-50',
        size: 'md',
        resolve: {
          Passport: function() {
            return $scope.userInfo.Passport;
          },
          $type: function() {
            return '1';
          },
          SearchParams: function() {
            return $scope.SearchParams;
          },
          IsHideUnit: function() {
            return true;
          }
        }
      });
      storeModal.result.then(function(parameters) {
        $scope.SearchParams.StoreCompanyId = parameters.ServeStoreRadio;
        $scope.Search(1);
      }, function() {});
    };
    //打开门店选择modal
    $scope.openCompanyModal = function() {
      if ($scope.userInfo.apiType == 's') {
        $scope.setCompany();
      } else {
        $scope.setCompany4Provider();
      }
    };
    $scope.getCsv = function() {
      $scope.CsvButtonDisabled = true;
      var Params = {
        EquipmentCodeOrName: $scope.SearchParams.EquipmentCodeOrName,
        UnitId: $scope.SearchParams.UnitId,
        Brand: $scope.SearchParams.Brand,
        Status: $scope.SearchParams.Status,
        EquipmentTypeClass: $scope.SearchParams.EquipmentTypeClass,
        EquipmentTypeName: $scope.SearchParams.EquipmentTypeName,
        EquipmentSubTypeName: $scope.SearchParams.EquipmentSubTypeName
      };
      DataMaintenanceFactory.getEquipListCsv($scope.userInfo.Passport, Params).success(function(data, status, headers) {
        headers = headers();
        var FileName = decodeURIComponent(headers["x-filename"]);
        var Data = "\ufeff" + data;
        var csvData = new Blob([Data], {
          type: "text/csv;charset=utf-8;"
        });
        //IE11 & Edge
        if (navigator.msSaveBlob) {
          navigator.msSaveBlob(csvData, FileName);
        } else {
          var link = document.createElement("a");
          link.href = window.URL.createObjectURL(csvData);
          link.setAttribute("download", FileName);
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
        $scope.CsvButtonDisabled = false;
      }).error(function(error) {
        $scope.CsvButtonDisabled = false;
        alert(error.Message);
      });
    };
    $scope.importCsv = function() {
      var ImportInfo = {
        Title: "设备导入",
        Type: "0"
      };
      var importCsv = $modal.open({
        animation: true,
        templateUrl: "importCsv.html",
        controller: "importCsvController",
        size: "lg",
        backdrop: "static",
        keyboard: false,
        resolve: {
          ImportInfo: function() {
            return ImportInfo;
          },
          Params: function() {
            return "";
          },
          Passport: function() {
            return $scope.userInfo.Passport;
          }
        }
      });
      importCsv.result.then(function(result) {
        if (result == "success") {
          window.ReloadEquipmentList();
        }
      });
    };
  }
]);