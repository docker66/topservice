angular.module("n580440").controller("equipmentModifyController", ["$scope", "$http", "$timeout", "$modal", "ngProgress", "$filter", "$sce", "$state", "$stateParams", "DataMaintenanceFactory", "$q",
  function($scope, $http, $timeout, $modal, ngProgress, $filter, $sce, $state, $stateParams, DataMaintenanceFactory, $q) {
    ngProgress.start();
    // detail or edit
    $scope.ViewEdit = {
      status: false
    };
    $scope.CompanyList = [];
    $scope.SelectEquipmentpos = [];
    $scope.Info = {
      CompanyId: $scope.userInfo.CompanyId,
      EquipmentTypeClass: null,
      ApiUrl: ApiMapper.ccApi
    };
    $scope.Data = {
      Unit: "",
      CompanyId: ""
    };
    $scope.Equipmentpos = [];
    $scope.BrandList = [];
    $scope.EquipmentTypeList = {
      EquipmentTypeClass: [],
      EquipmentTypeName: [],
      EquipmentSubTypeName: []
    };
    $scope.ModelList = [];
    $scope.CompanyList = [];
    $scope.ViewData = {};
    $scope.EditData = {};
    $scope.CausePriorityList = {};
    $scope.EditCausePriorityList = {};
    $scope.tableTitle = "报修原因及时限";

    function removeBlankOption() {
      setTimeout(function() {
        var Select2ResultLabels = document.querySelectorAll(".select2-result-label");
        for (var i = 0; i < Select2ResultLabels.length; i++) {
          if (Select2ResultLabels[i].textContent == "") {
            Select2ResultLabels[i].remove();
          }
        }
      }, 0);
    }
    $scope.loadList = function(tag) {
      if (parseInt(tag) === 0) {
        $scope.tableTitle = "报修原因及时限";
      } else if (parseInt(tag) === 1) {
        $scope.tableTitle = "报修历程";
      } else {
        $scope.tableTitle = "调拨历程";
      }
    };
    // select2 樣式初始化 與 資料獲取
    $scope.init = function() {
      $scope.ViewEdit.status = false;
      $scope.buttonDisabled = false;
      $scope.EditButtonDisabled = true;
      setTimeout(function() {
        // 品牌
        $("[name=Brand]").select2({
          placeholder: "请选择品牌",
          allowClear: true
        });
        // 大类
        $("[name=TypeClass]").select2({
          placeholder: "请选择大类"
        }).on("select2-opening", function() {
          removeBlankOption();
        }).on("select2-loaded", function() {
          removeBlankOption();
        });
        // 中类
        $("[name=TypeName]").select2({
          placeholder: "请选择中类"
        }).on("select2-opening", function() {
          removeBlankOption();
        }).on("select2-loaded", function() {
          removeBlankOption();
        });
        // 小类
        $("[name=SubTypeName]").select2({
          placeholder: "请选择小类"
        }).on("select2-opening", function() {
          removeBlankOption();
        }).on("select2-loaded", function() {
          removeBlankOption();
        });
        // 維修商
        $("[name=FormCompanyList]").select2({
          placeholder: "请选择维修商",
          allowClear: true
        });
        // 所在位置
        $("[name=Position]").select2({
          placeholder: "请选择所在位置"
        });
        $(".select2-container").css("z-index", 999);
      }, 0);
      var p1 = DataMaintenanceFactory.getBrand($scope.userInfo.Passport);
      var p2 = DataMaintenanceFactory.getModel($scope.userInfo.Passport);
      var p3 = DataMaintenanceFactory.getEquipType($scope.userInfo.Passport);
      var p4 = DataMaintenanceFactory.getEquipmentPos($scope.userInfo.Passport);
      var p5 = DataMaintenanceFactory.getCompanyList($scope.userInfo.Passport, 'p');
      var p6 = DataMaintenanceFactory.getDetailEquipment($scope.userInfo.Passport, $stateParams.equipmentId);
      var all = $q.all([p1, p2, p3, p4, p5, p6]);
      all.then(function(res) {
        var BrandData = res[0].data;
        var ModelData = res[1].data;
        var TypeData = res[2].data;
        var PosData = res[3].data;
        var pCompanyData = res[4].data;
        var DetailEquipment = res[5].data;
        // 1.品牌 选项
        $scope.BrandList = BrandData;
        // 2.型号 选项
        var ModelList = [];
        _(ModelData).each(function(Items, index) {
          ModelList.push({
            id: Items.Model,
            text: Items.Model
          });
        });
        $("[name=Model]").select2({
          placeholder: "请选择型号",
          data: ModelList,
          allowClear: true
        });
        $(".select2-container").css("z-index", 999);
        // 3.大中小类 选项
        $scope.EquipmentTypeList = {
          EquipmentTypeClass: [],
          EquipmentTypeName: [],
          EquipmentSubTypeName: []
        };
        $scope.EquipmentTypeList.EquipmentTypeClass = TypeData.EquipmentTypeClass;
        $scope.EquipmentTypeList.EquipmentTypeName = TypeData.EquipmentTypeName;
        $scope.EquipmentTypeList.EquipmentSubTypeName = TypeData.EquipmentSubTypeName;
        // 4.所在区域 选项
        var Regions = _.groupBy(PosData, "Region");
        _(Regions).each(function(Items) {
          $scope.Equipmentpos.push({
            Region: Items[0].Region,
            children: Items
          });
        });
        // 5.公司别 选项
        $scope.CompanyList = pCompanyData;
        // 6.設備詳情 報修時限及歷程
        $scope.ViewData = JSON.parse(JSON.stringify(DetailEquipment.EquipDetail));
        $scope.CausePriorityList = JSON.parse(JSON.stringify(DetailEquipment.CausePriority));
        let RezHistories = $scope.ViewData.RezHistory;
        $scope.TransferHistories = $scope.ViewData.Transfer;
        console.log($scope.TransferHistories);
        $scope.RezHistories = _(RezHistories).each(item => {
          if (item.Maintenance === 0) item.AccrualTypeName = "一般";
          else if (item.Maintenance === 1) item.AccrualTypeName = "巡检";
          else if (item.Maintenance === 2) item.AccrualTypeName = "统包";
          else if (item.Maintenance === 3) item.AccrualTypeName = "申诉";
        }); //报修记录
        setTimeout(function() {
          ngProgress.complete();
          $scope.EditButtonDisabled = false;
        }, 0);
      }).catch(function(e) {
        ngProgress.complete();
        alert(e.data.Message);
      });
    };
    $scope.init();
    // 編輯&&設定預設值
    $scope.setEdit = function() {
      $scope.ViewEdit.status = true;
      $scope.EditData = JSON.parse(JSON.stringify($scope.ViewData));
      if ($scope.EditData.Allocation.length > 0) {
        $scope.getCauseAndPriorities($scope.EditData.EquipmentSubTypeId, $scope.EditData.EquipmentId, $scope.EditData.Allocation[0].MaintainerId, $scope.EditData.Service);
      } else {
        $scope.EditData.Allocation.push({
          MaintainerId: "",
          MaintainerNickName: ""
        });
      }
      setTimeout(function() {
        // 1.品牌
        $("[name=Brand]").select2("val", $scope.EditData.Brand);
        // 2.型號
        $("[name=Model]").select2("val", $scope.EditData.Model);
        // 3.大中小類
        $("[name=TypeClass]").select2("val", $scope.EditData.EquipmentClass.EquipmentClass).on("select2-selecting", function(e) {
          $scope.EditData.EquipmentClass.EquipmentClassName = e.object.text;
          //清除 中,小類
          $scope.EditData.EquipmentTypeId = "";
          $scope.EditData.EquipmentSubTypeId = "";
          $scope.EditData.EquipmentTypeName = "";
          $scope.EditData.EquipmentSubTypeName = "";
          $("[name=TypeName]").select2("val", $scope.EditData.EquipmentTypeId);
          $("[name=SubTypeName]").select2("val", $scope.EditData.EquipmentSubTypeId);
        });
        $("[name=TypeName]").select2("val", $scope.EditData.EquipmentTypeId).on("select2-selecting", function(e) {
          $scope.EditData.EquipmentTypeName = e.object.text;
          // 清除 小類
          $scope.EditData.EquipmentSubTypeName = "";
          $scope.EditData.EquipmentSubTypeId = "";
          $("[name=SubTypeName]").select2("val", $scope.EditData.EquipmentSubTypeId);
        });
        $("[name=SubTypeName]").select2("val", $scope.EditData.EquipmentSubTypeId).on("select2-selecting", function(e) {
          $scope.EditData.EquipmentSubTypeName = e.object.text;
          if ($scope.EditData.Allocation.length > 0) {
            setTimeout(function() {
              $scope.getCauseAndPriorities($scope.EditData.EquipmentSubTypeId, $scope.EditData.EquipmentId, $scope.EditData.Allocation[0].MaintainerId, $scope.EditData.Service);
            }, 0);
          }
        });
        // 4.預設維修商
        $("[name=FormCompanyList]").select2("val", $scope.EditData.Allocation[0].MaintainerId).on("select2-selecting", function(e) {
          $scope.EditData.Allocation[0].MaintainerNickName = e.object.text;
        });
        // 5.擺放位置
        var Position = [];
        _($scope.EditData.Position).each(function(Items) {
          Position.push(Items.PositionId);
        });
        $scope.SelectEquipmentpos = Position;
        $("[name=Position]").select2("val", $scope.SelectEquipmentpos);
        $scope.$apply();
      }, 0);
    };
    $scope.setCompany = function() {
      var modalInstance = $modal.open({
        templateUrl: "unitsModal.html",
        controller: "ntOrgPicker",
        resolve: {
          Options: function() {
            return {
              Type: "S1",
              Source: "n580440"
            };
          },
          Info: function() {
            return $scope.Info;
          },
          UnitInfo: function() {
            return "";
          }
        }
      });
      modalInstance.result.then(function(Unit) {
        $scope.EditData.Unit = Unit.Unit[0];
        $scope.EditData.UnitId = Unit.UnitId[0];
        $scope.EditData.CompanyId = Unit.CompanyId;
        $scope.EditData.CompanyNickName = Unit.Nickname;
      });
    };
    $scope.getCauseAndPriorities = function(EquipmentSubTypeId, EquipmentId, MaintainerId, Service) {
      $scope.EditCausePriorityList = [];
      var Params = {
        EquipmentSubTypeId: EquipmentSubTypeId,
        EquipmentId: EquipmentId,
        MaintainerId: MaintainerId,
        Service: Service
      };
      DataMaintenanceFactory.getCauseAndPriorities($scope.userInfo.Passport, Params).success(function(data) {
        $scope.EditCausePriorityList = data;
      });
    };
    $scope.btnCancel = function() {
      window.close();
    };
    $scope.btnOk = function() {
      $scope.buttonDisabled = true;
      ngProgress.start();
      var Params = {
        Equipment: {
          EquipmentId: $scope.EditData.EquipmentId,
          EquipmentName: $scope.EditData.EquipmentName,
          Manufacturer: $scope.EditData.Brand,
          Model: $scope.EditData.Model,
          PropertyCode: $scope.EditData.PropertyCode,
          UnitId: $scope.EditData.UnitId,
          Unit: $scope.EditData.Unit,
          CompanyId: $scope.EditData.CompanyId,
          EquipmentTypeClass: $scope.EditData.EquipmentClass.EquipmentClass,
          EquipmentTypeId: $scope.EditData.EquipmentTypeId,
          EquipmentSubTypeId: $scope.EditData.EquipmentSubTypeId,
          EquipmentCode: $scope.EditData.EquipmentCode,
          IPositionIds: $scope.SelectEquipmentpos,
          IsRezPhotos: $scope.EditData.Picture,
          Status: $scope.EditData.Status
        },
        MaintainerId: $scope.EditData.Allocation[0].MaintainerId,
        Service: $scope.EditData.Service
      };
      DataMaintenanceFactory.updateEquip($scope.userInfo.Passport, Params).success(function(data) {
        ngProgress.complete();
        $scope.$toastlast = toastr["success"]("修改完成", "");
        setTimeout(function() {
          if (window.opener) {
            window.opener.ReloadEquipmentList();
            window.close();
          } else {
            window.close();
          }
        }, 1000);
      }).error(function(error) {
        $scope.buttonDisabled = false;
        ngProgress.complete();
        alert(error.Message);
      });
    };
  }
]);