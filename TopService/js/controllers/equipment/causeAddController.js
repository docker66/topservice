angular.module("n580440").controller("causeAddCtrl", ["$scope", "$http", "$timeout", "$modal", "ngProgress", "$filter", "$sce", "$state", "$stateParams", "DataMaintenanceFactory", "$q", "FileUploader", "i18n",
  function($scope, $http, $timeout, $modal, ngProgress, $filter, $sce, $state, $stateParams, DataMaintenanceFactory, $q, FileUploader, i18n) {
    ngProgress.start();
    $scope.Data = {
      CompanyId: "",
      EquipmentSubTypeId: "",
      FileName: ""
    };
    $scope.EquipmentTypeList = {
      EquipmentTypeClass: [],
      EquipmentTypeName: [],
      EquipmentSubTypeName: []
    };
    $scope.CompanyList = [];
    $scope.sCompanyList = [];
    $scope.FilterCompanyId = "";
    $scope.FilterEquipmentClass = "";
    $scope.FilterEquipmentTypeId = "";
    $scope.EquipCodeList = [];
    // Import 内容
    $scope.ImportInfo = {
      Type: "1" // 导入类型,  (0:设备)
    };
    $scope.FileName = "";
    $scope.Result = {
      type: 0, // 验证结果 ,(0=>未验证 , 1 => success , -1=>error ,999=>等待载入中)
      errorMessage: "", // 错误讯息.
      data: "", // 第二只api所需资料.
      buttonDisabled: true // 第二只api按钮是否启用.
    };
    $scope.BackupDefer = "";
    $scope.BackupPromise = "";
    // upload setting
    var uploader = ($scope.uploader = new FileUploader({
      url: ApiMapper.ccApi + "/g/management/uploadTmpl/Temp/0",
      headers: {
        Passport: $scope.userInfo.Passport
      }
    }));
    //  uploader filters
    uploader.filters.push({
      name: "imageFilter",
      fn: function(item /*{File|FileLikeObject}*/ , options) {
        var type = "|" + item.type.slice(item.type.lastIndexOf("/") + 1) + "|";
        if (uploader.queue.length == 1) {
          uploader.removeFromQueue(0);
        }
        return uploader.queue.length < 10;
      }
    });
    // 重新选取-清除原本资料
    uploader.onAfterAddingFile = function() {
      $scope.FileName = "";
      $scope.Result = {
        type: 0,
        errorMessage: "",
        data: "",
        buttonDisabled: true
      };
    };
    // 获取表头栏位
    function getColumns(type) {
      var List = [];
      switch (type) {
        // 2.原因及時限导入栏位(有兩种列表)
        case "1":
          //原因
          var Columns = [{
            field: "MaintainerNickName",
            title: "厂商",
            align: "center",
            width: 100
          }, {
            field: "Causes",
            title: "原因",
            align: "center",
            width: 100
          }, {
            field: "Status",
            title: "状态",
            align: "center",
            width: 100
          }];
          //时限
          var Columns2 = [{
            field: "Code",
            title: "设备编号",
            align: "center",
            width: 100
          }, {
            field: "Causes",
            title: "原因",
            align: "center",
            width: 100
          }, {
            field: "Priority",
            title: "时限",
            align: "center",
            width: 100
          }, {
            field: "Service",
            title: "服务网点",
            align: "center",
            width: 100
          }];
          List = [Columns, Columns2];
          break;
        default:
          break;
      }
      return List;
    }
    // 生成表格
    $scope.createTable = function(Type, InsertData, UpdateData, InsertData2, UpdateData2) {
      // 建立[新增]与[更新]表格
      setTimeout(function() {
        var columns = getColumns(Type);
        // 新增列表
        $("#InsertTable").bootstrapTable({
          columns: columns[0],
          classes: "table",
          cache: false,
          data: InsertData,
          height: 300,
          striped: true,
          pagination: true,
          pageSize: 10000,
          onlyInfoPagination: true,
          formatDetailPagination: function(totalRows) {
            return "共 " + totalRows + " 笔";
          },
          formatNoMatches: function() {
            return "无新增资料";
          }
        });
        // 更新列表
        $("#UpdateTable").bootstrapTable({
          columns: columns[0],
          classes: "table",
          cache: false,
          data: UpdateData,
          height: 300,
          striped: true,
          pagination: true,
          pageSize: 10000,
          onlyInfoPagination: true,
          formatDetailPagination: function(totalRows) {
            return "共 " + totalRows + " 笔";
          },
          formatNoMatches: function() {
            return "无更新资料";
          }
        });
        if (InsertData2 !== undefined && UpdateData2 !== undefined) {
          // 新增列表(第二种)
          $("#InsertTable2").bootstrapTable({
            columns: columns[1],
            classes: "table",
            cache: false,
            data: InsertData2,
            height: 300,
            striped: true,
            pagination: true,
            pageSize: 10000,
            onlyInfoPagination: true,
            formatDetailPagination: function(totalRows) {
              return "共 " + totalRows + " 笔";
            },
            formatNoMatches: function() {
              return "无新增资料";
            }
          });
          // 更新列表(第二种)
          $("#UpdateTable2").bootstrapTable({
            columns: columns[1],
            classes: "table",
            cache: false,
            data: UpdateData2,
            height: 300,
            striped: true,
            pagination: true,
            pageSize: 10000,
            onlyInfoPagination: true,
            formatDetailPagination: function(totalRows) {
              return "共 " + totalRows + " 笔";
            },
            formatNoMatches: function() {
              return "无更新资料";
            }
          });
        }
      }, 0);
    };
    // 备份Csv
    $scope.backupCsv = function(data, fileName, type, status) {
      var URI = ApiMapper.ccApi + "/g/management/EnterDataBase/CausesPriorities";
      var ParameterStr = {
        Import: data,
        FileName: fileName,
        Tmpl: type,
        IsConfirm: status // 0:取消 , 1:确认
      };
      // 呼api
      $http({
        url: URI,
        method: "POST",
        contentType: "application/json",
        headers: {
          Passport: $scope.userInfo.Passport
        },
        data: JSON.stringify(ParameterStr)
      }).success(function(data) {
        $scope.BackupDefer.resolve(data);
      }).error(function(error) {
        $scope.BackupDefer.reject(error);
      });
    };

    function removeBlankOption() {
      setTimeout(function() {
        var Select2ResultLabels = document.querySelectorAll(".select2-result-label");
        for (var i = 0; i < Select2ResultLabels.length; i++) {
          if (Select2ResultLabels[i].textContent == "") {
            Select2ResultLabels[i].remove();
          }
        }
      }, 0);
    }
    $scope.init = function() {
      setTimeout(function() {
        // 維修商
        $("[name=FormCompanyList]").select2({
          placeholder: "请选择维修商"
        }).on("change", function(e) {
          if ($scope.Data.CompanyId !== "" && $scope.Data.EquipmentSubTypeId !== "") {
            $scope.getEquipCodes($scope.Data.CompanyId, $scope.Data.EquipmentSubTypeId);
          }
        });
        // 小类
        $("[name=SubTypeName]").select2({
          placeholder: "请选择小类"
        }).on("select2-opening", function() {
          removeBlankOption();
        }).on("select2-loaded", function() {
          removeBlankOption();
        }).on("change", function(e) {
          if ($scope.Data.CompanyId !== "" && $scope.Data.EquipmentSubTypeId !== "") {
            $scope.getEquipCodes($scope.Data.CompanyId, $scope.Data.EquipmentSubTypeId);
          }
        });
        // 厂商
        $("[name=sCompanyList]").select2({
          placeholder: "请选择报修商"
        }).on("select2-selecting", function(e) {
          $scope.FilterEquipmentClass = "";
          $scope.FilterEquipmentTypeId = "";
          $scope.Data.EquipmentSubTypeId = "";
          $("[name=TypeClass]").select2("val", $scope.FilterEquipmentClass);
          $("[name=TypeName]").select2("val", $scope.FilterEquipmentTypeId);
          $("[name=SubTypeName]").select2("val", $scope.FilterEquipmentTypeId);
        });
        // 大类
        $("[name=TypeClass]").select2({
          placeholder: "大类过滤条件"
        }).on("select2-opening", function() {
          removeBlankOption();
        }).on("select2-loaded", function() {
          removeBlankOption();
        }).on("select2-selecting", function() {
          $scope.FilterEquipmentTypeId = "";
          $scope.Data.EquipmentSubTypeId = "";
          $("[name=TypeName]").select2("val", $scope.FilterEquipmentTypeId);
          $("[name=SubTypeName]").select2("val", $scope.FilterEquipmentTypeId);
        });
        // 中类
        $("[name=TypeName]").select2({
          placeholder: "中类过滤条件"
        }).on("select2-opening", function() {
          removeBlankOption();
        }).on("select2-loaded", function() {
          removeBlankOption();
        }).on("select2-selecting", function() {
          $scope.Data.EquipmentSubTypeId = "";
          $("[name=SubTypeName]").select2("val", $scope.FilterEquipmentTypeId);
        });
        $(".select2-container").css("z-index", 999);
      }, 0);
      var p1 = DataMaintenanceFactory.getCompanyList($scope.userInfo.Passport, 'p');
      var p2 = DataMaintenanceFactory.getCompanyList($scope.userInfo.Passport, 's');
      var p3 = DataMaintenanceFactory.getEquipType($scope.userInfo.Passport);
      var all = $q.all([p1, p2, p3]);
      all.then(function(res) {
        var pCompanyData = res[0].data;
        var sCompanyData = res[1].data;
        var TypeData = res[2].data;
        // 1.公司别
        $scope.CompanyList = pCompanyData;
        // 2.公司别(厂商)
        $scope.sCompanyList = sCompanyData;
        // 3.大中小类
        $scope.EquipmentTypeList = {
          EquipmentTypeClass: [],
          EquipmentTypeName: [],
          EquipmentSubTypeName: []
        };
        $scope.EquipmentTypeList.EquipmentTypeClass = TypeData.EquipmentTypeClass;
        $scope.EquipmentTypeList.EquipmentTypeName = TypeData.EquipmentTypeName;
        $scope.EquipmentTypeList.EquipmentSubTypeName = TypeData.EquipmentSubTypeName;
        ngProgress.complete();
      }).catch(function(e) {
        ngProgress.complete();
        alert(e.data.Message);
      });
    };
    $scope.init();
    $scope.getEquipCodes = function(CompanyId, EquipmentSubTypeId) {
      $scope.EquipCodeList = [];
      var Param = {
        SubTypeId: EquipmentSubTypeId,
        MaintainerId: CompanyId
      };
      DataMaintenanceFactory.getSubEquipCodes($scope.userInfo.Passport, Param).success(function(data) {
        $scope.EquipCodeList = data;
      });
    };
    $scope.checkCsv = function() {
      // 等待伺服器处理资料
      $scope.Result.type = "999";
      // 参数设定
      $scope.FileName = uploader.queue[0].file.name;
      var URI = ApiMapper.ccApi + "/g/management/readCSV/CausesPriorities";
      var ParameterStr = {
        FileName: $scope.FileName,
        Tmpl: $scope.ImportInfo.Type,
        SubTypeId: $scope.Data.EquipmentSubTypeId,
        MaintainerId: $scope.Data.CompanyId,
        CompanyId: $scope.FilterCompanyId
      };
      //全部上傳
      uploader.uploadAll();
      //上傳成功
      uploader.onSuccessItem = function(fileItem, response, status, headers) {
        $http({
          url: URI,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: $scope.userInfo.Passport
          },
          data: JSON.stringify(ParameterStr)
        }).success(function(data) {
          $scope.Result.data = data;
          $scope.Result.type = "1";
          $scope.Result.buttonDisabled = false;
          $scope.createTable($scope.ImportInfo.Type, $scope.Result.data.InsertMappingView, $scope.Result.data.UpdateMappingView, $scope.Result.data.InsertPrioritiesView, $scope.Result.data.UpdatePrioritiesView);
        }).error(function(error) {
          $scope.Result.type = "-1";
          $scope.Result.buttonDisabled = true;
          var errorMessage = "请确认导入类型或档案内容是否有误!! " + "</br></br>" + error.ExceptionMessage;
          $scope.Result.errorMessage = $sce.trustAsHtml(errorMessage);
        });
      };
      //上傳失敗
      uploader.onErrorItem = function(fileItem, response, status, headers) {
        $scope.Result.type = "-1";
        $scope.Result.buttonDisabled = true;
        $scope.Result.errorMessage = response.ExceptionMessage;
      };
    };
    $scope.clean = function() {
      if ($scope.Result.data !== "") {
        $scope.BackupDefer = $q.defer();
        $scope.BackupPromise = $scope.BackupDefer.promise;
        $scope.BackupPromise.then(function(data) {
          uploader.clearQueue();
          document.getElementById("uploadInput").value = "";
          $scope.Result = {
            type: 0,
            errorMessage: "",
            data: "",
            buttonDisabled: true
          };
          $("#InsertTable").bootstrapTable("destroy");
          $("#UpdateTable").bootstrapTable("destroy");
          $("#InsertTable2").bootstrapTable("destroy");
          $("#UpdateTable2").bootstrapTable("destroy");
        }, function(data) {
          alert(data);
        });
        $scope.backupCsv($scope.Result.data, $scope.FileName, $scope.ImportInfo.Type, 0);
      } else {
        uploader.clearQueue();
        document.getElementById("uploadInput").value = "";
        $scope.Result = {
          type: 0,
          errorMessage: "",
          data: "",
          buttonDisabled: true
        };
        $("#InsertTable").bootstrapTable("destroy");
        $("#UpdateTable").bootstrapTable("destroy");
        $("#InsertTable2").bootstrapTable("destroy");
        $("#UpdateTable2").bootstrapTable("destroy");
      }
    };
    $scope.copy = function() {
      document.getElementById("CopyTextarea").select();
      document.execCommand("copy");
    };
    $scope.btnCancel = function() {
      if ($scope.Result.data !== "") {
        $scope.BackupDefer = $q.defer();
        $scope.BackupPromise = $scope.BackupDefer.promise;
        $scope.BackupPromise.then(function(data) {
          window.close();
        }, function(error) {
          alert(error);
        });
        $scope.backupCsv($scope.Result.data, $scope.FileName, $scope.ImportInfo.Type, 0);
      } else {
        window.close();
      }
    };
    $scope.btnOk = function() {
      $scope.Result.buttonDisabled = true;
      ngProgress.start();
      $scope.BackupDefer = $q.defer();
      $scope.BackupPromise = $scope.BackupDefer.promise;
      $scope.BackupPromise.then(function(data) {
        if (data.Success) {
          ngProgress.complete();
          $scope.$toastlast = toastr["success"]("新增完成", "");
          setTimeout(function() {
            if (window.opener) {
              window.opener.ReloadCausePrioritieList();
              window.close();
            } else {
              window.close();
            }
          }, 1000);
        }
      }, function(error) {
        $scope.Result.buttonDisabled = false;
        alert(error);
      });
      $scope.backupCsv($scope.Result.data, $scope.FileName, $scope.ImportInfo.Type, 1);
    };
  }
]);