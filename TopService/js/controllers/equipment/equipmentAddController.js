angular.module("n580440").controller("equipmentAddCtrl", ["$scope", "$http", "$timeout", "$modal", "ngProgress", "$filter", "$sce", "$state", "$stateParams", "DataMaintenanceFactory", "$q",
  function($scope, $http, $timeout, $modal, ngProgress, $filter, $sce, $state, $stateParams, DataMaintenanceFactory, $q) {
    ngProgress.start();
    $scope.CompanyList = [];
    $scope.Info = {
      CompanyId: $scope.userInfo.CompanyId,
      EquipmentTypeClass: null,
      ApiUrl: ApiMapper.ccApi
    };
    $scope.Data = {
      CompanyId: "",
      Nickname: "",
      EquipmentName: "",
      Brand: "",
      Model: "",
      PropertyCode: "",
      Unit: "",
      UnitId: "",
      Transfer: "",
      EquipmentClass: {
        EquipmentClass: "",
        EquipmentClassName: ""
      },
      EquipmentTypeId: "",
      EquipmentTypeName: "",
      EquipmentSubTypeId: "",
      EquipmentSubTypeName: "",
      EquipmentCode: "",
      Allocation: {
        MaintainerId: "",
        MaintainerNickName: ""
      },
      Service: 0,
      Picture: 0,
      Status: 1
    };
    $scope.SelectEquipmentpos = [];
    $scope.Equipmentpos = [];
    $scope.BrandList = [];
    $scope.EquipmentTypeList = {
      EquipmentTypeClass: [],
      EquipmentTypeName: [],
      EquipmentSubTypeName: []
    };
    $scope.ModelList = [];
    $scope.CompanyList = [];

    function removeBlankOption() {
      setTimeout(function() {
        var Select2ResultLabels = document.querySelectorAll(".select2-result-label");
        for (var i = 0; i < Select2ResultLabels.length; i++) {
          if (Select2ResultLabels[i].textContent == "") {
            Select2ResultLabels[i].remove();
          }
        }
      }, 0);
    }
    // select2 樣式初始化 與 資料獲取
    $scope.init = function() {
      setTimeout(function() {
        // 品牌
        $("[name=Brand]").select2({
          placeholder: "请选择品牌",
          allowClear: true
        });
        // 大类
        $("[name=TypeClass]").select2({
          placeholder: "请选择大类"
        }).on("select2-opening", function() {
          removeBlankOption();
        }).on("select2-loaded", function() {
          removeBlankOption();
        });
        // 中类
        $("[name=TypeName]").select2({
          placeholder: "请选择中类"
        }).on("select2-opening", function() {
          removeBlankOption();
        }).on("select2-loaded", function() {
          removeBlankOption();
        });
        // 小类
        $("[name=SubTypeName]").select2({
          placeholder: "请选择小类"
        }).on("select2-opening", function() {
          removeBlankOption();
        }).on("select2-loaded", function() {
          removeBlankOption();
        });
        // 維修商
        $("[name=FormCompanyList]").select2({
          placeholder: "请选择维修商",
          allowClear: true
        });
        // 所在位置
        $("[name=Position]").select2({
          placeholder: "请选择所在位置"
        });
        $(".select2-container").css("z-index", 999);
      }, 0);
      var p1 = DataMaintenanceFactory.getBrand($scope.userInfo.Passport);
      var p2 = DataMaintenanceFactory.getModel($scope.userInfo.Passport);
      var p3 = DataMaintenanceFactory.getEquipType($scope.userInfo.Passport);
      var p4 = DataMaintenanceFactory.getEquipmentPos($scope.userInfo.Passport);
      var p5 = DataMaintenanceFactory.getCompanyList($scope.userInfo.Passport, 'p');
      var all = $q.all([p1, p2, p3, p4, p5]);
      all.then(function(res) {
        var BrandData = res[0].data;
        var ModelData = res[1].data;
        var TypeData = res[2].data;
        var PosData = res[3].data;
        var pCompanyData = res[4].data;
        // 1.品牌
        $scope.BrandList = BrandData;
        // 2.型号 (没有id,所以用input)
        var ModelList = [];
        _(ModelData).each(function(Items, index) {
          ModelList.push({
            id: Items.Model,
            text: Items.Model
          });
        });
        $("[name=Model]").select2({
          placeholder: "请选择型号",
          data: ModelList,
          allowClear: true
        });
        $(".select2-container").css("z-index", 999);
        // 3.大中小类
        $scope.EquipmentTypeList = {
          EquipmentTypeClass: [],
          EquipmentTypeName: [],
          EquipmentSubTypeName: []
        };
        $scope.EquipmentTypeList.EquipmentTypeClass = TypeData.EquipmentTypeClass;
        $scope.EquipmentTypeList.EquipmentTypeName = TypeData.EquipmentTypeName;
        $scope.EquipmentTypeList.EquipmentSubTypeName = TypeData.EquipmentSubTypeName;
        // 4.所在区域
        var Regions = _.groupBy(PosData, "Region");
        _(Regions).each(function(Items) {
          $scope.Equipmentpos.push({
            Region: Items[0].Region,
            children: Items
          });
        });
        // 5.公司别
        $scope.CompanyList = pCompanyData;
        setTimeout(function() {
          ngProgress.complete();
        }, 0);
        // 1.大中小類
        $("[name=TypeClass]").on("select2-selecting", function(e) {
          $scope.Data.EquipmentClass.EquipmentClassName = e.object.text;
          //清除 中,小類
          $scope.Data.EquipmentTypeId = "";
          $scope.Data.EquipmentSubTypeId = "";
          $scope.Data.EquipmentTypeName = "";
          $scope.Data.EquipmentSubTypeName = "";
          $("[name=TypeName]").select2("val", $scope.Data.EquipmentTypeId);
          $("[name=SubTypeName]").select2("val", $scope.Data.EquipmentSubTypeId);
        });
        $("[name=TypeName]").on("select2-selecting", function(e) {
          $scope.Data.EquipmentTypeName = e.object.text;
          // 清除 小類
          $scope.Data.EquipmentSubTypeId = "";
          $scope.Data.EquipmentSubTypeName = "";
          $("[name=SubTypeName]").select2("val", $scope.Data.EquipmentSubTypeId);
        });
        $("[name=SubTypeName]").on("select2-selecting", function(e) {
          $scope.Data.EquipmentSubTypeName = e.object.text;
        });
        // 2.預設維修商
        $("[name=FormCompanyList]").on("select2-selecting", function(e) {
          $scope.Data.Allocation.MaintainerNickName = e.object.text;
        });
      }).catch(function(e) {
        ngProgress.complete();
        alert(e.data.Message);
      });
    };
    $scope.init();
    $scope.setCompany = function() {
      var modalInstance = $modal.open({
        templateUrl: "unitsModal.html",
        controller: "ntOrgPicker",
        resolve: {
          Options: function() {
            return {
              Type: "S1",
              Source: "n580440"
            };
          },
          Info: function() {
            return $scope.Info;
          },
          UnitInfo: function() {
            return "";
          }
        }
      });
      modalInstance.result.then(function(Unit) {
        $scope.Data.Unit = Unit.Unit[0];
        $scope.Data.UnitId = Unit.UnitId[0];
        $scope.Data.CompanyId = Unit.CompanyId;
        $scope.Data.Nickname = Unit.Nickname;
      });
    };
    $scope.getEquipCode = function(unitId, equipmentTypeClass) {
      DataMaintenanceFactory.getEquipCode($scope.userInfo.Passport, unitId, equipmentTypeClass).success(function(data) {
        $scope.Data.EquipmentCode = data;
      });
    };
    $scope.btnCancel = function() {
      window.close();
    };
    $scope.btnOk = function() {
      $scope.buttonDisabled = true;
      ngProgress.start();
      var Params = {
        Equipment: {
          EquipmentName: $scope.Data.EquipmentName,
          Manufacturer: $scope.Data.Brand,
          Model: $scope.Data.Model,
          PropertyCode: $scope.Data.PropertyCode,
          UnitId: $scope.Data.UnitId,
          Unit: $scope.Data.Unit,
          CompanyId: $scope.Data.CompanyId,
          EquipmentTypeClass: $scope.Data.EquipmentClass.EquipmentClass,
          EquipmentTypeId: $scope.Data.EquipmentTypeId,
          EquipmentSubTypeId: $scope.Data.EquipmentSubTypeId,
          EquipmentCode: $scope.Data.EquipmentCode,
          IPositionIds: $scope.SelectEquipmentpos,
          IsRezPhotos: $scope.Data.Picture,
          Status: $scope.Data.Status
        },
        MaintainerId: $scope.Data.Allocation.MaintainerId,
        Service: $scope.Data.Service
      };
      DataMaintenanceFactory.createEquip($scope.userInfo.Passport, Params).success(function(data) {
        ngProgress.complete();
        $scope.$toastlast = toastr["success"]("新增完成", "");
        setTimeout(function() {
          if (window.opener) {
            window.opener.ReloadEquipmentList();
            window.close();
          } else {
            window.close();
          }
        }, 1000);
      }).error(function(error) {
        $scope.buttonDisabled = false;
        ngProgress.complete();
        alert(error.Message);
      });
    };
    // $timeout(function () {
    //     $('[name=ComponentManufacturer]').select2("val", caseData.EquipmentCode);
    //     $('[name=EquipmentList2]').select2("val", caseData.EquipmentCode);
    // }, 0);
  }
]);