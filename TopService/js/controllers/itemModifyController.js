angular.module("n580440").controller("itemModifyCtrl", ["$scope", "$http", "$modal", "$timeout", "ngProgress",
  function($scope, $http, $modal, $timeout, ngProgress) {
    $scope.open = function(size) {
      var modalInstance = $modal.open({
        templateUrl: "batchAddEquipment.html", //模板窗口的地址
        size: size, //大小配置不能自定义大小，只能是sm，md，lg等这些值
        controller: "batchAddEquipmentCtrl", //$modal指定的控制器 寫在下面
        resolve: {
          Info: function() {
            return $scope.Info;
          }
        }
      });
      modalInstance.result.then(function(result) {
        //$modalInstance.close()正常关闭后执行的函数
      });
    };
  }
]);
// 批量增新品項modal
angular.module("n580440").controller("batchAddEquipmentCtrl", ["$scope", "$http", "$modal", "$modalInstance", "$timeout", "i18n", "Info",
  function($scope, $http, $modal, $modalInstance, $timeout, i18n, Info) {
    $scope.Equipmentpos = []; //儲存設備區域
    $scope.Position = "";
    $scope.Passport = Cookies.get("Passport");
    $scope.CompanyId = Cookies.get("CompanyId");
    $scope.equipment = {
      CompanyId: $scope.CompanyId,
      UnitId: [], // 保存門店ID
      Unit: [], // 保存門店名稱
      EquipmentName: "", //设备名称
      EquipmentTypeClass: 0, //大类
      EquipmentTypeId: "", //中类
      EquipmentTypeName: "",
      EquipmentSubTypeId: "", //小类
      EquipmentSubTypeName: "",
      IPositionIds: []
    };
    $scope.EquipmentTypeList = {
      EquipmentTypeClass: [],
      EquipmentTypeName: [],
      EquipmentSubTypeName: []
    };
    $scope.openUnits = function(size) {
      // 呼叫門店modal 寫在common.js裡面
      var modalInstance = $modal.open({
        templateUrl: "unitsModal.html",
        size: size,
        controller: "ntOrgPicker",
        resolve: {
          Options: function() {
            return {
              Type: "S2",
              Source: "B2C",
              setStatus: true
            };
          },
          Info: function() {
            return Info;
          },
          UnitInfo: null
        }
      });
      modalInstance.result.then(function(Unit) {
        console.log(`Unit: ${JSON.stringify(Unit)}`);
        $scope.equipment.Unit = Unit.Unit;
        $scope.equipment.UnitId = Unit.UnitId;
        $scope.Units = Unit.Units;
      });
    };

    function removeBlankOption() {
      setTimeout(function() {
        var Select2ResultLabels = document.querySelectorAll(".select2-result-label");
        for (var i = 0; i < Select2ResultLabels.length; i++) {
          if (Select2ResultLabels[i].textContent == "") {
            Select2ResultLabels[i].remove();
          }
        }
      }, 0);
    }
    //获取设施全部分类
    $scope.GetEquipmentType = function() {
      $http({
        url: ApiMapper.sApi + "/s/equipmenttypes/company/" + $scope.equipment.CompanyId,
        method: "GET",
        contentType: "application/json",
        headers: {
          Passport: $scope.Passport
        }
      }).success(function(data) {
        $scope.EquipmentTypeList = data;
        console.log(data);
      }).error(function() {});
    };
    $scope.GetEquipmentType();
    $scope.init = function() {
      setTimeout(function() {
        // 中类
        $("[name=TypeName]").select2({
          placeholder: i18n.T("82009")
        }).on("select2-opening", function() {
          removeBlankOption();
        }).on("select2-loaded", function() {
          removeBlankOption();
        });
        // 小类
        $("[name=SubTypeName]").select2({
          placeholder: i18n.T("82008")
        }).on("select2-opening", function() {
          removeBlankOption();
        }).on("select2-loaded", function() {
          removeBlankOption();
        });
        $(".select2-container").css("z-index", 999);
        $("[name=TypeName]").on("change", function(e) {
          $scope.equipment.EquipmentTypeName = e.added.text;
          // 清除 小類
          $scope.equipment.EquipmentSubTypeId = "";
          $scope.equipment.EquipmentSubTypeName = "";
          $("[name=SubTypeName]").select2("val", $scope.equipment.EquipmentSubTypeId);
          $scope.$apply();
        });
        $("[name=SubTypeName]").on("change", function(e) {
          $scope.equipment.EquipmentSubTypeName = e.added.text;
        });
      }, 0);
    };
    $scope.init();
    //获取区域
    $http({
      url: ApiMapper.sApi + "/s/equipmentpos",
      method: "GET",
      contentType: "application/json",
      headers: {
        Passport: $scope.Passport
      },
      cache: false
    }).success(function(data) {
      $scope.Equipmentpos = eval(data);
      $timeout(function() {
        $("[name=Position]").select2({
          placeholder: i18n.T("82010")
        });
      }, 200);
    });
    // 所選擇之大類
    $scope.setEquipmentTypeClass = function(item) {
      $scope.equipment.EquipmentTypeClass = item.EquipmentTypeClass;
      $scope.equipment.EquipmentTypeId = "";
      $scope.equipment.EquipmentSubTypeId = "";
      $scope.equipment.EquipmentTypeName = "";
      $scope.equipment.EquipmentSubTypeName = "";
      $("[name=TypeName]").select2("val", $scope.equipment.EquipmentTypeId);
      $("[name=SubTypeName]").select2("val", $scope.equipment.EquipmentSubTypeId);
    };
    $scope.btnOk = function() {
      $scope.alertStr = "";
      if (!angular.isDefined($scope.equipment.EquipmentName)) {
        $scope.alertStr += "\r\n" + i18n.T("82011");
      }
      if (!angular.isDefined($scope.Position) || $scope.Position == "") {
        $scope.alertStr += "\r\n" + i18n.T("82013");
      }
      if ($scope.alertStr != "") {
        $scope.$toastlast = toastr["error"]($scope.alertStr, "");
        return false;
      }
      $scope.RegionId = [];
      if ($scope.Position.length > 0) {
        _($scope.Equipmentpos).each(function(Item) {
          if ($scope.Position.indexOf(Item.PositionId) >= 0) {
            $scope.RegionId.push(Item.RegionId);
            $scope.RegionId.push(Item.PositionId);
          }
        });
        $scope.equipment.IPositionIds = $scope.RegionId;
      }
      // 配合傳遞資料必須改變陣列結構
      // $scope.Units = [];
      // $scope.equipment.UnitId.forEach(UnitId => {
      //   $scope.Units.push({
      //     'UnitId': UnitId
      //   })
      // });
      // $scope.Units.forEach((item, index1) => {
      //   $scope.equipment.Unit.forEach((UnitIds, index2) => {
      //     if (index1 === index2) {
      //       item.Unit = UnitIds
      //     } else {
      //       return
      //     }
      //   });
      // });
      var params = {
        IsStore: 0,
        Units: $scope.Units,
        EquipmentName: $scope.equipment.EquipmentName, // 设备名称
        EquipmentTypeClass: $scope.equipment.EquipmentTypeClass, // 大类
        EquipmentTypeId: $scope.equipment.EquipmentTypeId, // 中类
        EquipmentSubTypeId: $scope.equipment.EquipmentSubTypeId, // 小类
        PositionIds: $scope.equipment.IPositionIds // 區域
      };
      console.log(`給後端的資料${JSON.stringify(params)}`);
      //新增设备
      jQuery.ajax({
        url: ApiMapper.sApi + "/s/equipment/batch",
        type: "POST",
        async: false,
        contentType: "application/json",
        headers: {
          Passport: $scope.Passport
        },
        data: JSON.stringify(params),
        success: function(data) {
          var str = "";
          data.forEach(data => {
            if (data.Receipt.StatusCode == 1) {
              str += `${data.Info}${i18n.T("82014")}\n`;
              toastr.options.timeOut = 5000;
              toastr.options.extendedTimeOut = 1000;
              toastr["success"](`${data.Info}${i18n.T("82014")}`, "");
              $modalInstance.close($scope.equipment.EquipmentTypeId);
            } else {
              str += `${data.Info}\n${data.Receipt.Messages}\n`;
              toastr.options.timeOut = 0;
              toastr.options.extendedTimeOut = 0;
              toastr["error"](`${data.Receipt.Messages}`, `${data.Info}`);
            }
          });
          //alert(str);// 關閉modal後傳給前面modal的資料 目前沒用到
        },
        error: function(res) {
          toastr["error"](res.Messages, "");
        }
      });
    };
    $scope.btnCancel = function() {
      $modalInstance.dismiss("cancel");
    };
  }
]);