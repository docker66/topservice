n580440.controller('ntDivisionsPicker', ['$scope', '$http', '$modalInstance', '$timeout', function ($scope, $http, $modalInstance, $timeout) {
    $scope.DivisionsData = {};
    $scope.Company = {};
    $scope.Area = {};
    $scope.City = {};
    $scope.Province = {};
    $scope.Divisions = [];
    $scope.DivisionName;
    $scope.Companys = [];
    $scope.Areas = [];
    $scope.Provinces = [];
    $scope.Cities = [];
    $scope.isHide = false;
    $scope.isHide = true;
    $scope.CheckLevel = { 'Level1': '', 'Level2': '', 'Level3': '', 'Level4': '' };
    $scope._Unit = { 'Region': '', 'Province': '', 'City': '', 'District': '' };

    $http.get('AdministrativeDivisions.js')
        .success(function (DivisionsData) {
            $scope.DivisionsData = DivisionsData;
            $scope.Companys = _.filter($scope.DivisionsData[0]);
            $scope.Company = _.find($scope.Companys);
            $scope.Areas = _.filter($scope.DivisionsData[1], function (Items) { return Items.ReferenceCode == $scope.Company.DivisionCode; });
            $scope.Area = _.find($scope.Areas);
            $scope.Provinces = _.filter($scope.DivisionsData[2], function (Items) { return Items.ReferenceCode == $scope.Area.DivisionCode; });
            $scope.Province = _.find($scope.Provinces);
            $scope.Cities = _.filter($scope.DivisionsData[3], function (Items) { return Items.ReferenceCode == $scope.Province.DivisionCode; });
            $('#Unit01,#Unit02,#Unit03,#Unit04').slimScroll({ height: '300px' });
        });

    $scope._CompanyID = [];
    $scope._AreaID = [];
    $scope._ProvinceID = [];
    $scope._CityID = [];
    $scope.isChecked = function (EventID, id) {
        if (EventID == "Company") {
            return $scope._CompanyID.indexOf(id);
        } else if (EventID == "Area") {
            return $scope._AreaID.indexOf(id);
        } else if (EventID == "Province") {
            return $scope._ProvinceID.indexOf(id);
        } else {
            return $scope._CityID.indexOf(id);
        }
    };
    //选中
    $scope.toSelected = function (EventID, id, ReferenceCode) {
        if (EventID == "Company") {
            $scope._CompanyID = [id];
            $scope._AreaID = []; $scope._ProvinceID = []; $scope._CityID = [];
        } else if (EventID == "Area") {
            $scope._CompanyID = [ReferenceCode];
            $scope._AreaID = [id];
            $scope._ProvinceID = []; $scope._CityID = [];
        } else if (EventID == "Province") {
            $scope._AreaID = [ReferenceCode];
            $scope._ProvinceID = [id];
            $scope._CityID = [];
            $scope.Area = _.find($scope.Areas, function (Item) {
                return Item.DivisionCode == ReferenceCode;
            });
            $scope._CompanyID = [$scope.Area.ReferenceCode];
        } else {
            $scope._CityID = [id];
            $scope._ProvinceID = [ReferenceCode];
            $scope.Province = _.find($scope.Provinces, function (Item) {
                return Item.DivisionCode == ReferenceCode;
            });
            $scope._AreaID = [$scope.Province.ReferenceCode];
            $scope.Area = _.find($scope.Areas, function (Item) {
                return Item.DivisionCode == $scope._AreaID;
            });
            $scope._CompanyID = [$scope.Area.ReferenceCode];
        }
        $scope.LoadUnits(EventID, id);
    };
    $scope.unSelected = function (EventID, id, ReferenceCode) {
        $scope.DivisionName = { 'Company': '', 'Area': '', 'Province': '', 'City': '' };
        $scope._CompanyID = [];
        $scope._AreaID = [];
        $scope._ProvinceID = [];
        $scope._CityID = [];
    };
    //加载4层架构
    $scope.LoadUnits = function (EventID, id) {
        if (EventID == "Company") {
            $scope.Company = _.find($scope.Companys, function (Items) { return Items.DivisionCode == id; });
            $scope.Areas = _.filter($scope.DivisionsData[1], function (Items) { return Items.ReferenceCode == id; });
            $scope.Area = _.find($scope.Areas);
            $scope.Provinces = _.filter($scope.DivisionsData[2], function (Items) { return Items.ReferenceCode == $scope.Area.DivisionCode; });
            $scope.Province = _.find($scope.Provinces);
            $scope.Cities = _.filter($scope.DivisionsData[3], function (Items) { return Items.ReferenceCode == $scope.Province.DivisionCode; });
        } else if (EventID == "Area") {
            $scope.Company = _.find($scope.Companys, function (Items) { return Items.DivisionCode == $scope._CompanyID[0]; });
            $scope.Area = _.find($scope.Areas, function (Items) { return Items.DivisionCode == id; });
            $scope.Provinces = _.filter($scope.DivisionsData[2], function (Items) { return Items.ReferenceCode == id; });
            $scope.Province = _.find($scope.Provinces);
            $scope.Cities = _.filter($scope.DivisionsData[3], function (Items) { return Items.ReferenceCode == $scope.Province.DivisionCode; });
        } else if (EventID == "Province") {
            $scope.Company = _.find($scope.Companys, function (Items) { return Items.DivisionCode == $scope._CompanyID[0]; });
            $scope.Area = _.find($scope.Areas, function (Items) { return Items.DivisionCode == $scope._AreaID[0]; });
            $scope.Province = _.find($scope.Provinces, function (Items) { return Items.DivisionCode == id; });
            $scope.Cities = _.filter($scope.DivisionsData[3], function (Items) { return Items.ReferenceCode == id; });
        } else {
            $scope.Company = _.find($scope.Companys, function (Items) { return Items.DivisionCode == $scope._CompanyID[0]; });
            $scope.Area = _.find($scope.Areas, function (Items) { return Items.DivisionCode == $scope._AreaID[0]; });
            $scope.Province = _.find($scope.Provinces, function (Items) { return Items.DivisionCode == $scope._ProvinceID[0]; });
            $scope.City = _.find($scope.Cities, function (Items) { return Items.DivisionCode == id; });
        }
        $('#Unit01,#Unit02,#Unit03,#Unit04').slimScroll({ height: '300px' });
    };
    //复选框click事件
    $scope.ItemsClick = function (EventID, itemID, ReferenceCode) {
        var isChecked = $scope.isChecked(EventID, itemID);
        if (isChecked < 0) {
            $scope.toSelected(EventID, itemID, ReferenceCode);
        } else {
            $scope.unSelected(EventID, itemID, ReferenceCode);
        }
    };

    $scope.ok = function () {
        if ($scope._CityID.length == 0) {
            alert('请选择区域!');
            return false;
        }
        $scope._Unit.Region = $scope.Company.DivisionName;
        $scope._Unit.Province = $scope.Area.DivisionName;
        $scope._Unit.City = $scope.Province.DivisionName;
        $scope._Unit.District = $scope.City.DivisionName;
        $modalInstance.close($scope._Unit);
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}])
    ;