n580440.controller('ntUsersPicker', ['$scope', '$http', '$modalInstance', '$timeout', 'Passport', 'CompanyInfo', 'Type', function ($scope, $http, $modalInstance, $timeout, Passport, CompanyInfo, Type) {
    $scope.UserList = [];
    $scope.IsLoading = true; 
    $scope.CompanyInfo = CompanyInfo;
    $scope.selectItem = '';

    $scope.select = function (item) {
        $scope.selectItem = item;
    };

    $scope.getsUserList = function (CompanyId) {
        var Url = '';
        if (Type == 'S') {
            Url = ApiMapper.ccApi + '/g/management/SUnit/User/' + CompanyId;
        } else {
            Url = ApiMapper.ccApi + '/g/management/PUnit/User/' + CompanyId;
        };
        $http({
                url: Url,
                method: 'Get',
                contentType: 'application/json',
                headers: {
                    'Passport': Passport
                }
            })
            .success(function (data) {
                $scope.IsLoading = false;
                $scope.UserList = data;
            }).error(function (error) {
                alert(error.Messages);
            })
    };
    $scope.getsUserList($scope.CompanyInfo.CompanyId);

    $scope.ok = function () {
        if($scope.selectItem==''){
            $modalInstance.close({
                'HrCode': '',
                'UserName': ''
            });
        }else{
            $modalInstance.close($scope.selectItem);
        };
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);