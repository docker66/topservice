n580440.controller('storeModalCtrl', function($scope, $modalInstance, $type, SearchParams, Passport, IsHideUnit, $http, $timeout) {
  // 報修單 = 1 巡檢案件 = 2 報價單 = 3 請款單 = 4 發票 = 5
  $scope.Passport = Passport;
  //服務門店清單
  $scope.ServeStoreRadio = SearchParams.CompanyId;
  $scope.ServeStoreListData = "";
  $scope.SelectStoreList = [];
  $scope.ShowStoreList = [];
  $scope.ShowNickname = "";
  $scope.IsFirstLoading = true;
  $scope.IsHideUnit = IsHideUnit;
  $scope.CompanyListSearch = "";
  $scope.hasOtherCompany = SearchParams.hasOtherCompany;
  $scope.ModalTitle = "选择客户/门店";
  if ($scope.IsHideUnit) {
    $scope.ModalTitle = "选择客户";
  };
  // slimScroll 初始化
  $timeout(function() {
    $("#inner-content").slimScroll({
      height: '250px'
    });
  }, 200);
  //-------------------------------------------------//
  //-------------------- 選擇門店 --------------------//
  //-------------------------------------------------//
  $scope.selectServeStore = function() {
    $scope.modalTitle = '服务对象';
    $scope.modalsubTitle = '请输入门店关键字';
    var ServeStoreStrCheck = [];
    //[p-0025]获取维修商服务对象清单
    $http({
      url: ApiMapper.pApi + '/p/mcompany',
      method: 'GET',
      contentType: 'application/json',
      headers: {
        'Passport': $scope.Passport
      },
    }).success(function(data) {
      // console.log(data);
      //將data加入isCheck的屬性 --- 多選時會應用到
      angular.forEach(data, function(value, key) {
        ServeStoreStrCheck.push({
          'CompanyId': value.CompanyId,
          'CompanyName': value.CompanyName,
          'Nickname': value.Nickname,
          'isCheck': false
        });
      });
      if ($scope.hasOtherCompany) {
        ServeStoreStrCheck.push({
          'CompanyId': '00000000-0000-0000-0000-000000000000',
          'CompanyName': '其他',
          'Nickname': '其他',
          'isCheck': false
        });
      };
      $scope.ServeStoreStr = ServeStoreStrCheck;
      _($scope.ServeStoreStr).each(function(Items) {
        if (Items.CompanyId == $scope.ServeStoreRadio) {
          Items.isCheck = true;
        } else {
          Items.isCheck = false;
        }
      });
      if (data.length == 1) {
        $scope.ServeStoreRadio = ServeStoreStrCheck[0].CompanyId;
        $scope.selectServeStoreList(ServeStoreStrCheck[0].CompanyId);
      }
      //執行選單select2
      $('[name=StoreListSearch]').select2({
        placeholder: $scope.modalsubTitle
      });
      if ($scope.ServeStoreRadio != '') {
        $scope.selectServeStoreList($scope.ServeStoreRadio);
      }
    });
  };
  $scope.selectServeStore();
  //获取维修商服务对象門店 Api
  $scope.selectServeStoreList = function(selectStoreID) {
    _($scope.ServeStoreStr).each(function(Items) {
      if (Items.CompanyId == selectStoreID) {
        Items.isCheck = true;
      } else {
        Items.isCheck = false;
      }
    });
    $scope.ServeStoreRadio = selectStoreID;
    //清空暫存
    $scope.SelectStoreList = [];
    $scope.ServeStoreListData = [];
    var ServeStoreListDatastr = [];
    SelectStoreListArr = [];
    //[p-0026]获取单一服务对象门店清单
    //  報修單 = 1 巡檢案件 = 2 報價單 = 3 請款單 = 4 發票 = 5
    $scope.isLoading = true;
    $http({
      url: ApiMapper.pApi + '/p/mcompany/unit/' + selectStoreID + '/' + $type,
      method: 'GET',
      contentType: 'application/json',
      headers: {
        'Passport': $scope.Passport
      },
    }).success(function(data) {
      //將data加入isCheck的屬性 --- 多選時會應用到
      angular.forEach(data, function(value, key) {
        ServeStoreListDatastr.push({
          'UnitId': value.UnitId,
          'Unit': value.Unit,
          'UnitLevel': value.UnitLevel,
          'ReferenceUnitId': value.ReferenceUnitId,
          'Status': value.Status,
          'isCheck': false
        })
      });
      if ($scope.IsFirstLoading) {
        angular.forEach(SearchParams.UnitIds, function(value, key) {
          angular.forEach(ServeStoreListDatastr, function(value2, key2) {
            if (value2.UnitId == value) {
              ServeStoreListDatastr[key2].isCheck = true;
            }
          });
        });
        $scope.IsFirstLoading = false;
      }
      //ng-repeat 帶入門店清單
      // $scope.ServeStoreListData = ServeStoreListDatastr;
      $scope.isLoading = false;
      $scope.ServeStoreListData = ServeStoreListDatastr;
    });
  }
  //清空门店选择
  $scope.restStoreSelect = function(CompID) {
    //清空服務對象
    $scope.ServeStoreRadio = '';
    //清空門店清單
    $scope.ServeStoreListData = [];
    //刪除已選取的門店
    $scope.SelectStoreList = [];
    //清空暫存陣列
    SelectStoreListArr = [];
    var parameters = {
      SelectStoreList: '',
      ServeStoreStr: '',
      ServeStoreRadio: '',
      ServeStoreListData: '',
      SelectStoreList: ''
    }
    parameters = {
      SelectStoreList: SelectStoreListArr,
      ServeStoreStr: $scope.ServeStoreStr,
      ServeStoreRadio: $scope.ServeStoreRadio,
      ServeStoreListData: $scope.ServeStoreListData,
      SelectStoreList: $scope.SelectStoreList
    }
    $modalInstance.close(parameters);
  };
  //储存公司名称
  $scope.saveStoreSelect = function() {
    //服務公司名稱替換
    $scope.CompanyId = $scope.ServeStoreRadio;
  };
  //暫存陣列
  var SelectStoreListArr = [];
  //暫存已選取門店
  $scope.loadSelectStore = function(Uid, id, check) {
    $scope.SelectStoreList = [];
    SelectStoreListArr = [];
    angular.forEach($scope.ServeStoreListData, function(value, key) {
      if (value.isCheck == true || value.isCheck == 'true') {
        SelectStoreListArr.push(value.UnitId);
      }
    });
    var parameters = {
      SelectStoreList: '',
      ServeStoreStr: '',
      ServeStoreRadio: '',
      ServeStoreListData: '',
      SelectStoreList: ''
    }
    parameters = {
      SelectStoreList: SelectStoreListArr,
      ServeStoreStr: $scope.ServeStoreStr,
      ServeStoreRadio: $scope.ServeStoreRadio,
      ServeStoreListData: $scope.ServeStoreListData,
      SelectStoreList: SelectStoreListArr
    }
    $modalInstance.close(parameters);
  };
  //-------------------- 选择门店 END --------------------//
});