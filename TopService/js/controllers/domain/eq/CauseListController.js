angular.module('n580440').controller('CauseListController', ['$scope', '$http', '$timeout', '$modal', 'ngProgress', '$filter', '$sce', '$state', '$stateParams', 'DataMaintenanceFactory', '$q', function($scope, $http, $timeout, $modal, ngProgress, $filter, $sce, $state, $stateParams, DataMaintenanceFactory, $q) {
  ngProgress.start();
  $scope.CsvButtonDisabled = false;
  $scope.ActiveItem = '';
  $scope.currentPage = 0;
  $scope.CompanyList = [];
  $scope.pCompanyList = [];
  $scope.CausePrioritieList = [];
  $scope.SearchParams = {
    'sCompanyId': '',
    'pCompanyId': '',
    'Filtration': ''
  };
  $scope.init = function() {
    setTimeout(function() {
      // 报修公司
      $('[name=sCompanyList]').select2({
        placeholder: "报修公司",
        allowClear: true
      });
      // 维修商
      $('[name=pCompanyList]').select2({
        placeholder: "维修商",
        allowClear: true
      });
      $('.select2-container').css('z-index', 999);
    }, 0);
    var p1 = DataMaintenanceFactory.getsCompanyList($scope.userInfo.Passport);
    var p2 = DataMaintenanceFactory.getpCompanyList($scope.userInfo.Passport);
    var all = $q.all([p1, p2]);
    all.then(function(res) {
      var sCompanyListData = res[0].data;
      var pCompanyListData = res[1].data;
      $scope.sCompanyList = sCompanyListData;
      $scope.pCompanyList = pCompanyListData;
      ngProgress.complete();
    }).catch(function(e) {
      ngProgress.complete();
      alert(e.data.Message);
    });
  };
  $scope.init();
  // 下载板模
  $scope.downloadExcel = DataMaintenanceFactory.downloadExcel;
  // 新开页面-刷新页面用
  window.ReloadCausePrioritieList = function() {
    if ($scope.currentPage !== 0) {
      $scope.Search($scope.currentPage);
    };
  };
  $scope.Search = function(PageNo) {
    $scope.currentPage = PageNo;
    ngProgress.start();
    $scope.CausePrioritieList = [];
    var Params = {
      'Filtration': $scope.SearchParams.Filtration,
      'SCompanyId': $scope.SearchParams.sCompanyId,
      'PCompanyId': $scope.SearchParams.pCompanyId
    };
    DataMaintenanceFactory.getCausePrioritieList($scope.userInfo.Passport, PageNo, Params).success(function(data) {
      $scope.CausePrioritieList = data[1];
      //總數量
      $scope.totalItems = data[2];
      //是否顯示頁碼
      $scope.PaginationisShow = data[2] > 20 ? true : false;
      ngProgress.complete();
    }).error(function(error) {
      alert(error.Message);
      ngProgress.complete();
    });
  };
  $scope.pageChanged = function(e) {
    $scope.Search(e);
  };
  $scope.getCsv = function(item) {
    $scope.CsvButtonDisabled = true;
    $scope.ActiveItem = item;
    var Params = {
      'MaintainerId': item.MaintainerId,
      'MaintainerNickName': item.MaintainerNickName,
      'EquipmentSubTypeId': item.EquipmentSubTypeId,
      'EquipmentSubTypeName': item.EquipmentSubTypeName,
      'CompanyId': item.CompanyId,
      'CompanyName': item.CompanyName
    };
    DataMaintenanceFactory.getCausesPrioritiesCsv($scope.userInfo.Passport, Params).success(function(data, status, headers) {
      headers = headers();
      var FileName = decodeURIComponent(headers['x-filename']);
      var Data = '\ufeff' + data;
      var csvData = new Blob([Data], {
        type: 'text/csv;charset=utf-8;'
      });
      //IE11 & Edge
      if (navigator.msSaveBlob) {
        navigator.msSaveBlob(csvData, FileName);
      } else {
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(csvData);
        link.setAttribute('download', FileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
      $scope.CsvButtonDisabled = false;
      $scope.ActiveItem = '';
    }).error(function(error) {
      $scope.CsvButtonDisabled = false;
      $scope.ActiveItem = '';
      alert(error.Message);
    })
  };
  $scope.importCsv = function(item) {
    var ImportInfo = {
      'Title': '原因及时限导入',
      'Type': '1'
    };
    var Params = {
      CompanyId: item.CompanyId,
      CompanyName: item.CompanyName,
      EquipmentSubTypeId: item.EquipmentSubTypeId,
      EquipmentSubTypeName: item.EquipmentSubTypeName,
      MaintainerId: item.MaintainerId,
      MaintainerNickName: item.MaintainerNickName
    };
    var importCsv = $modal.open({
      animation: true,
      templateUrl: 'importCsv.html',
      controller: 'importCsvController',
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      resolve: {
        ImportInfo: function() {
          return ImportInfo;
        },
        Params: function() {
          return Params;
        },
        Passport: function() {
          return $scope.userInfo.Passport;
        }
      }
    });
    importCsv.result.then(function(result) {
      if (result == 'success') {
        window.ReloadCausePrioritieList();
      };
    });
  };
}]);