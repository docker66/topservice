n580440.controller("ChangePwdCtl", [
  "$scope",
  "$http",
  "$modal",
  "$modalInstance",
  "$timeout",
  function($scope, $http, $modal, $modalInstance, $timeout) {
    $scope.User = {
      UserId: Cookies.get("uID"),
      oPassword: "",
      nPassword: "",
      cPassword: ""
    };
    $scope.Change4Password = eval(Cookies.get("Change4Password"));
    $scope.errorMassage = "新密码两次输入不一致，请重新输入！";
    $scope.isSuccess = false;
    var Func_Timeouter_Lock;
    $scope.Repair_Time = function(Tag) {
      if ($scope.TimesForLock == 200) {
        $timeout.cancel(Func_Timeouter_Lock);
        $scope.TimesForLock = 0;
        $scope.ChecknewPassword();
      } else {
        $scope.TimesForLock = $scope.TimesForLock + 1;
        Func_Timeouter_Lock = $timeout(function() {
          $scope.Repair_Time();
        }, 1);
      }
    };
    $scope.ChangePassword = function() {
      $scope.TimesForLock = 0;
      $timeout.cancel(Func_Timeouter_Lock);
      $scope.Repair_Time();
    };
    $scope.ChecknewPassword = function() {
      if ($scope.User.nPassword != "") {
        $http({
          url: ApiMapper.ccApi + "/cc/check/password/" + $scope.User.nPassword,
          method: "GET",
          contentType: "application/json",
          headers: { Passport: Cookies.get("Passport") }
        }).success(function(data) {
          if (!data) {
            $scope.errorMassage = "新密格式不正确，请重新输入！";
            $scope.isSuccess = true;
          } else {
            $scope.isSuccess = false;
          }
        });
      }
      if (
        ($scope.Change4Password &&
          $scope.User.oPassword != "" &&
          $scope.User.cPassword != "") ||
        !$scope.Change4Password
      ) {
        if ($scope.User.nPassword != $scope.User.cPassword) {
          $scope.errorMassage = "新密码两次输入不一致，请重新输入！";
          $scope.isSuccess = true;
        } else {
          $scope.isSuccess = false;
        }
      }
    };
    $scope.ok = function() {
      if (!$scope.Change4Password && $scope.User.oPassword == "") {
        alert("请输入原密码！");
        return false;
      }
      if ($scope.User.nPassword != $scope.User.cPassword) {
        $scope.isSuccess = true;
      } else {
        $http({
          url: ApiMapper.ccApi + "/cc/check/password/" + $scope.User.nPassword,
          method: "GET",
          contentType: "application/json",
          headers: { Passport: Cookies.get("Passport") }
        }).success(function(data) {
          if (!data) {
            $scope.errorMassage = "新密格式不正确，请重新输入！";
            $scope.isSuccess = true;
          } else {
            //执行修改密码
            var ApiStr =
              ApiMapper.ccApi + "/s/user/identity/" + $scope.User.nPassword;
            if ($scope.Change4Password) {
              ApiStr =
                ApiMapper.sApi +
                "/s/user/identity/update/" +
                $scope.User.UserId +
                "/" +
                md5($scope.User.nPassword);
            }
            $http({
              url: ApiStr,
              method: "PATCH",
              contentType: "application/json",
              headers: { Passport: Cookies.get("Passport") },
              data: {
                id: $scope.User.UserId,
                Password: md5($scope.User.nPassword)
              }
            })
              .success(function(data) {
                if (data.StatusCode != 1) {
                  alert(data.Messages);
                } else {
                  Cookies.get("Change4Password", false, { path: "/" });
                  alert("密码修改成功！");
                  $modalInstance.close($scope.User);
                }
              })
              .error(function(data) {
                alert(data.Messages);
              });
          }
        });
      }
    };
    $scope.cancel = function() {
      $modalInstance.dismiss("cancel");
    };
  }
]);
n580440.controller("ConfirmCodeCtl", [
  "$scope",
  "$http",
  "$modal",
  "$modalInstance",
  "$timeout",
  function($scope, $http, $modal, $modalInstance, $timeout) {
    $scope.UnitId = Cookies.get("UnitId");
    $scope.Unit = Cookies.get("Unit");
    $scope.UserId = Cookies.get("uID");
    $scope.StoreUser = Cookies.get("StoreUser");
    $scope.Passport = Cookies.get("Passport");
    $scope.PassCode = "";

    $scope.ok = function() {
      $http({
        url: ApiMapper.sApi + "/s/user/validtime/" + $scope.UserId,
        method: "PATCH",
        contentType: "application/json",
        headers: { Passport: $scope.Passport },
        data: { UnitId: $scope.UnitId, PassCode: $scope.PassCode }
      })
        .success(function(data) {
          if (data.StatusCode == 1) {
            alert("门店密码确认成功！");
            Cookies.get("ExpiredTime", data.Messages, { path: "/" });
            $modalInstance.dismiss("");
          } else {
            alert(data.Messages);
          }
        })
        .error(function(data) {
          //alert("获取Ticket失败！");
        });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss("cancel");
    };
  }
]);
n580440.controller("ChangeUnitCtl", [
  "$scope",
  "$http",
  "$modal",
  "$modalInstance",
  "$timeout",
  function($scope, $http, $modal, $modalInstance, $timeout) {
    $scope.UnitId = Cookies.get("UnitId");
    $scope.Unit = Cookies.get("Unit");
    $scope.UserId = Cookies.get("uID");
    $scope.StoreUser = Cookies.get("StoreUser");
    $scope.Passport = Cookies.get("Passport");
    $scope.QRCodeStr = "";
    $scope.UnitInfo = { Name: "", Id: "", PassCode: "" };
    $scope.ChangeTag = 0;
    $scope.format = "yyyy/MM/dd";
    $scope.RequestTime = { StartDate: "", EndDate: "" };
    $scope.supportInfo = {};
    $scope.supportShow = false;
    $scope.SupportList = [];

    $scope.dateOptions = {
      format: "yyyy/mm/dd",
      startingDay: 1,
      class: "datepicker"
    };

    //日期选单
    $scope.openStartDate = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.StartOpened = true;
    };
    $scope.openEndDate = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.EndOpened = true;
    };
    //获取人员支援信息
    $scope.getSupportInfo = function() {
      $http({
        url: ApiMapper.sApi + "/s/user/records/" + $scope.UserId,
        method: "GET",
        contentType: "application/json",
        headers: { Passport: $scope.Passport }
      })
        .success(function(data) {
          if (data.length > 0) {
            _(data).each(function(Item) {
              if (
                ValidtorTime(Item.StartTime, GetNow()) &&
                ValidtorTime(GetNow(), Item.EndTime)
              ) {
                Item.StartTime = FormatDATE(Item.StartTime);
                Item.EndTime = FormatDATE(Item.EndTime);
                $scope.supportInfo = Item;
                $scope.supportShow = true;
                $scope.SupportList.push(Item);
              }
            });
          }
        })
        .error(function(data) {
          alert(data.Messages);
        });
    };
    $scope.getSupportInfo();
    $scope.ok = function() {
      $scope.ApiStr = ApiMapper.sApi + "/s/user/transfer/" + $scope.UserId; //人员调店
      $scope.PostParames = {
        Source: 2,
        UnitId: $scope.UnitInfo.Id,
        PassCode: $scope.UnitInfo.PassCode
      };
      $scope.alertStr = "是否调店至";
      if ($scope.UnitInfo.Id.toUpperCase() == $scope.UnitId.toUpperCase()) {
        if (parseInt($scope.ChangeTag) == 0) {
          alert("调职门店和当前门店一样，不需要调店！");
          return false;
        } else {
          alert("支援门店和当前门店一样，不允许操作！");
          return false;
        }
      }
      if (parseInt($scope.ChangeTag) == 1) {
        $scope.ApiStr = ApiMapper.sApi + "/s/user/support/" + $scope.UserId; //支援
        $scope.alertStr = "是否支援门店：";
        if (
          $scope.RequestTime.StartDate == "" ||
          $scope.RequestTime.EndDate == ""
        ) {
          alert("请选择支援起迄日期！");
          return false;
        } else if (ValidtorTime($scope.RequestTime.EndDate, GetNow())) {
          alert("结束时间需晚于当前时间！");
          return false;
        } else {
          $scope.PostParames = {
            Info: {
              Source: 2,
              UnitId: $scope.UnitInfo.Id,
              StartTime: $scope.RequestTime.StartDate,
              EndTime: $scope.RequestTime.EndDate
            },
            PassCode: $scope.UnitInfo.PassCode
          };
        }
      }
      $http({
        url: ApiMapper.sApi + "/s/unit/" + $scope.UnitInfo.Id,
        contentType: "application/json",
        headers: { Passport: $scope.Passport },
        method: "GET"
      }).success(function(data) {
        $scope.UnitName = data.Unit;
        if (data.StatusCode != "null" && data.StatusCode != null) {
          $scope.isSubmit = false;
          alert("门店编号不对，请重新输入！");
        } else {
          if (parseInt($scope.ChangeTag) == 1) {
            $scope.PostParames.Info.Unit = data.Unit;
          } else {
            $scope.PostParames.Unit = data.Unit;
          }
          if (confirm($scope.alertStr + "<" + data.Unit + ">")) {
            $http({
              url: $scope.ApiStr,
              method: "PATCH",
              contentType: "application/json",
              headers: { Passport: $scope.Passport },
              data: $scope.PostParames
            })
              .success(function(data) {
                if (data.StatusCode == 1 && parseInt($scope.ChangeTag) == 1) {
                  alert("支援门店<" + $scope.UnitName + ">成功！");
                  $modalInstance.close("");
                } else if (
                  data.StatusCode == 1 &&
                  parseInt($scope.ChangeTag) == 0
                ) {
                  Cookies.get("UnitId", $scope.UnitInfo.Id, { path: "/" });
                  Cookies.get("Unit", $scope.UnitName, { path: "/" });
                  alert("成功调店至<" + $scope.UnitName + ">！");
                  $modalInstance.close("");
                } else {
                  alert(data.Messages);
                }
              })
              .error(function(data) {
                alert(data.Messages);
              });
          } else {
            $scope.UnitInfo.Unit = "";
            $scope.isSubmit = false;
          }
        }
      });
    };
    $scope.cancel = function() {
      $modalInstance.dismiss("cancel");
    };
  }
]);
