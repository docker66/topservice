angular.module("n580440").controller("organizeUnitListCtrl", ["$scope", "$http", "$timeout", "$modal", "ngProgress", "$filter", "$sce", "$state", "$stateParams", "DataMaintenanceFactory", "$q","i18n",
  function($scope, $http, $timeout, $modal, ngProgress, $filter, $sce, $state, $stateParams, DataMaintenanceFactory, $q, i18n) {
    $scope.currentPage = 0; // 0:还没查询过
    $scope.SearchParams = {
      CompanyId: $scope.userInfo.CompanyId,
      Filtration: ""
    };
    $scope.CsvButtonDisabled = false;
    //区分需求端和服务端;2:需求端 3:服务端
    var apiType = $scope.userInfo.apiType;
    // 下载板模
    $scope.downloadExcel = DataMaintenanceFactory.downloadExcel;
    window.ReloadStoreUnitList = function(params) {
      if ($scope.currentPage !== 0) {
        $scope.Search($scope.currentPage);
      }
    };
    // 查询
    $scope.Search = function(PageNo) {
      $scope.currentPage = PageNo;
      ngProgress.start();
      $scope.UnitList = [];
      DataMaintenanceFactory.getUnit($scope.userInfo.Passport, apiType, PageNo, $scope.SearchParams).success(function(data) {
        $scope.UnitList = data[1];
        //總數量
        $scope.totalItems = data[2];
        //是否顯示頁碼
        $scope.PaginationisShow = data[0] > 1 ? true : false;
        ngProgress.complete();
      }).error(function(error) {
        ngProgress.complete();
      });
    };
    $scope.Search(1);
    $scope.pageChanged = function(v) {
      $scope.Search(v);
    };
    $scope.openModify = function(id) {
      var urlStr = "organize/punit/edit";
      if($scope.userInfo.apiType == 's'){
        urlStr = "#/organize/sunit/edit/";
      }else{
        urlStr = "#/organize/punit/edit/";
      }
      window.open(urlStr + id);
    };
    $scope.openAdd = function(){
      var urlStr = "organize/punit/add";
      if($scope.userInfo.apiType == 's'){
        urlStr = "#/organize/sunit/add";
      }else{
        urlStr = "#/organize/punit/add";
      }
      window.open(urlStr);
    };
    $scope.importCsv = function() {
      var ImportInfo = {
        Title: "报修单位导入",
        Type: "2"
      };
      var importCsv = $modal.open({
        animation: true,
        templateUrl: "importCsv.html",
        controller: "importCsvController",
        size: "lg",
        backdrop: "static",
        keyboard: false,
        resolve: {
          ImportInfo: function() {
            return ImportInfo;
          },
          Params: function() {
            return "";
          },
          Passport: function() {
            return $scope.userInfo.Passport;
          }
        }
      });
      importCsv.result.then(function(result) {
        if (result == "success") {
          window.ReloadStoreUnitList();
        }
      });
    };
    $scope.getCsv = function() {
      $scope.CsvButtonDisabled = true;
      var Params = {
        Filtration: $scope.SearchParams.Filtration
      };
      DataMaintenanceFactory.getUnitCsv[apiType]($scope.userInfo.Passport, Params).success(function(data, status, headers) {
        headers = headers();
        var FileName = decodeURIComponent(headers["x-filename"]);
        var Data = "\ufeff" + data;
        var csvData = new Blob([Data], {
          type: "text/csv;charset=utf-8;"
        });
        //IE11 & Edge
        if (navigator.msSaveBlob) {
          navigator.msSaveBlob(csvData, FileName);
        } else {
          var link = document.createElement("a");
          link.href = window.URL.createObjectURL(csvData);
          link.setAttribute("download", FileName);
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
        $scope.CsvButtonDisabled = false;
      }).error(function(error) {
        $scope.CsvButtonDisabled = false;
        alert(error.Message);
      });
    };
  }
]);