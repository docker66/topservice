angular.module('n580440').controller('addMaintainerUnitController', ['$scope', '$http', '$timeout', '$modal', 'ngProgress', '$filter', '$sce', '$state', '$stateParams', 'DataMaintenanceFactory', '$q', function($scope, $http, $timeout, $modal, ngProgress, $filter, $sce, $state, $stateParams, DataMaintenanceFactory, $q) {
  ngProgress.start();
  $scope.Data = {
    'CompanyId': '',
    'Nickname': '',
    'Unit': '',
    'StreetAddress': '',
    'ClientIds': [],
    'Liaisons': []
  };
  $scope.init = function() {
    setTimeout(function() {
      // 维修商
      $('[name=pCompanyList]').select2({
        placeholder: "请选择客戶全称"
      });
      // 报修公司
      $('[name=sCompanyList]').select2({
        placeholder: "请选择负责区域"
      });
      $('.select2-container').css('z-index', 999);
      // $('.date-picker').datepicker({autoclose: true});
    }, 0);
    var p1 = DataMaintenanceFactory.getCompanyList($scope.userInfo.Passport, 's');
    var p2 = DataMaintenanceFactory.getCompanyList($scope.userInfo.Passport, 'p');
    var all = $q.all([p1, p2]);
    all.then(function(res) {
      var pCompanyData = res[0].data;
      var sCompanyData = res[1].data;
      // 1.公司别
      $scope.pCompanyList = pCompanyData;
      $scope.sCompanyList = sCompanyData;
      // 2.預設維修商
      $('[name=pCompanyList]').on("select2-selecting", function(e) {
        $scope.Data.CompanyName = e.object.text;
        $scope.Data.Nickname = e.object.element[0].dataset.nickname;
      });
      setTimeout(function() {
        ngProgress.complete();
      }, 0);
    }).catch(function(e) {
      ngProgress.complete();
      alert(e.data.Message);
    });
  };
  $scope.init();
  $scope.addLiaison = function() {
    $scope.Data.Liaisons.push({
      'Name': '',
      'PhoneNo': '',
      'Email': ''
    });
  };
  $scope.deleteLiaison = function(index) {
    $scope.Data.Liaisons.splice(index, 1);
  };
  $scope.btnCancel = function() {
    window.close();
  };
  $scope.btnOk = function() {
    $scope.buttonDisabled = true;
    ngProgress.start();
    var Params = JSON.parse(JSON.stringify($scope.Data));
    DataMaintenanceFactory.createUnit($scope.userInfo.Passport, $scope.userInfo.apiType, Params).success(function(data) {
      ngProgress.complete();
      $scope.$toastlast = toastr['success']("新增完成", "");
      setTimeout(function() {
        if (window.opener) {
          window.opener.ReloadMaintainerUnitList();
          window.close();
        } else {
          window.close();
        }
      }, 1000);
    }).error(function(error) {
      $scope.buttonDisabled = false;
      ngProgress.complete();
      alert(error.Messages);
    })
  };
}]);