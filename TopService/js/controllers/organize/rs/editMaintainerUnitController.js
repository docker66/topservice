angular.module('n580440').controller('editMaintainerUnitController', ['$scope', '$http', '$timeout', '$modal', 'ngProgress', '$filter', '$sce', '$state', '$stateParams', 'DataMaintenanceFactory', '$q', function($scope, $http, $timeout, $modal, ngProgress, $filter, $sce, $state, $stateParams, DataMaintenanceFactory, $q) {
  ngProgress.start();
  $scope.Data = {
    'CompanyId': '',
    'Nickname': '',
    'Unit': '',
    'PhoneNo': '',
    'Email': '',
    'StreetAddress': '',
    'ClientIds': [],
    'UnitManager': '',
    'JournalManager': '',
    'UnitAssigner': '',
    'Liaisons': [],
    'Financer': [{
      'FinanceCompanyId': '',
      'Bank': '',
      'BankAccount': '',
      'PaymentStartline': '',
      'PaymentDeadline': '',
      'PaymentApproveDeadline': '',
      'Voucher': '',
      'Way': '',
    }]
  };
  $scope.FinancerId = 0;
  $scope.IsLoading = true;
  $scope.init = function() {
    setTimeout(function() {
      // 维修商
      $('[name=pCompanyList]').select2({
        placeholder: "请选择客戶全称"
      });
      // 报修公司
      $('[name=sCompanyList]').select2({
        placeholder: "请选择负责区域"
      });
      $('.select2-container').css('z-index', 999);
    }, 0);
    var p1 = DataMaintenanceFactory.getCompanyList($scope.userInfo.Passport, 's');
    var p2 = DataMaintenanceFactory.getCompanyList($scope.userInfo.Passport, 'p');
    var p3 = DataMaintenanceFactory.getUnitDetail($scope.userInfo.Passport, $scope.userInfo.apiType, $stateParams.unitId);
    var all = $q.all([p1, p2, p3]);
    all.then(function(res) {
      var pCompanyData = res[0].data;
      var sCompanyData = res[1].data;
      var PUnitDetail = res[2].data;
      // 1.公司别
      $scope.pCompanyList = pCompanyData;
      $scope.sCompanyList = sCompanyData;
      $scope.Data = PUnitDetail;
      // 2.财务资料
      if (PUnitDetail.Liaisons == null) {
        $scope.Data.Liaisons = [];
      }
      // 3.成本中心
      if (PUnitDetail.Finances == null) {
        $scope.Data.Finances = [];
      }
      // 4.公司匿称
      _.forEach($scope.pCompanyList, function(item, index) {
        if ($scope.Data.CompanyId == item.CompanyId) {
          $scope.Data.Nickname = item.Nickname
        };
      });
      // 据点负责人 据点调度 项目管理人
      $scope.Data.managerA = {
        'HrCode': $scope.Data.UnitManagerId,
        'UserName': $scope.Data.UnitManager
      };
      $scope.Data.managerB = {
        'HrCode': $scope.Data.UnitAssignerId,
        'UserName': $scope.Data.UnitAssigner
      };
      $scope.Data.managerC = {
        'HrCode': $scope.Data.JournalManagerId,
        'UserName': $scope.Data.JournalManager
      };
      // 預設維修商
      $('[name=pCompanyList]').on("select2-selecting", function(e) {
        $scope.Data.CompanyName = e.object.text;
        $scope.Data.Nickname = e.object.element[0].dataset.nickname;
      });
      setTimeout(function() {
        $('[name=pCompanyList]').select2('val', $scope.Data.CompanyId);
        $('[name=sCompanyList]').select2('val', $scope.Data.ClientIds);
        $('[name=FinanceCompanyList]').select2({
          placeholder: "请选择对应门店的公司"
        });
        $('.select2-container').css('z-index', 999);
      }, 0);
      ngProgress.complete();
    }).catch(function(e) {
      ngProgress.complete();
      alert(e.data.Message);
    });
  };
  $scope.init();
  $scope.addLiaison = function() {
    $scope.Data.Liaisons.push({
      'Name': '',
      'PhoneNo': '',
      'Email': ''
    });
  };
  $scope.deleteLiaison = function(index) {
    $scope.Data.Liaisons.splice(index, 1);
  };
  $scope.addFinance = function() {
    $scope.Data.Finances.push({
      'FinanceCompanyId': '',
      'Bank': '',
      'BankAccount': '',
      'PaymentStartline': '',
      'PaymentDeadline': '',
      'PaymentApproveDeadline': '',
      'Voucher': 0,
      'Way': 0,
    });
    setTimeout(function() {
      $('[name=FinanceCompanyList]').select2({
        placeholder: "请选择对应门店的公司"
      });
      $('.select2-container').css('z-index', 999);
    }, 0);
  };
  $scope.deleteFinance = function(index) {
    $scope.Data.Finances.splice(index, 1);
  };
  $scope.selectUser = function(ModelType, Model, Index) {
    if ($scope.Data.CompanyId !== '') {
      var CompanyInfo = {
        'Company': $scope.Data.Nickname,
        'CompanyId': $scope.Data.CompanyId
      };
      var modalInstance = $modal.open({
        templateUrl: 'usersModal.html',
        controller: 'ntUsersPicker',
        windowClass: 'xl-modal',
        resolve: {
          Passport: function() {
            return $scope.userInfo.Passport;
          },
          CompanyInfo: function() {
            return CompanyInfo;
          },
          Type: function() {
            return 'P'
          }
        }
      });
      modalInstance.result.then(function(UserData) {
        var UserInfo = JSON.parse(JSON.stringify(UserData));
        // 1.姓名
        UserInfo.Name = UserData.UserName;
        // 2.多组电话
        _(UserData.Phone).each(function(item, index) {
          if (index == 0) {
            UserInfo.PhoneNo = item;
          } else {
            UserInfo.PhoneNo = UserInfo.PhoneNo + ',' + item;
          };
        });
        // 3.多组邮件
        _(UserData.Email).each(function(item, index) {
          if (index == 0) {
            UserInfo.Email = item;
          } else {
            UserInfo.Email = UserInfo.Email + ',' + item;
          };
        });
        // 输出
        if (ModelType == 'S') {
          $scope.Data[Model] = UserInfo;
        } else {
          $scope.Data[Model][Index] = UserInfo;
        };
      });
    };
  };
  $scope.btnCancel = function() {
    window.close();
  };
  $scope.btnOk = function() {
    // $scope.buttonDisabled = true;
    ngProgress.start();
    var Params = JSON.parse(JSON.stringify($scope.Data));
    delete Params.managerA;
    delete Params.managerB;
    delete Params.managerC;
    // 据点负责人 据点调度 项目管理人
    Params.UnitManagerId = $scope.Data.managerA.HrCode;
    Params.UnitManager = $scope.Data.managerA.UserName;
    Params.UnitAssignerId = $scope.Data.managerB.HrCode;
    Params.UnitAssigner = $scope.Data.managerB.UserName;
    Params.JournalManagerId = $scope.Data.managerC.HrCode;
    Params.JournalManager = $scope.Data.managerC.UserName;
    Params.Finances = [];
    if ($scope.Data.Finances !== null && $scope.Data.Finances !== undefined) {
      if ($scope.Data.Finances.length > 0) {
        _($scope.Data.Finances).each(function(item, index) {
          if (item.FinanceCompanyId !== '') {
            Params.Finances.push(item);
          };
        });
      };
    };
    DataMaintenanceFactory.updateUnit($scope.userInfo.Passport, $scope.userInfo.apiType, Params).success(function(data) {
      ngProgress.complete();
      $scope.$toastlast = toastr['success']("修改完成", "");
      setTimeout(function() {
        if (window.opener) {
          window.opener.ReloadMaintainerUnitList();
          window.close();
          // window.location.reload();
        } else {
          window.close();
        }
      }, 1000);
    }).error(function(error) {
      $scope.buttonDisabled = false;
      ngProgress.complete();
      alert(error.Messages);
    });
  };
}]);