angular.module("n580440").controller("organizeUnitAddCtrl", ["$scope", "$http", "$timeout", "$modal", "ngProgress", "$filter", "$sce", "$state", "$stateParams", "DataMaintenanceFactory", "$q", "FileUploader", "i18n",
  function($scope, $http, $timeout, $modal, ngProgress, $filter, $sce, $state, $stateParams, DataMaintenanceFactory, $q, FileUploader,i18n) {
    ngProgress.start();
    $scope.Info = {
      CompanyId: $scope.userInfo.CompanyId,
      EquipmentTypeClass: null,
      ApiUrl: ApiMapper.ccApi
    };
    $scope.CompanyList = [];
    $scope.AreaList = [];
    $scope.ProvinceList = [];
    $scope.AreaFilter = {
      ReferenceUnitId: ""
    };
    $scope.ProvinceFilter = {
      ReferenceUnitId: ""
    };
    $scope.Data = {
      CompanyId: "",
      Company: "",
      Level2Id: "",
      Level3Id: "",
      Unit: "",
      UnitId: "",
      ServiceType: "",
      Province: "",
      City: "",
      District: "",
      StreetAddress: "",
      Phones: "",
      Emails: "",
      Open: "",
      LifeCycle: "",
      managerA: {
        HrCode: "",
        UserName: ""
      },
      managerB: {
        HrCode: "",
        UserName: ""
      },
      managerC: {
        HrCode: "",
        UserName: ""
      },
      CostCenter: {
        CompanyName: "",
        CompanyNickname: "",
        CompanyCode: "",
        Name: "",
        Code: "",
        Invoice: "",
        InvoiceAccount: "",
        InvoiceBank: "",
        TaxId: ""
      }
    };
    $scope.UnitRreadonly = {
      status: false
    };
    // upload setting
    var uploader = ($scope.uploader = new FileUploader({
      url: ApiMapper.ccApi + "/g/management/uploadTmpl/Temp/1",
      headers: {
        Passport: $scope.userInfo.Passport
      }
    }));
    //  uploader filters
    uploader.filters.push({
      name: "imageFilter",
      fn: function(item /*{File|FileLikeObject}*/ , options) {
        var type = "|" + item.type.slice(item.type.lastIndexOf("/") + 1) + "|";
        if (uploader.queue.length == 1) {
          uploader.removeFromQueue(0);
        }
        return uploader.queue.length < 10;
      }
    });
    $scope.clean = function() {
      uploader.clearQueue();
      document.getElementById("uploadInput").value = "";
    };
    $scope.init = function() {
      setTimeout(function() {
        // 公司别
        $("[name=Company]").select2({
          placeholder: "请选择公司"
        });
        // 大区
        $("[name=Level2]").select2({
          placeholder: "请选择大区"
        }).on("select2-selecting", function(e) {
          $scope.$apply();
        });
        // 省 营运部
        $("[name=Level3]").select2({
          placeholder: "请选择营运部"
        });
        // 营运类型
        $("[name=ServiceType]").select2({
          placeholder: "请选择营运类型"
        });
        // 营运状态
        $("[name=LifeCycle]").select2({
          placeholder: "请选择营运状态"
        });
        $(".select2-container").css("z-index", 999);
        $(".select2-container").css("display", "inline-block");
        $(".date-picker").datepicker({
          autoclose: true
        });
      }, 0);
      var p1 = DataMaintenanceFactory.getCompanyList($scope.userInfo.Passport,$scope.userInfo.apiType);
      var all = $q.all([p1]);
      all.then(function(res) {
        var CompanyListData = res[0].data;
        $scope.CompanyList = CompanyListData;
        // 1.公司别
        $("[name=Company]").on("select2-selecting", function(e) {
          $scope.Data.Company = e.object.text;
          $("[name=Level2]").select2("val", "");
          $("[name=Level3]").select2("val", "");
          $scope.Data.Level2Id = "";
          $scope.Data.Level3Id = "";
          $scope.getsUnit(e.object.id);
        });
        // 2.区
        $("[name=Level2]").on("select2-selecting", function(e) {
          $("[name=Level3]").select2("val", "");
          $scope.Data.Level3Id = "";
          $scope.ProvinceFilter.ReferenceUnitId = e.object.id;
          $scope.$apply();
        });
        setTimeout(function() {
          ngProgress.complete();
        }, 0);
      }).catch(function(e) {
        ngProgress.complete();
        alert(e.data.Message);
      });
    };
    $scope.init();
    $scope.deleteManager = function(index) {
      $scope.Data.managers.splice(index, 1);
    };
    $scope.getUnit = function(CompanyId) {
      DataMaintenanceFactory.getUnits($scope.userInfo.Passport,$scope.userInfo.apiType, CompanyId).success(function(UnitData) {
        $scope.AreaFilter.ReferenceUnitId = UnitData[0][0].UnitId;
        $scope.AreaList = UnitData[1];
        $scope.ProvinceList = UnitData[2];
      }).error(function(error) {
        alert(error.Messages);
      });
    };
    $scope.btnCancel = function() {
      window.close();
    };
    $scope.btnOk = function() {
      $scope.buttonDisabled = true;
      ngProgress.start();
      var Params = JSON.parse(JSON.stringify($scope.Data));
      delete Params.managerA;
      delete Params.managerB;
      delete Params.managerC;
      // 维修员 饮料经理 食安经理
      if ($scope.Data.managerA.HrCode !== undefined) {
        Params.NurseId = $scope.Data.managerA.HrCode;
        Params.NurseName = $scope.Data.managerA.UserName;
      } else {
        Params.NurseId = "";
        Params.NurseName = "";
      }
      if ($scope.Data.managerB.HrCode !== undefined) {
        Params.DrinkDeputyId = $scope.Data.managerB.HrCode;
        Params.DrinkDeputyName = $scope.Data.managerB.UserName;
      } else {
        Params.DrinkDeputyId = "";
        Params.DrinkDeputyName = "";
      }
      if ($scope.Data.managerC.HrCode !== undefined) {
        Params.FoodDeputyId = $scope.Data.managerC.HrCode;
        Params.FoodDeputyName = $scope.Data.managerC.UserName;
      } else {
        Params.FoodDeputyId = "";
        Params.FoodDeputyName = "";
      }
      DataMaintenanceFactory.createUnit($scope.userInfo.Passport.$scope.userInfo.apiType, Params).success(function(data) {
        $scope.UnitRreadonly.status = true;
        // 上传 展店工程.pdf
        if (uploader.queue.length == 1) {
          $scope.$toastlast = toastr["success"]("新增门店完成", "");
          uploader.onBeforeUploadItem = function(item) {
            item.formData.push({
              filename: uploader.queue[0].file.name
            });
            item.formData.push({
              UnitId: Params.UnitId
            });
          };
          //全部上傳
          uploader.uploadAll();
          //上傳成功
          uploader.onSuccessItem = function(fileItem, response, status, headers) {
            ngProgress.complete();
            $scope.$toastlast = toastr["success"]("上传PDF已完成", "");
            setTimeout(function() {
              if (window.opener) {
                window.opener.ReloadStoreUnitList();
                window.close();
              } else {
                window.close();
              }
            }, 1000);
          };
          //上傳失敗
          uploader.onErrorItem = function(fileItem, response, status, headers) {
            ngProgress.complete();
            alert(response.ExceptionMessage);
          };
        } else {
          ngProgress.complete();
          $scope.$toastlast = toastr["success"]("新增完成", "");
          setTimeout(function() {
            if (window.opener) {
              window.opener.ReloadStoreUnitList();
              window.close();
            } else {
              window.close();
            }
          }, 1000);
        }
      }).error(function(error) {
        $scope.buttonDisabled = false;
        ngProgress.complete();
        alert(error.Messages);
      });
    };
    $scope.uploadPDF = function() {
      uploader.onBeforeUploadItem = function(item) {
        item.formData.push({
          filename: uploader.queue[0].file.name
        });
        item.formData.push({
          UnitId: $scope.Data.UnitId
        });
      };
      //全部上傳
      uploader.uploadAll();
      //上傳成功
      uploader.onSuccessItem = function(fileItem, response, status, headers) {
        ngProgress.complete();
        $scope.$toastlast = toastr["success"]("上传PDF已完成", "");
        setTimeout(function() {
          if (window.opener) {
            window.opener.ReloadStoreUnitList();
            window.close();
          } else {
            window.close();
          }
        }, 1000);
      };
      //上傳失敗
      uploader.onErrorItem = function(fileItem, response, status, headers) {
        ngProgress.complete();
        alert(response.ExceptionMessage);
      };
    };
    $scope.selectUser = function(ModelType, Model, Index) {
      if ($scope.Data.CompanyId !== "") {
        var CompanyInfo = {
          Company: $scope.Data.Company,
          CompanyId: $scope.Data.CompanyId
        };
        var modalInstance = $modal.open({
          templateUrl: "usersModal.html",
          controller: "ntUsersPicker",
          windowClass: "xl-modal",
          resolve: {
            Passport: function() {
              return $scope.userInfo.Passport;
            },
            CompanyInfo: function() {
              return CompanyInfo;
            },
            Type: function() {
              return "S";
            }
          }
        });
        modalInstance.result.then(function(UserData) {
          if (ModelType == "S") {
            $scope.Data[Model] = UserData;
          } else {
            $scope.Data[Model][Index] = UserData;
          }
        });
      }
    };
  }
]).controller("organizeUnitModifyCtrl", ["$scope", "$http", "$timeout", "$modal", "ngProgress", "$filter", "$sce", "$state", "$stateParams", "DataMaintenanceFactory", "$q", "FileUploader", "i18n",
  function($scope, $http, $timeout, $modal, ngProgress, $filter, $sce, $state, $stateParams, DataMaintenanceFactory, $q, FileUploader,i18n) {
    ngProgress.start();
    $scope.Info = {
      CompanyId: $scope.userInfo.CompanyId,
      EquipmentTypeClass: null,
      ApiUrl: ApiMapper.ccApi
    };
    $scope.CompanyList = [];
    $scope.AreaList = [];
    $scope.ProvinceList = [];
    $scope.AreaFilter = {
      ReferenceUnitId: ""
    };
    $scope.ProvinceFilter = {
      ReferenceUnitId: ""
    };
    $scope.UserList = [];
    $scope.IsLoading = true;
    $scope.Data = {};
    var apiType = Cookies.get("SigninType") == 2 ? 's' : 'p';
    // upload setting
    var uploader = ($scope.uploader = new FileUploader({
      url: ApiMapper.ccApi + "/g/management/uploadTmpl/Temp/1",
      headers: {
        Passport: $scope.userInfo.Passport
      }
    }));
    //  uploader filters
    uploader.filters.push({
      name: "imageFilter",
      fn: function(item /*{File|FileLikeObject}*/ , options) {
        if (uploader.queue.length == 1) {
          uploader.removeFromQueue(0);
        }
        return this.queue.length < 10;
      }
    });
    $scope.clean = function() {
      uploader.clearQueue();
      document.getElementById("uploadInput").value = "";
      $scope.Data.file.name = "";
    };
    $scope.download = function(FileName, FilePath) {
      var link = document.createElement("a");
      link.href = FilePath;
      link.download = FileName;
      link.style = "display:none;";
      document.body.appendChild(link);
      link.click();
      link.remove();
    };

    function removeBlankOption() {
      setTimeout(function() {
        var Select2ResultLabels = document.querySelectorAll(".select2-result-label");
        for (var i = 0; i < Select2ResultLabels.length; i++) {
          if (Select2ResultLabels[i].textContent == "") {
            Select2ResultLabels[i].remove();
          }
        }
      }, 0);
    }
    $scope.init = function() {
      setTimeout(function() {
        // 公司别
        $("[name=Company]").select2({
          placeholder: "请选择公司"
        });
        // 大区
        $("[name=Level2]").select2({
          placeholder: "请选择大区"
        }).on("select2-selecting", function(e) {
          $scope.$apply();
        });
        // 省 营运部
        $("[name=Level3]").select2({
          placeholder: "请选择营运部"
        });
        // 营运类型
        $("[name=ServiceType]").select2({
          placeholder: "请选择营运类型"
        }).on("select2-opening", function() {
          removeBlankOption();
        }).on("select2-loaded", function() {
          removeBlankOption();
        });
        // 营运状态
        $("[name=LifeCycle]").select2({
          placeholder: "请选择营运状态"
        }).on("select2-opening", function() {
          removeBlankOption();
        }).on("select2-loaded", function() {
          removeBlankOption();
        });
        $(".date-picker").datepicker({
          autoclose: true
        });
        $(".select2-container").css("z-index", 999);
        $(".select2-container").css("display", "inline-block");
      }, 0);
      var p1 = DataMaintenanceFactory.getCompanyList($scope.userInfo.Passport, apiType);
      var p2 = DataMaintenanceFactory.getUnitDetail($scope.userInfo.Passport, apiType, $stateParams.unitId);
      var all = $q.all([p1, p2]);
      all.then(function(res) {
        var CompanyListData = res[0].data;
        var SUnitDetail = res[1].data;
        if (SUnitDetail.Open !== null) {
          SUnitDetail.Open = $filter("date")(new Date(SUnitDetail.Open), "yyyy/MM/dd");
        }
        if (SUnitDetail.Close !== null) {
          SUnitDetail.Close = $filter("date")(new Date(SUnitDetail.Close), "yyyy/MM/dd");
        }
        $scope.CompanyList = CompanyListData;
        $scope.Data = JSON.parse(JSON.stringify(SUnitDetail));
        // 店長
        if ($scope.Data.StoreLeader !== null) {
          $scope.Data.StoreLeader = {
            HrCode: SUnitDetail.StoreLeader.UserId,
            UserName: SUnitDetail.StoreLeader.UserName
          };
        }
        // 經理
        $scope.Data.Manager = [];
        if (SUnitDetail.Manager !== null) {
          if (SUnitDetail.Manager.length > 0) {
            _(SUnitDetail.Manager).each(function(item, index) {
              var Info = {
                HrCode: item.UserId,
                UserName: item.UserName
              };
              $scope.Data.Manager.push(Info);
            });
          }
        }
        // 維修專員/飲料經理/食安經理
        $scope.Data.managerA = {
          HrCode: SUnitDetail.NurseId,
          UserName: SUnitDetail.NurseName
        };
        $scope.Data.managerB = {
          HrCode: SUnitDetail.DrinkDeputyId,
          UserName: SUnitDetail.DrinkDeputyName
        };
        $scope.Data.managerC = {
          HrCode: SUnitDetail.FoodDeputyId,
          UserName: SUnitDetail.FoodDeputyName
        };
        // 1.公司别
        $("[name=Company]").on("select2-selecting", function(e) {
          $scope.Data.Company = e.object.text;
          $("[name=Level2]").select2("val", "");
          $("[name=Level3]").select2("val", "");
          $scope.Data.Level2Id = "";
          $scope.Data.Level3Id = "";
          $scope.getsUnit(e.object.id);
        });
        // 2.区
        $("[name=Level2]").on("select2-selecting", function(e) {
          $("[name=Level3]").select2("val", "");
          $scope.Data.Level3Id = "";
          $scope.ProvinceFilter.ReferenceUnitId = e.object.id;
          $scope.$apply();
        });
        // 3.营运类型
        $("[name=ServiceType]").select2("val", $scope.Data.ServiceType);
        // 4.营运状态
        $("[name=LifeCycle]").select2("val", $scope.Data.LifeCycle);
        if ($scope.Data.CompanyId) {
          $scope.getsUnit($scope.Data.CompanyId);
        } else {
          setTimeout(function() {
            ngProgress.complete();
          }, 0);
        }
      }).catch(function(e) {
        ngProgress.complete();
        alert(e.Message);
      });
    };
    $scope.init();
    $scope.addManager = function() {
      $scope.Data.Manager.push({
        HrCode: "",
        UserName: ""
      });
    };
    $scope.deleteManager = function(index) {
      $scope.Data.Manager.splice(index, 1);
    };
    $scope.getsUnit = function(CompanyId) {
      DataMaintenanceFactory.getUnits($scope.userInfo.Passport, apiType, CompanyId).success(function(UnitData) {
        $scope.AreaFilter.ReferenceUnitId = UnitData[0][0].UnitId;
        $scope.AreaList = UnitData[1];
        $scope.ProvinceList = UnitData[2];
        // 载入初始化给值 [公司][大区][营运部]
        if ($scope.Data.CompanyId !== null && $scope.IsLoading !== false) {
          setTimeout(function() {
            $("[name=Company]").select2("val", $scope.Data.CompanyId);
            $("[name=Level2]").select2("val", $scope.Data.Level2Id);
            $("[name=Level3]").select2("val", $scope.Data.Level3Id);
            $scope.ProvinceFilter.ReferenceUnitId = $scope.Data.Level2Id;
            $scope.IsLoading = false;
            ngProgress.complete();
          }, 0);
        }
      }).catch(function(e) {
        ngProgress.complete();
        alert(e.data.Message);
      });
    };
    // $scope.getUserList = function (CompanyId) {
    //     DataMaintenanceFactory.getsUserList($scope.userInfo.Passport, CompanyId)
    //         .success(function (UserData) {
    //             if ($scope.Data.StoreLeader !== null) {
    //                 if ($scope.Data.StoreLeader.HrCode) {
    //                     var key = {
    //                         'HrCode': $scope.Data.StoreLeader.HrCode
    //                     };
    //                     $scope.Data.StoreLeader.UserName = (_.findWhere(UserData, key)).UserName;
    //                 };
    //             };
    //             if ($scope.Data.managerA !== null) {
    //                 if ($scope.Data.managerA.HrCode) {
    //                     var key = {
    //                         'HrCode': $scope.Data.managerA.HrCode
    //                     };
    //                     $scope.Data.managerA.UserName = (_.findWhere(UserData, key)).UserName;
    //                 };
    //             };
    //             if ($scope.Data.managerB !== null) {
    //                 if ($scope.Data.managerB.HrCode) {
    //                     var key = {
    //                         'HrCode': $scope.Data.managerB.HrCode
    //                     };
    //                     $scope.Data.managerB.UserName = (_.findWhere(UserData, key)).UserName;
    //                 };
    //             };
    //             if ($scope.Data.managerC !== null) {
    //                 if ($scope.Data.managerC.HrCode) {
    //                     var key = {
    //                         'HrCode': $scope.Data.managerC.HrCode
    //                     };
    //                     $scope.Data.managerC.UserName = (_.findWhere(UserData, key)).UserName;
    //                 };
    //             };
    //             if ($scope.Data.Manager !== null) {
    //                 if ($scope.Data.Manager.length > 0) {
    //                     _($scope.Data.Manager).each(function (item, index) {
    //                         var key = {
    //                             'HrCode': item.HrCode
    //                         };
    //                         $scope.Data.Manager[index].UserName = (_.findWhere(UserData, key)).UserName;
    //                     });
    //                 };
    //             };
    //         }).catch(function (e) {
    //             ngProgress.complete();
    //             alert(e.data.Message);
    //         });
    // };
    $scope.donloadFile = function() {
      var URI = ApiMapper.ccApi + "/g/management/donloadFile";
      var ParameterStr = {
        UnitId: $scope.Data.UnitId,
        FileName: $scope.Data.FileName[0]
      };
      $http({
        url: URI,
        method: "POST",
        contentType: "application/json",
        headers: {
          Passport: $scope.userInfo.Passport
        },
        responseType: "arraybuffer",
        data: JSON.stringify(ParameterStr)
      }).success(function(data, status, headers) {
        headers = headers();
        var FileName = decodeURIComponent(headers["x-filename"]);
        var Data = data;
        var PDFData = new Blob([Data], {
          type: "application/pdf"
        });
        //IE11 & Edge
        if (navigator.msSaveBlob) {
          navigator.msSaveBlob(PDFData, FileName);
        } else {
          var link = document.createElement("a");
          link.href = window.URL.createObjectURL(PDFData);
          link.setAttribute("download", FileName);
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
      }).error(function(error) {});
    };
    $scope.deleteFile = function() {
      var URI = ApiMapper.ccApi + "/g/management/deleteFile";
      var ParameterStr = {
        UnitId: $scope.Data.UnitId,
        FileName: $scope.Data.FileName[0]
      };
      var r = confirm("确定删除档案？");
      if (r == true) {
        $http({
          url: URI,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: $scope.userInfo.Passport
          },
          data: JSON.stringify(ParameterStr)
        }).success(function(data, status, headers) {
          $scope.Data.FileName = [];
          alert(data);
        }).error(function(error) {
          alert(error);
        });
      }
    };
    $scope.selectUser = function(ModelType, Model, Index) {
      if ($scope.Data.CompanyId !== "") {
        var CompanyInfo = {
          Company: $scope.Data.Company,
          CompanyId: $scope.Data.CompanyId
        };
        var modalInstance = $modal.open({
          templateUrl: "usersModal.html",
          controller: "ntUsersPicker",
          windowClass: "xl-modal",
          resolve: {
            Passport: function() {
              return $scope.userInfo.Passport;
            },
            CompanyInfo: function() {
              return CompanyInfo;
            },
            Type: function() {
              return "S";
            }
          }
        });
        modalInstance.result.then(function(UserData) {
          if (ModelType == "S") {
            $scope.Data[Model] = UserData;
          } else {
            $scope.Data[Model][Index] = UserData;
          }
        });
      }
    };
    $scope.btnCancel = function() {
      window.close();
    };
    $scope.btnOk = function() {
      $scope.buttonDisabled = true;
      ngProgress.start();
      var Params = JSON.parse(JSON.stringify($scope.Data));
      delete Params.managerA;
      delete Params.managerB;
      delete Params.managerC;
      // 店长
      if ($scope.Data.StoreLeader !== null && $scope.Data.StoreLeader !== undefined) {
        Params.StoreLeader = {
          UserId: $scope.Data.StoreLeader.HrCode,
          UserName: $scope.Data.StoreLeader.UserName
        };
      }
      // 经理
      Params.Manager = [];
      if ($scope.Data.Manager !== null && $scope.Data.Manager !== undefined) {
        if ($scope.Data.Manager.length > 0) {
          _($scope.Data.Manager).each(function(item, index) {
            if (item.HrCode !== "") {
              Params.Manager.push({
                UserId: item.HrCode,
                UserName: item.UserName
              });
            }
          });
        }
      }
      // 维修员 饮料经理 食安经理
      Params.NurseId = $scope.Data.managerA.HrCode;
      Params.NurseName = $scope.Data.managerA.UserName;
      Params.DrinkDeputyId = $scope.Data.managerB.HrCode;
      Params.DrinkDeputyName = $scope.Data.managerB.UserName;
      Params.FoodDeputyId = $scope.Data.managerC.HrCode;
      Params.FoodDeputyName = $scope.Data.managerC.UserName;
      DataMaintenanceFactory.updateUnit($scope.userInfo.Passport, apiType, Params).success(function(data) {
        // 上传 展店工程.pdf
        if (uploader.queue.length == 1) {
          uploader.onBeforeUploadItem = function(item) {
            item.formData.push({
              filename: uploader.queue[0].file.name
            });
            item.formData.push({
              UnitId: Params.UnitId
            });
          };
          //全部上傳
          uploader.uploadAll();
          //上傳成功
          uploader.onSuccessItem = function(fileItem, response, status, headers) {
            ngProgress.complete();
            $scope.$toastlast = toastr["success"]("修改完成", "");
            setTimeout(function() {
              if (window.opener) {
                window.opener.ReloadStoreUnitList();
                window.close();
                // window.location.reload();
              } else {
                window.close();
                // window.location.reload();
              }
            }, 1000);
          };
          //上傳失敗
          uploader.onErrorItem = function(fileItem, response, status, headers) {
            $scope.buttonDisabled = false;
            ngProgress.complete();
            alert(response.ExceptionMessage);
          };
        } else {
          ngProgress.complete();
          $scope.$toastlast = toastr["success"]("修改完成", "");
          setTimeout(function() {
            if (window.opener) {
              window.opener.ReloadStoreUnitList();
              window.close();
              // window.location.reload();
            } else {
              window.close();
              // window.location.reload();
            }
          }, 1000);
        }
      }).error(function(error) {
        $scope.buttonDisabled = false;
        ngProgress.complete();
        alert(error.Messages);
      });
    };
  }
]);