//部门及人员维护
angular.module("n580440").controller("organizeUserListCtrl", ["$scope", "$http", "$modal", "$timeout", "ngProgress", "DataMaintenanceFactory","i18n",
  function($scope, $http, $modal, $timeout, ngProgress, DataMaintenanceFactory, i18n) {
    ngProgress.start();
    $scope.DeptID = Request.QueryString("DeptID") != "" ? Request.QueryString("DeptID") : $scope.userInfo.UnitId;
    $scope.DeptInfoList = {};
    $scope.UnitData = [];
    $scope.UnitPath = "";
    $scope.MemberList = [];
    $scope._MemberList = [];
    $scope.LengthForMemberList = [];
    $scope.ThumbnailForUnit = {
      AvatorId: null,
      Avator: "../../Content/images/d0.png"
    };
    var apiType = $scope.userInfo.SigninType == 2 ? 's' : 'p';
    $scope.SearchParams = {
        'Filtration': ''
    };
    $scope.UserList = [];
    $scope.CsvButtonDisabled = false;
    // 下载板模
    $scope.downloadExcel = DataMaintenanceFactory.downloadExcel;

    //设定部门路径显示
    var setUnitCrumbs = (obj) => {
      DataMaintenanceFactory.getUnits($scope.userInfo.Passport, apiType, $scope.userInfo.CompanyId)
      /*$http({
        url: ApiMapper.sApi + "/s/unit/maps",
        method: "GET",
        contentType: "application/json",
        headers: {
          Passport: $scope.userInfo.Passport
        }
      })*/
      .success(function(Unitdata) {
        $scope.Unitdata = Unitdata;
        _($scope.Unitdata[0]).each(item => {
          if (item.UnitId === obj.Level1) {
            $scope.UnitPath = item.Unit;
          }
        });
        if (obj.UnitLevel >= 40) {
          _($scope.Unitdata[1]).each(item => {
            if (item.UnitId === obj.Level2) {
              $scope.UnitPath += ' > ' + item.Unit;
            }
          });
        }
        if (obj.UnitLevel >= 60) {
          _($scope.Unitdata[2]).each(item => {
            if (item.UnitId === obj.Level3) {
              $scope.UnitPath += ' > ' + item.Unit;
            }
          });
        }
        if (obj.UnitLevel == 100) {
          $scope.UnitPath += ' > ' + obj.Unit;
        }
      });
    };
    //获取单一部门信息
    //依据传入的部门ID查询
    $scope.ShowDeptName = function(id) {
      $scope.DeptID = id;
      $scope.DeptInfoList.length = 0;
      DataMaintenanceFactory.getUnitDetail($scope.userInfo.Passport, apiType, $scope.DeptID)
      /*$http({
        url: `${ApiMapper.sApi}/${apiType}/unit/${Base64.encode($scope.DeptID)}`,
        method: "GET",
        cache: false,
        contentType: "application/json",
        headers: {
          Passport: $scope.userInfo.Passport
        }
      })*/
      .success(res => {
        $scope.DeptInfoList = res;
        $scope.ThumbnailForUnit = {
          AvatorId: null,
          Avator: "../../Content/images/d0.png"
        };
        setUnitCrumbs($scope.DeptInfoList);
        $scope.getThumbnailForUnit();
        $scope.getQrcode();
        if ($scope.DeptInfoList.IPhoneNos != null) {
          $scope.DeptInfoList.IPhoneNos = $scope.DeptInfoList.IPhoneNos.join(',').toString();
        }
      });
    };
    //获取单一部门所有成员
    //依据传入的部门ID查询
    $scope.ShowMember = function(DeptID) {
      $scope.mDeptID = DeptID;
      $scope.MemberList = [];
      DataMaintenanceFactory.getUserList($scope.userInfo.Passport, apiType, $scope.DeptID)
      // $http({
      //   url: `${ApiMapper.sApi}/${apiType}/unit/user/${$scope.DeptID}`,
      //   method: "POST",
      //   cache: false,
      //   contentType: "application/json",
      //   headers: {
      //     Passport: $scope.userInfo.Passport
      //   }
      // })
      .success(function(data) {
        $scope._MemberList = data;
        $scope.LengthForMemberList = _.range($scope._MemberList.length / 3);
        $scope._Array = new Array();
        $scope.tmpArray = new Array();
        for (var i = 0; i < $scope._MemberList.length; i++) {
          $scope.tmpArray.length = 0;
          $scope.tmpArray = $scope._MemberList[i];
          var ContactInfo = $scope.tmpArray.Contacts;
          if (ContactInfo.length > 0) {
            if (ContactInfo[0].Type == 1) {
              $scope.tmpArray.PhoneNo = ContactInfo[0].Info;
              if (ContactInfo.length > 1) $scope.tmpArray.Email = ContactInfo[1].Info;
            } else if (ContactInfo[0].Type == 0) {
              $scope.tmpArray.Email = ContactInfo[0].Info;
              if (ContactInfo.length > 1) $scope.tmpArray.PhoneNo = ContactInfo[1].Info;
            }
          } else {
            $scope.tmpArray.PhoneNo = "";
            $scope.tmpArray.Email = "";
          }
          $scope.tmpArray.Avator = "../../Content/images/a1.png";
          $scope.tmpArray.AvatorId = null;
          $scope._Array.push($scope.tmpArray);
        }
        $scope.MemberList = $scope._Array;
        $scope.getThumbnailForUser();
        ngProgress.complete();
      });
    };
    //获取门店QrCode
    $scope.getQrcode = function() {
      $http({
        url: `${ApiMapper.sApi}/s/unit/info/qrcode/${Base64.encode($scope.DeptID)}`,
        method: "GET",
        contentType: "application/json",
        headers: {
          Passport: $scope.userInfo.Passport
        }
      }).success(function(res) {
        if (res != null) {
          $scope.QrcodeURL = res;
          $scope.ThumbnailForUnit.Avator = $scope.QrcodeURL;
        } else {
          $scope.QrcodeURL = "";
        }
      });
    };
    //获取部门图片
    $scope.getThumbnailForUnit = function() {
      $http({
        url: ApiMapper.ccApi + "/cc/thumbnail/size", //'/cc/thumbnail/S/' + encodeURIComponent($scope.Users.Id),
        method: "POST",
        contentType: "application/json",
        headers: {
          Passport: $scope.userInfo.Passport
        },
        data: {
          Size: "XL",
          CollectionId: encodeURIComponent($scope.DeptID)
        }
      }).success(function(data) {
        if (data.length > 0) {
          //$scope.ThumbnailForUnit = _.first(data);
          $scope.ThumbnailForUnit.Avator = ApiMapper.FileApi + "/" + data[0].Thumbnail;
          $scope.ThumbnailForUnit.AvatorId = data[0].Id;
        }
      });
    };
    //获取部门人员图片
    $scope.getThumbnailForUser = function() {
      _($scope.MemberList).each(function(Item) {
        $http({
          url: ApiMapper.ccApi + "/cc/thumbnail/size",
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: $scope.userInfo.Passport
          },
          data: {
            Size: "s",
            CollectionId: encodeURIComponent(Item.UserId)
          }
        }).success(function(data) {
          var thumbnail = eval(data);
          if (thumbnail.length > 0) {
            Item.Avator = ApiMapper.FileApi + "/" + thumbnail[thumbnail.length - 1].Thumbnail;
            Item.AvatorId = thumbnail[thumbnail.length - 1].Id;
          }
        });
      });
    };
    //预先加载本人所在部门和人员
    $scope.ShowDeptName($scope.userInfo.UnitId);
    $scope.ShowMember($scope.userInfo.UnitId);
    //console.log($scope.Info);
    //打开选择可检视部门层
    $scope.items = "";
    $scope.open = function(size) {
      $scope.Info.ApiUrl = ApiMapper.ccApi;
      var modalInstance = $modal.open({
        templateUrl: "unitsModal.html",
        size: size,
        backdrop: "static",
        controller: "ntOrgPicker",
        resolve: {
          Options: function() {
            return {
              Type: "S1",
              Source: "B2C"
            };
          },
          Info: function() {
            return $scope.Info;
          },
          UnitInfo: null
        }
      });
      modalInstance.result.then(function(Unit) {
        $scope.DeptID = Unit.UnitId[0];
        $scope.ShowDeptName($scope.DeptID);
        $scope.ShowMember($scope.DeptID);
        // $scope.UnitPath = Unit.CompanyName;
        // if (Unit.RegionName.length > 0) $scope.UnitPath += ` > ${Unit.RegionName[0]}`;
        // if (Unit.DivisionName.length > 0) $scope.UnitPath += ` > ${Unit.DivisionName[0]}`;
        //$scope.UnitPath = _.sortBy($scope.UnitPath, function (Items) { return Items.Level; });
      });
    };
    //打开部门维护层
    $scope.ModifyDeptOpen = function(size) {
      var modalInstance = $modal.open({
        templateUrl: "ModifyDeptPage.html",
        controller: "ModifyDeptCtrl",
        backdrop: "static",
        size: size,
        resolve: {
          DeptInfoList: function() {
            return $scope.DeptInfoList;
          }
        }
      });
      modalInstance.result.then(function(result) {
        $scope.ShowDeptName(result);
        $timeout(function() {
          $scope.getThumbnailForUnit();
        }, 5000);
      });
    };
    //打开成员维护画面
    $scope.MemberDeptID = "";
    var userModifyOpen = (eventId = "add", userId) => {
      var modalInstance = $modal.open({
        templateUrl: "userModifyModal.html",
        controller: "organizeUserModifyCtrl",
        backdrop: "static",
        resolve: {
          paramObj: () => {
            return {
              userId: userId,
              deptId: $scope.DeptID,
              eventId: eventId
            };
          }
        }
      });
      modalInstance.result.then(function(result) {
        $scope._MemberDeptID = result;
        $scope.ShowMember($scope._MemberDeptID);
      });
    };
    $scope.userAdd = function() {
      userModifyOpen("add", null);
    };
    $scope.userModify = userId => {
      userModifyOpen("modify", userId);
    };
    /*弹出预览视窗*/
    $scope.showImages = function() {
      var url = "";
      url = ApiMapper.ccApi + "/cc/image/" + $scope.ThumbnailForUnit.AvatorId;
      if ($scope.ThumbnailForUnit.AvatorId != null) {
        var ImgData = url;
        var modalInstance = $modal.open({
          templateUrl: "ShowImg.html",
          size: "lg",
          controller: "ShowImgCtrl",
          resolve: {
            ImgData: function() {
              return ImgData;
            }
          }
        });
      }
    };
    $scope.importCsv = function() {
      var ImportInfo = {
        'Title': '维修单位人員导入',
        'Type': '5'
      };
      if(apiType == 's'){
        ImportInfo = {
        'Title': '报修单位人员导入',
        'Type': '3'
      };
      }
      var importCsv = $modal.open({
        animation: true,
        templateUrl: 'importCsv.html',
        controller: 'importCsvController',
        size: 'lg',
        backdrop: 'static',
        keyboard: false,
        resolve: {
          ImportInfo: function() {
            return ImportInfo;
          },
          Params: function() {
            return '';
          },
          Passport: function() {
            return $scope.userInfo.Passport;
          }
        }
      });
      importCsv.result.then(function(result) {
        if (result == 'success') {
          window.ReloadMaintainerUserList();
        };
      });
    };
    $scope.getCsv = function() {
      $scope.CsvButtonDisabled = true;
      var Params = {
        'Filtration': $scope.SearchParams.Filtration
      };
      DataMaintenanceFactory.getUserCsv($scope.userInfo.Passport, apiType, Params).success(function(data, status, headers) {
        headers = headers();
        var FileName = decodeURIComponent(headers['x-filename']);
        var Data = '\ufeff' + data;
        var csvData = new Blob([Data], {
          type: 'text/csv;charset=utf-8;'
        });
        //IE11 & Edge
        if (navigator.msSaveBlob) {
          navigator.msSaveBlob(csvData, FileName);
        } else {
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(csvData);
          link.setAttribute('download', FileName);
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
        $scope.CsvButtonDisabled = false;
      }).error(function(error) {
        $scope.CsvButtonDisabled = false;
        alert(error.Message);
      })
    };
  }
]).controller("organizeUserModifyCtrl", ["$scope", "$http", "$modalInstance", "$modal", "$timeout", "$q", "FileUploader", "DataMaintenanceFactory", "paramObj", "i18n",
  function($scope, $http, $modalInstance, $modal, $timeout, $q, FileUploader, DataMaintenanceFactory, paramObj, i18n) {
    $scope.Passport = Cookies.get("Passport");
    $scope.UserId = paramObj.userId;
    $scope.AvatorId = null;
    $scope.isInuse = false;
    var apiType = Cookies.get("SigninType") == 2 ? 's' : 'p';
    var uploader = ($scope.uploader = new FileUploader({
      url: ApiMapper.ccApi + "/cc/upload",
      headers: {
        Passport: $scope.Passport
      }
    }));
    $scope.Info = {
      'CompanyId': Cookies.get("CompanyId"),
      'EquipmentTypeClass': null,
      'ApiUrl': ApiMapper.ccApi
    };
    $scope.MemberInfo = {
      IsManager: 0,
      IsAssistantManager: 0,
      IsManagerAccount: 0,
      UserId: "",
      UserName: "",
      UnitId: "",
      Unit: "",
      TitleId: "",
      Title: "",
      CompanyId: "",
      Nickname: "",
      Status: "",
      Level: 0,
      Phone: "",
      Email: "",
      Accessories: [],
      AccessoriesStr: []
    };
    // FILTERS
    uploader.filters.push({
      name: "imageFilter",
      fn: function(item /*{File|FileLikeObject}*/ , options) {
        var type = "|" + item.type.slice(item.type.lastIndexOf("/") + 1) + "|";
        return "|jpg|png|jpeg|bmp|gif|".indexOf(type) !== -1;
      }
    });
    // CALLBACKS
    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
      //console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
      if (uploader.queue.length > 1) {
        $scope.uploader.queue.splice(0, 1);
        //uploader.queue.splice(0,1);
      }
      console.log(uploader);
      //console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
      //console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
      //console.info('onBeforeUploadItem', item);
      //item.formData.push({Key: data.Messages});
      item.formData.push({
        Source: "41"
      });
      item.formData.push({
        Source: "1"
      });
      Array.prototype.push.apply(item.formData, uploader.formData);
    };
    var Func_Timeouter_Lock;
    $scope.Repair_Time = function() {
      if ($scope.TimesForLock == 200) {
        $timeout.cancel(Func_Timeouter_Lock);
        $scope.TimesForLock = 0;
        $scope.CheckUsed();
      } else {
        $scope.TimesForLock = $scope.TimesForLock + 1;
        Func_Timeouter_Lock = $timeout(function() {
          $scope.Repair_Time();
        }, 1);
      }
    };
    $scope.setCompany = function() {
      var modalInstance = $modal.open({
        templateUrl: 'unitsModal.html',
        controller: 'ntOrgPicker',
        resolve: {
          Options: function() {
            return {
              'Type': 'S1',
              'Source': 'n580440'
            };
          },
          Info: function() {
            return $scope.Info;
          },
          UnitInfo: function() {
            return '';
          }
        }
      });
      modalInstance.result.then(function(Unit) {
        $scope.MemberInfo.Nickname = Unit.Nickname;
        $scope.MemberInfo.Unit = Unit.Unit[0];
        $scope.MemberInfo.UnitId = Unit.UnitId[0];
        $scope.getTitles(Unit.CompanyId);
      });
    };
    $scope.setUnits = function() {
      var modalInstance = $modal.open({
        templateUrl: 'unitsModal.html',
        controller: 'ntOrgPicker',
        resolve: {
          Options: function() {
            return {
              'Type': 'S2',
              'Source': 'n580440'
            };
          },
          Info: function() {
            return $scope.Info;
          },
          UnitInfo: function() {
            return '';
          }
        }
      });
      modalInstance.result.then(function(Unit) {
        $scope.MemberInfo.Accessories = Unit.UnitId;
        $scope.MemberInfo.AccessoriesStr = Unit.Unit;
      });
    };
    $scope.getUnits = function(companyId) {
      DataMaintenanceFactory.getUnits($scope.Passport, apiType, companyId).success(function(data) {
        var units = data[3];
        $scope.MemberInfo.AccessoriesStr = [];
        _(units).each(function(item, index) {
          _($scope.MemberInfo.Accessories).each(function(id, index) {
            if (id == item.UnitId) {
              $scope.MemberInfo.AccessoriesStr.push(item.Unit);
            };
          });
        });
      }).error(function(error) {
        alert(error);
      });
    };
    //获取职称
    $scope.getTitles = function(companyId) {
      DataMaintenanceFactory.getTitles($scope.Passport, apiType, companyId).success(function(res) {
        $scope.TitleInfo = res;
        $('[name=titleList]').select2().on('select2-selecting', function(e) {
          $scope.MemberInfo.Title = e.object.text;
        }).on('select2-removed', function(e) {
          $scope.MemberInfo.Title = '';
        });
        if ($scope.MemberInfo.TitleId) {
          setTimeout(function() {
            $('[name=titleList]').select2("val", $scope.MemberInfo.TitleId);
          }, 0);
        }
      }).error(function(error) {
        alert(error);
      });
    };
    //判断电话号码是否已经使用
    $scope.CheckUsed = function() {
      $http({
        url: ApiMapper.sApi + "/s/phones/" + $scope.MemberInfo.Phone,
        method: "GET",
        contentType: "application/json",
        headers: {
          Passport: $scope.Passport
        }
      }).success(function(data) {
        if (data.UserInfo != null) {
          var MapId = data.UserInfo.UnitId;
          if (data.UserInfo.CallinType < 100) {
            var MapId = data.UserInfo.UserId;
          }
          if (data.Duplicate == 1 && MapId != $scope.UserId) {
            $scope.MessageTitle = data.UserInfo.Unit;
            if (data.UserInfo.CallinType < 100) {
              $scope.MessageTitle = data.UserInfo.UserName;
            }
            $scope.isInuse = true;
          } else {
            $scope.isInuse = false;
          }
        }
      });
    };
    //号码修改
    $scope.ChangePhoneNO = function() {
      $timeout.cancel(Func_Timeouter_Lock);
      $scope.TimesForLock = 0;
      $scope.Repair_Time();
    };
    //init 获取人员详细信息
    if (paramObj.eventId == "add") {
      $scope.isRead = true;
      $scope.MemberInfo.Status = 1;
    } else {
      $scope.isRead = false;
      if ($scope.UserId != "") {
        DataMaintenanceFactory.getUserDetail($scope.Passport, apiType, $scope.UserId).
        success(function(res) {
          $scope.MemberInfo = res;
          $scope._tmpArry = {};
          _.each($scope.MemberInfo.Contacts, function(_Items, i) {
            $scope._tmpArry = _Items;
            if ($scope._tmpArry.Type == 0) {
              $scope.MemberInfo.Email = $scope._tmpArry.Info;
            }
            if ($scope._tmpArry.Type == 1) {
              $scope.MemberInfo.Phone = $scope._tmpArry.Info;
            }
          });
          //获取管理部门名称列表
          if ($scope.MemberInfo.Accessories.length > 0) {
            $scope.getUnits($scope.MemberInfo.CompanyId);
          }
          /*获取职称列表*/
          $scope.getTitles($scope.MemberInfo.CompanyId);
          /*获取缩略图*/
          $scope.thumbnail = {};
          jQuery.ajax({
            url: ApiMapper.sApi + "/cc/thumbnail/size",
            type: "POST",
            async: false,
            headers: {
              Passport: $scope.Passport
            },
            contentType: "application/json",
            data: {
              Size: "L",
              CollectionId: encodeURIComponent($scope.MemberInfo.UserId)
            },
            success: function(data) {
              $scope.thumbnail = eval(data);
              if ($scope.thumbnail.length > 0) {
                $scope.Avator = ApiMapper.sApi + "/" + $scope.thumbnail[$scope.thumbnail.length - 1].Thumbnail.replace("/55497/", "");
                $scope.AvatorId = $scope.thumbnail[$scope.thumbnail.length - 1].Id;
                $("#imghead").show();
              }
            }
          });
        });
      }
    }
    /*弹出预览视窗*/
    $scope.showImg = function() {
      var url = "";
      if (jQuery("[id=AccountImg]").val() != "") {
        if (window.navigator.userAgent.indexOf("MSIE") >= 1) {
          // IE
          url = jQuery("[id=AccountImg]").val();
        } else if (window.navigator.userAgent.indexOf("Firefox") > 0) {
          // Firefox
          url = window.URL.createObjectURL(document.getElementById("AccountImg").files[0]);
        } else if (window.navigator.userAgent.indexOf("Chrome") > 0) {
          // Chrome
          url = window.URL.createObjectURL(document.getElementById("AccountImg").files[0]);
        }
      } else {
        if ($scope.AvatorId != null) {
          url = ApiMapper.ccApi + "/cc/image/" + $scope.AvatorId;
        }
      }
      if (url != "") {
        var ImgData = url;
        var modalInstance = $modal.open({
          templateUrl: "ShowImg.html",
          size: "lg",
          controller: "ShowImgCtrl",
          resolve: {
            ImgData: function() {
              return ImgData;
            }
          }
        });
      }
    };
    //上传进度图片
    var isUplodefilesed = false;
    $scope.Uplodefiles = function(data) {
      var _defer = $q.defer();
      var _Promise = _defer.promise;
      if (uploader.queue.length > 0) {
        uploader.onBeforeUploadItem = function(item) {
          item.formData.push({
            Key: $scope.UserId
          });
          item.formData.push({
            Source: "41"
          });
          item.formData.push({
            Type: "1"
          });
        };
        uploader.uploadAll();
        uploader.onCompleteAll = function() {
          _defer.resolve('success');
        };
      } else {
        _defer.resolve('success');
      }
      return _Promise;
    };
    //修改数据
    $scope.submitUserData = function(actionId) {
      var _defer = $q.defer();
      var _Promise = _defer.promise;
      var params = JSON.parse(JSON.stringify($scope.MemberInfo));
      if ($scope.MemberInfo.Level == 0) {
        params.IsManagerAccount = 0;
        params.Accessories = [];
      }
      if ($scope.MemberInfo.Level == 1) {
        params.IsManagerAccount = 1;
        params.IsManager = 0;
        params.IsAssistantManager = 1;
      };
      if (actionId == "add") {
        DataMaintenanceFactory.createUser($scope.Passport, apiType, params).success(function(data) {
          _defer.resolve('success');
        }).error(function(error) {
          _defer.reject(error.Messages);
        });
      } else {
        DataMaintenanceFactory.updateUser($scope.Passport, apiType, params).success(function(data) {
          _defer.resolve('success');
        }).error(function(error) {
          _defer.reject(error.Messages);
        });
      }
      return _Promise;
    };
    $scope.btnOk = function() {
      if (!$scope.isInuse) {
        $scope.alertStr = "";
        if ($scope.MemberInfo.Status == "0") {
          if ($scope.MemberInfo.Remark == null || $scope.MemberInfo.Remark == "") {
            $scope.alertStr += "\r\n请填写帐号停用原因！";
          }
        }
        if ($scope.MemberInfo.UserId != "" && paramObj.eventId == "add") {
          jQuery.ajax({
            url: `${ApiMapper.sApi}/${apiType}/user/${$scope.MemberInfo.UserId}`,
            type: "GET",
            async: false,
            headers: {
              Passport: $scope.Passport
            },
            contentType: "application/json",
            success: function(data) {
              $scope.alertStr += "\r\n此工号已存在！";
            },
            error: function() {
              $scope.alertStr = "";
            }
          });
        }
        if ($scope.alertStr != "") {
          //alert($scope.alertStr);
          $scope.$toastlast = toastr['error']($scope.alertStr, "");
          return false;
        }
        $scope.TitlName = "";
        if (angular.isDefined($scope.MemberInfo.Title)) {
          for (var k = 0; k < $scope.TitleInfo.length; k++) {
            if ($scope.MemberInfo.Title == $scope.TitleInfo[k].TitleId) {
              $scope.TitleName = $scope.TitleInfo[k].Title;
            }
          }
        }
        /*上传图片 先删除旧有图档,然后上传新图片*/
        if (jQuery("[id=AccountImg]").val() != "") {
          if ($scope.AvatorId != null) {
            jQuery.ajax({
              url: ApiMapper.ccApi + "/cc/file/" + $scope.AvatorId,
              type: "DELETE",
              async: false,
              contentType: "application/json",
              headers: {
                Passport: $scope.Passport
              },
              success: function(data) {
                $scope.Flag = true;
              }
            });
            if (!$scope.Flag) {
              alert("图片删除失败！");
              return false;
            }
          }
        }
        var submitPromise = $scope.submitUserData(paramObj.eventId); //提交数据
        submitPromise.then(function(res) {
          if (res == 'success') {
            var UplodefilesPromise = $scope.Uplodefiles();
            return UplodefilesPromise;
          }
        }).then(function(res) {
          if (res == 'success') {
            $scope.$toastlast = toastr['success']("修改人员信息成功", "");
            $modalInstance.close(paramObj.deptId);
          }
        })
      }
    };
    $scope.btnCancel = function() {
      $modalInstance.dismiss("cancel");
    };
  }
]).controller("ShowImgCtrl", ["$scope", "$modalInstance", "ImgData",
  function($scope, $modalInstance, ImgData) {
    $scope.showImgData = ImgData;
  }
]);