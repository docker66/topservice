angular.module("n580440").controller("subclassModifyCtrl", ["$scope", "$http", "$modal", "$timeout", "$q", "ngProgress", "i18n",
  function($scope, $http, $modal, $timeout, $q, ngProgress, i18n) {
    /***************************/
    /***    [設定] 通用參數   ***/
    /***************************/
    ngProgress.start(); // 進度條
    $scope.Passport = Cookies.get("Passport");
    $scope.isModify = ""; //判斷是否有變更紀錄
    $scope.isLeave = ""; // 設定為離開此頁面
    // 綁定選擇
    $scope.equipment = {
      EquipmentName: "", //设备名称
      EquipmentTypeClass: 0, //大类
      EquipmentTypeId: "", //中类
      EquipmentTypeName: "", //中类名稱
      EquipmentTypeStatus: "", //中类狀態
      EquipmentTypelistStart: [], //目前啟用的的中類清單
      EquipmentSubTypeId: "", //小类
      EquipmentSubTypeName: "", //小类名称
      EquipmentSubTypeStatus: "", //小类狀態
      Status: "" // 狀態選擇
    };
    // 綁定獲取資料
    $scope.EquipmentTypeList = {
      EquipmentTypeClass: [], //大類
      EquipmentType: [], //中類
      EquipmentSubType: [] //小類
    };
    // // 分頁設定
    $scope.pageData = ""; //數據源
    //分頁總數
    $scope.pageSize = 5;
    $scope.pages = 1; //分頁數
    $scope.pageList = [];
    $scope.selPage = 1;
    // 權限獲取
    $scope.TimesForLock = 0;
    $scope.LockTimeForShow = "";
    let Func_Timeouter_Lock;
    // 0:沒人獲取 1:自己獲取 2:已有人獲取 3:是否重新獲取
    $scope.Permission = {
      Status: 0,
      UserName: ""
    };
    // 狀態選擇
    $scope.select = {
      id: 2 // 預設給全選
    };
    // 下拉選單狀態
    $scope.status = [{
      name: "全部",
      id: 2
    }, {
      name: "启用",
      id: 1
    }, {
      name: "停用",
      id: 0
    }];
    /***************************/
    /***   [設定] 權限獲取     ***/
    /***************************/
    // 判斷權限是否有人獲取 需獲取才能控制 使用promise
    $scope.checkPermission = function() {
      var mydefer = $q.defer();
      var myPromise = mydefer.promise;
      // 已獲取 不重複獲取
      if ($scope.Permission.Status == 1) {
        mydefer.resolve("success");
      } else {
        // 0:沒人獲取 1:自己獲取 2:已有人獲取
        $scope.Permission.Status = 0;
        $scope.Permission.UserName = "";
        $scope.TimesForLock = 0; // 時間到了鎖定
        $scope.LockTimeForShow = "";
        $http({
          url: ApiMapper.sApi + "/s/user/identity/equiptype/ishasAuthority",
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: $scope.Passport
          }
        }).success(function(data) {
          if (data.IsHasAuthority) {
            $scope.Permission.Status = 1;
            $scope.ReLock(); //給權限
            mydefer.resolve("success");
          } else {
            $scope.Permission.Status = 2;
            $scope.Permission.UserName = data.UserName;
          }
        }).error(function(data) {
          $scope.$toastlast = toastr["error"](data.Messages, "");
        });
      }
      return myPromise;
    };
    // 鎖定權限
    $scope.ReLock = function() {
      $http({
        url: ApiMapper.sApi + "/s/user/identity/equiptype/isGive",
        method: "POST",
        contentType: "application/json",
        headers: {
          Passport: $scope.Passport
        },
        data: {
          IsGive: 1 // 1是給權限 2是放棄權限
        }
      }).success(function(data) {
        // 鎖定成功時 後端會開始計算15分鐘後自動釋放權限 這邊要配合設定的時間
        $timeout.cancel(Func_Timeouter_Lock);
        $scope.Repair_Lock_Time();
      }).error(function(data) {
        $scope.$toastlast = toastr["error"](data.Messages, "");
      });
    };
    // 解鎖權限
    $scope.ReUnlock = function() {
      $http({
        url: ApiMapper.sApi + "/s/user/identity/equiptype/isGive",
        method: "POST",
        contentType: "application/json",
        headers: {
          Passport: $scope.Passport
        },
        data: {
          IsGive: 0 //0是放棄權限 1是給權限
        }
      }).success(function(data) {}).error(function(data) {
        $scope.$toastlast = toastr["error"](data.Messages, "");
      });
    };
    // 釋放權限 - controller銷毀時 解鎖
    $scope.$on("$destroy", function() {
      $scope.isLeave = true;
      $scope.ReUnlock();
    });
    // 釋放權限 -  關閉/刷新頁面時 解锁
    window.onbeforeunload = function() {
      // ajax 设为同步 , 设异步会被跳过直接执行刷新或关闭.
      $http({
        url: ApiMapper.sApi + "/s/user/identity/equiptype/isGive",
        method: "POST",
        contentType: "application/json",
        headers: {
          Passport: $scope.Passport
        },
        data: {
          IsGive: 0
        },
        async: false
      }).success(function(data) {
        console.log(data);
      }).error(function(data) {
        $scope.$toastlast = toastr["error"](data.Messages, "");
      });
    };
    // 編輯權限計時器
    $scope.Repair_Lock_Time = function() {
      // 設定秒數 15分鐘 視窗有1分鐘倒數所以是14分鐘>>840秒
      if ($scope.TimesForLock == 840) {
        $scope.LockTimeForShow = "";
        $scope.openForLockModal();
        $scope.Permission.Status = 0; //跳出是否繼續編輯modal
      }
      $scope.TimesForLock = $scope.TimesForLock + 1;
      var min = parseInt($scope.TimesForLock / 60);
      var lastsecs = $scope.TimesForLock % 60;
      $scope.LockTimeForShow = min + "分" + lastsecs + "秒";
      Func_Timeouter_Lock = $timeout(function() {
        if (!$scope.isLeave) {
          $scope.Repair_Lock_Time();
        } else {
          return;
        }
      }, 1000);
    };
    // 保持權限
    $scope.KeepForLock = function() {
      $timeout.cancel(Func_Timeouter_Lock); //取消编辑权计时器
      $scope.TimesForLock = 0; //重置编辑权计时器
      $scope.LockTimeForShow = "";
      // 不要讓直接鎖定權限,若剛好有人獲取成功會造成同時多人編輯狀況
      // 先解鎖後,再次判斷有沒有人獲取後給權限
      $scope.Permission.Status = 0;
      $scope.ReUnlock();
      setTimeout(function() {
        $scope.checkPermission();
      }, 500);
    };
    // 放棄權限
    $scope.CancelForLock = function() {
      $timeout.cancel(Func_Timeouter_Lock); //取消提醒计时器
      $scope.TimesForLock = 0; //重置编辑权计时器
      $scope.LockTimeForShow = "";
      //解锁 權限权
      $scope.Permission.Status = 0;
      $scope.ReUnlock();
    };
    /***************************/
    /***   [設定] 分頁設定     ***/
    /***************************/
    // 設定分頁資訊
    $scope.setPageInfo = function() {
      // 分頁設定
      // 選擇中類時會綁定此中類下的小類,來獲取小類長度
      let arr = [];
      $scope.EquipmentTypeList.EquipmentSubType.forEach(EquipmentSub => {
        if (EquipmentSub.EquipmentTypeId == $scope.equipment.EquipmentTypeId) {
          arr.push(EquipmentSub);
        }
      });
      $scope.pageData = arr; //數據源
      $scope.pages = Math.ceil($scope.pageData.length / $scope.pageSize);
      $scope.newPages = $scope.pages > 5 ? 5 : $scope.pages;
      $scope.pageList = [];
      $scope.selPage = 1;
      $scope.EquipmentSubTypePage = $scope.pageData.slice(0, $scope.pageSize);
      //分頁要repeat的數組
      for (var i = 0; i < $scope.newPages; i++) {
        $scope.pageList.push(i + 1);
      }
    };
    //設置表格數據源(切換分頁獲取資訊)
    $scope.setData = function() {
      $scope.EquipmentSubTypePage = $scope.pageData.slice($scope.pageSize * ($scope.selPage - 1), $scope.selPage * $scope.pageSize); //通過當前頁數篩選出表格當前顯示數據
    };
    //設置當前選中頁樣式
    $scope.isActivePage = function(page) {
      return $scope.selPage == page;
    };
    //上一頁
    $scope.Previous = function() {
      $scope.selectPage($scope.selPage - 1);
    };
    //下一頁
    $scope.Next = function() {
      $scope.selectPage($scope.selPage + 1);
    };
    //打印當前選中頁索引
    $scope.selectPage = function(page) {
      //不能小於1大於最大
      if (page < 1 || page > $scope.pages) return;
      //最多顯示分頁數5
      if (page > 1) {
        //因為只顯示5個頁數，大於2頁開始分頁轉換
        var newpageList = [];
        for (var i = page - 2; i < (page + 2 > $scope.pages ? $scope.pages : page + 2); i++) {
          newpageList.push(i + 1);
        }
        $scope.pageList = newpageList;
      }
      $scope.selPage = page;
      $scope.setData();
      $scope.isActivePage(page);
    };
    /***************************/
    /*** [設定] 其餘function  ***/
    /***************************/
    // 判斷設備類是否有未變更紀錄
    $scope.GetModify = function() {
      $http({
        url: ApiMapper.sApi + "/s/equipmenttypes/notyet/",
        method: "GET",
        contentType: "application/json",
        headers: {
          Passport: $scope.Passport
        }
      }).success(function(data) {
        $scope.isModify = data;
      });
    };
    $scope.GetModify();
    //依狀態獲取分類or重新獲取資料
    $scope.GetEquipmentType = function(item) {
      $http({
        url: ApiMapper.sApi + "/s/equipmenttypes/filter/" + $scope.select.id,
        method: "GET",
        contentType: "application/json",
        headers: {
          Passport: $scope.Passport
        }
      }).success(function(data) {
        console.log(data);
        // 如果是選擇狀態,就把原本點選保存資料清空
        // 不是選擇狀態,代表是api要更新EquipmentTypeList,不須清空原本選擇
        if (item == "getStatus") {
          $scope.equipment.EquipmentTypeStatus = "";
          $scope.equipment.EquipmentTypeId = "";
          $scope.equipment.EquipmentSubTypeId = "";
          $scope.equipment.EquipmentTypeName = "";
          $scope.equipment.EquipmentSubTypeName = "";
        }
        $scope.EquipmentTypeList = data;
        // 如果中類只有某幾項小類停用,在停用狀態不該把中類設為停用
        // 所以在啟用或全選狀態來獲取目前正在啟用的中類清單交叉比對
        if ($scope.select.id == 1 || $scope.select.id == 2) {
          $scope.EquipmentTypeList.EquipmentType.forEach(EquipmentType => {
            if (EquipmentType.Status == 1) {
              $scope.equipment.EquipmentTypelistStart.push(EquipmentType);
            }
          });
        }
        if ($scope.select.id == 0) {
          $scope.EquipmentTypeList.EquipmentType.forEach(EquipmentType => {
            $scope.equipment.EquipmentTypelistStart.forEach(EquipmentTypeStart => {
              if (EquipmentType.EquipmentTypeId == EquipmentTypeStart.EquipmentTypeId) {
                EquipmentType.Status = 1;
              }
            });
          });
        }
        // 因為後端給的資料,中類啟用但之下如果有小類停用,此中類會被分為停用和啟用2種bug,這邊加入判斷條件可以去找出"中類啟用但小類停用之選項"
        if ($scope.select.id == 2) {
          //全選狀態
          $scope.EquipmentTypeList.EquipmentType.forEach(EquipmentType => {
            if (EquipmentType.Status == 0) {
              //中類停用狀態
              $scope.EquipmentTypeList.EquipmentSubType.forEach(EquipmentSubType => {
                if (EquipmentSubType.EquipmentTypeId == EquipmentType.EquipmentTypeId && EquipmentSubType.Status == 1 //小類卻是啟動狀態
                ) {
                  EquipmentType.hide = true; //加入隱藏條件於html
                }
              });
            }
          });
        }
        // 重新獲取分頁資訊
        $scope.setPageInfo();
        // 讓Ajax完成才改變status 否則畫面會delay
        $scope.equipment.Status = $scope.select.id;
        ngProgress.complete();
      }).error(function() {});
    };
    $scope.GetEquipmentType();
    // 所選擇之大類
    $scope.setEquipmentTypeClass = function(item) {
      $scope.equipment.EquipmentTypeClass = item.EquipmentClass;
      $scope.equipment.EquipmentTypeStatus = "";
      $scope.equipment.EquipmentTypeId = "";
      $scope.equipment.EquipmentSubTypeId = "";
      $scope.equipment.EquipmentTypeName = "";
      $scope.equipment.EquipmentSubTypeName = "";
    };
    // 所選擇之中類
    $scope.setEquipmentType = function(item) {
      $scope.equipment.EquipmentTypeId = item.EquipmentTypeId;
      $scope.equipment.EquipmentTypeName = item.EquipmentTypeName;
      $scope.equipment.EquipmentTypeStatus = item.Status;
      $scope.setPageInfo();
    };
    // 拖曳功能 (小類轉中類)
    $scope.onDropComplete = function(EquipmentSubType, EquipmentType) {
      let checkPermission = $scope.checkPermission();
      checkPermission.then(function(data) {
        if ((data = "success")) {
          //沒人使用才能執行以下功能
          $scope.equipment.EquipmentSubTypeStatus = EquipmentSubType.Status;
          $scope.equipment.EquipmentTypeClass = EquipmentSubType.EquipmentTypeClass;
          $scope.equipment.EquipmentTypeId = EquipmentSubType.EquipmentTypeId;
          $scope.equipment.EquipmentTypeName = EquipmentSubType.EquipmentTypeName;
          $scope.equipment.EquipmentSubTypeId = EquipmentSubType.EquipmentSubTypeId;
          $scope.equipment.EquipmentSubTypeName = EquipmentSubType.EquipmentSubTypeName;
          //傳給後端的資料
          let request = {
            ActionType: 11,
            EquipmentType: {},
            TypeChange: {
              TypeChange: {
                OldEquipmentTypeClass: $scope.equipment.EquipmentTypeClass,
                OldEquipmentTypeId: $scope.equipment.EquipmentTypeId,
                OldEquipmentTypeName: $scope.equipment.EquipmentTypeName,
                OldEquipmentSubTypeId: $scope.equipment.EquipmentSubTypeId,
                OldEquipmentSubTypeName: $scope.equipment.EquipmentSubTypeName,
                EquipmentTypeId: EquipmentType.EquipmentTypeId,
                EquipmentTypeName: EquipmentType.EquipmentTypeName
              },
              Status: $scope.equipment.EquipmentSubTypeStatus
            }
          };
          $http({
            //新增中類API
            url: ApiMapper.sApi + "/s/equipmenttypes/action",
            method: "POST",
            contentType: "application/json",
            headers: {
              Passport: $scope.Passport
            },
            data: request
          }).success(function(data) {
            if (data.StatusCode == 1) {
              $scope.$toastlast = toastr["success"]('转移成功', "");
              $scope.GetEquipmentType();
            } else {
              $scope.$toastlast = toastr["error"](data.Messages, "");
            }
          }).error(function(data) {
            $scope.$toastlast = toastr["error"]('转移失败', "");
          });
        }
      });
    };
    /****************************/
    /****    [呼叫] Modal     ***/
    /****************************/
    // 新增小類
    $scope.addEquipmentSubType = function() {
      // 點選可修改選項就獲取權限
      let checkPermission = $scope.checkPermission();
      checkPermission.then(function(data) {
        if ((data = "success")) {
          var modalInstance = $modal.open({
            templateUrl: "classCtr", //模板窗口的地址
            size: "sm", //大小配置不能自定义大小，只能是sm，md，lg等这些值
            controller: "classCtr", //$modal指定的控制器 寫在下面
            resolve: {
              options: {
                EquipmentTypeList: $scope.EquipmentTypeList,
                equipment: $scope.equipment,
                title: '新增小类',
                icon: "fa fa-check-square-o",
                iconColor: "text-blue",
                setFeature: "addEquipmentSubType"
              }
              //定義一個obj傳遞給$modal
            }
          });
          modalInstance.result.then(function(result) {
            var newEquipmentSubTypeName = result.newEquipmentSubTypeName;
            if (newEquipmentSubTypeName != "" && newEquipmentSubTypeName != null) {
              // 傳給後端的資料
              let request = {
                ActionType: 1,
                EquipmentType: {
                  EquipmentTypeId: $scope.equipment.EquipmentTypeId,
                  EquipmentTypeName: $scope.equipment.EquipmentTypeName,
                  EquipmentSubTypeName: newEquipmentSubTypeName,
                  EquipmentTypeClass: $scope.equipment.EquipmentTypeClass
                },
                TypeChange: {}
              };
              $http({
                // 新增小類API
                url: ApiMapper.sApi + "/s/equipmenttypes/action",
                method: "POST",
                contentType: "application/json",
                headers: {
                  Passport: $scope.Passport
                },
                data: request
              }).success(function(data) {
                if (data.StatusCode == 1) {
                  $scope.$toastlast = toastr["success"]('新增成功', "");
                  $scope.GetEquipmentType();
                } else {
                  $scope.$toastlast = toastr["error"](data.Messages, "");
                }
              }).error(function(data) {
                $scope.$toastlast = toastr["error"]('新增失败', "");
              });
            }
          });
        }
      });
    };
    // 新增中類
    $scope.addEquipmentType = function() {
      let checkPermission = $scope.checkPermission();
      checkPermission.then(function(data) {
        if ((data = "success")) {
          var modalInstance = $modal.open({
            templateUrl: "classCtr", //模板窗口的地址
            size: "sm", //大小配置不能自定义大小，只能是sm，md，lg等这些值
            controller: "classCtr", //$modal指定的控制器 寫在下面
            resolve: {
              options: {
                EquipmentTypeList: $scope.EquipmentTypeList,
                equipment: $scope.equipment,
                title: '新增中类',
                icon: "fa fa-check-square-o",
                iconColor: "text-blue",
                setFeature: "addEquipmentType"
              }
              //定義一個obj傳遞給$modal
            }
          });
          modalInstance.result.then(function(result) {
            var newEquipmentTypeName = result.newEquipmentTypeName;
            var newEquipmentSubTypeName = result.newEquipmentSubTypeName;
            if (newEquipmentTypeName != "" && newEquipmentTypeName != null && newEquipmentSubTypeName != "" && newEquipmentSubTypeName != null) {
              // 系統規定新增中類一定要再新增一個小類
              // 傳給後端的資料
              let request = {
                ActionType: 0,
                EquipmentType: {
                  EquipmentTypeName: newEquipmentTypeName,
                  EquipmentSubTypeName: newEquipmentSubTypeName,
                  EquipmentTypeClass: $scope.equipment.EquipmentTypeClass
                },
                TypeChange: {}
              };
              $http({
                //新增中類API
                url: ApiMapper.sApi + "/s/equipmenttypes/action",
                method: "POST",
                contentType: "application/json",
                headers: {
                  Passport: $scope.Passport
                },
                data: request
              }).success(function(data) {
                if (data.StatusCode == 1) {
                  $scope.$toastlast = toastr["success"]('新增成功', "");
                  $scope.GetEquipmentType();
                } else {
                  $scope.$toastlast = toastr["error"](data.Messages, "");
                }
              }).error(function(data) {
                $scope.$toastlast = toastr["error"]('新增失败', "");
              });
            } else {
              $scope.$toastlast = toastr["error"]('新增失败', "");
            }
          });
        }
      });
    };
    // 小類改名
    $scope.newEquipmentSubTypeName = function(item) {
      let checkPermission = $scope.checkPermission();
      checkPermission.then(function(data) {
        if ((data = "success")) {
          var modalInstance = $modal.open({
            templateUrl: "classCtr", //模板窗口的地址
            size: "sm", //大小配置不能自定义大小，只能是sm，md，lg等这些值
            controller: "classCtr", //$modal指定的控制器 寫在下面
            resolve: {
              options: {
                EquipmentTypeList: $scope.EquipmentTypeList,
                equipment: $scope.equipment,
                title: '小类改名',
                icon: "fa fa-pencil",
                iconColor: "text-gray",
                placeholder: '请输入新小类名称',
                setFeature: "newEquipmentSubTypeName"
              }
              //定義一個obj傳遞給$modal
            }
          });
          modalInstance.result.then(function(result) {
            $scope.equipment.EquipmentTypeClass = item.EquipmentTypeClass;
            $scope.equipment.EquipmentTypeId = item.EquipmentTypeId;
            $scope.equipment.EquipmentTypeName = item.EquipmentTypeName;
            $scope.equipment.EquipmentSubTypeId = item.EquipmentSubTypeId;
            $scope.equipment.EquipmentSubTypeName = item.EquipmentSubTypeName;
            $scope.equipment.EquipmentSubTypeStatus = item.Status;
            //沒人使用才能執行以下功能
            var newEquipmentSubType = result.newName;
            if (newEquipmentSubType != "" && newEquipmentSubType != null) {
              // 傳給後端的資料
              let request = {
                ActionType: 9,
                EquipmentType: {},
                TypeChange: {
                  TypeChange: {
                    OldEquipmentTypeClass: $scope.equipment.EquipmentTypeClass,
                    OldEquipmentTypeId: $scope.equipment.EquipmentTypeId,
                    OldEquipmentTypeName: $scope.equipment.EquipmentTypeName,
                    OldEquipmentSubTypeId: $scope.equipment.EquipmentSubTypeId,
                    OldEquipmentSubTypeName: $scope.equipment.EquipmentSubTypeName,
                    EquipmentTypeClass: $scope.equipment.EquipmentTypeClass,
                    EquipmentSubTypeId: $scope.equipment.EquipmentSubTypeId,
                    EquipmentSubTypeName: newEquipmentSubType
                  },
                  Status: $scope.equipment.EquipmentSubTypeStatus
                }
              };
              $http({
                //新增中類API
                url: ApiMapper.sApi + "/s/equipmenttypes/action",
                method: "POST",
                contentType: "application/json",
                headers: {
                  Passport: $scope.Passport
                },
                data: request
              }).success(function(data) {
                if (data.StatusCode == 1) {
                  $scope.$toastlast = toastr["success"]('更改成功', "");
                  $scope.GetEquipmentType();
                } else {
                  $scope.$toastlast = toastr["error"](data.Messages, "");
                }
              }).error(function(data) {
                $scope.$toastlast = toastr["error"]('更改失败', "");
              });
            }
          });
        }
      });
    };
    // 中類改名
    $scope.newEquipmentTypeName = function() {
      let checkPermission = $scope.checkPermission();
      checkPermission.then(function(data) {
        if ((data = "success")) {
          var modalInstance = $modal.open({
            templateUrl: "classCtr", //模板窗口的地址
            size: "sm", //大小配置不能自定义大小，只能是sm，md，lg等这些值
            controller: "classCtr", //$modal指定的控制器 寫在下面
            resolve: {
              options: {
                EquipmentTypeList: $scope.EquipmentTypeList,
                equipment: $scope.equipment,
                title: "中类改名",
                icon: "fa fa-pencil",
                iconColor: "text-gray",
                placeholder: "请输入新中类名称",
                setFeature: "newEquipmentTypeName"
              }
              //定義一個obj傳遞給$modal
            }
          });
          modalInstance.result.then(function(result) {
            var newEquipmentType = result.newName;
            if (newEquipmentType != "" && newEquipmentType != null) {
              // 傳給後端的資料
              let request = {
                ActionType: 8,
                EquipmentType: {},
                TypeChange: {
                  TypeChange: {
                    OldEquipmentTypeClass: $scope.equipment.EquipmentTypeClass,
                    OldEquipmentTypeId: $scope.equipment.EquipmentTypeId,
                    OldEquipmentTypeName: $scope.equipment.EquipmentTypeName,
                    EquipmentTypeClass: $scope.equipment.EquipmentTypeClass,
                    EquipmentTypeId: $scope.equipment.EquipmentTypeId,
                    EquipmentTypeName: newEquipmentType
                  },
                  Status: $scope.equipment.EquipmentTypeStatus
                }
              };
              $http({
                //新增中類API
                url: ApiMapper.sApi + "/s/equipmenttypes/action",
                method: "POST",
                contentType: "application/json",
                headers: {
                  Passport: $scope.Passport
                },
                data: request
              }).success(function(data) {
                if (data.StatusCode == 1) {
                  $scope.$toastlast = toastr["success"]("更改成功", "");
                  $scope.GetEquipmentType();
                } else {
                  $scope.$toastlast = toastr["error"](data.Messages, "");
                }
              }).error(function(data) {
                $scope.$toastlast = toastr["error"]("更改失败", "");
              });
            }
          });
        }
      });
    };
    // 停用小類
    $scope.EquipmentSubTypeStop = function(item) {
      let checkPermission = $scope.checkPermission();
      checkPermission.then(function(data) {
        if ((data = "success")) {
          //沒人使用才能執行以下功能
          $scope.equipment.EquipmentTypeClass = item.EquipmentTypeClass;
          $scope.equipment.EquipmentTypeId = item.EquipmentTypeId;
          $scope.equipment.EquipmentTypeName = item.EquipmentTypeName;
          $scope.equipment.EquipmentSubTypeId = item.EquipmentSubTypeId;
          $scope.equipment.EquipmentSubTypeName = item.EquipmentSubTypeName;
          var modalInstance = $modal.open({
            templateUrl: "classCtr", //模板窗口的地址
            size: "sm", //大小配置不能自定义大小，只能是sm，md，lg等这些值
            controller: "classCtr", //$modal指定的控制器 寫在下面
            resolve: {
              options: {
                item: item,
                EquipmentTypeList: $scope.EquipmentTypeList,
                equipment: $scope.equipment,
                title: "小类停用",
                icon: "fa fa-minus-circle",
                iconColor: "text-red",
                setFeature: "EquipmentSubTypeStop"
              }
              //定義一個obj傳遞給$modal
            }
          });
          modalInstance.result.then(function(result) {
            // 傳給後端的資料
            let request = {
              ActionType: 5,
              EquipmentType: {
                EquipmentTypeId: $scope.equipment.EquipmentTypeId,
                EquipmentTypeName: $scope.equipment.EquipmentTypeName,
                EquipmentSubTypeId: $scope.equipment.EquipmentSubTypeId,
                EquipmentSubTypeName: $scope.equipment.EquipmentSubTypeName,
                EquipmentTypeClass: $scope.equipment.EquipmentTypeClass
              },
              TypeChange: {}
            };
            $http({
              // 停用小類API
              url: ApiMapper.sApi + "/s/equipmenttypes/action",
              method: "POST",
              contentType: "application/json",
              headers: {
                Passport: $scope.Passport
              },
              data: request
            }).success(function(data) {
              if (data.StatusCode == 1) {
                $scope.$toastlast = toastr["success"]("停用成功", "");
                $scope.equipment.EquipmentTypeId = "";
                $scope.GetEquipmentType();
              } else {
                $scope.$toastlast = toastr["error"](data.Messages, "");
              }
            }).error(function(data) {
              $scope.$toastlast = toastr["error"]("停用失败", "");
            });
          });
        }
      });
    };
    // 停用中類
    $scope.EquipmentTypeStop = function(item) {
      let checkPermission = $scope.checkPermission();
      checkPermission.then(function(data) {
        if ((data = "success")) {
          //沒人使用才能執行以下功能
          var modalInstance = $modal.open({
            templateUrl: "classCtr", //模板窗口的地址
            size: "sm", //大小配置不能自定义大小，只能是sm，md，lg等这些值
            controller: "classCtr", //$modal指定的控制器 寫在下面
            resolve: {
              options: {
                item: item,
                EquipmentTypeList: $scope.EquipmentTypeList,
                equipment: $scope.equipment,
                title: "中类停用",
                icon: "fa fa-minus-circle",
                iconColor: "text-red",
                setFeature: "EquipmentTypeStop"
              }
              //定義一個obj傳遞給$modal
            }
          });
          modalInstance.result.then(function(result) {
            // 傳給後端的資料
            let request = {
              ActionType: 4,
              EquipmentType: {
                EquipmentTypeId: $scope.equipment.EquipmentTypeId,
                EquipmentTypeName: $scope.equipment.EquipmentTypeName,
                EquipmentTypeClass: $scope.equipment.EquipmentTypeClass
              },
              TypeChange: {}
            };
            $http({
              // 新增小類API
              url: ApiMapper.sApi + "/s/equipmenttypes/action",
              method: "POST",
              contentType: "application/json",
              headers: {
                Passport: $scope.Passport
              },
              data: request
            }).success(function(data) {
              if (data.StatusCode == 1) {
                $scope.$toastlast = toastr["success"]("停用成功", "");
                // 停用中類要把佔存啟用清單清空
                $scope.equipment.EquipmentTypelistStart = [];
                $scope.equipment.EquipmentTypeId = "";
                $scope.GetEquipmentType();
              } else {
                $scope.$toastlast = toastr["error"](data.Messages, "");
              }
            }).error(function(data) {
              $scope.$toastlast = toastr["error"]("停用失败", "");
            });
          });
        }
      });
    };
    // 啟用小類
    $scope.EquipmentSubTypeStart = function(item) {
      let checkPermission = $scope.checkPermission();
      checkPermission.then(function(data) {
        if ((data = "success")) {
          $scope.equipment.EquipmentTypeClass = item.EquipmentTypeClass;
          $scope.equipment.EquipmentTypeId = item.EquipmentTypeId;
          $scope.equipment.EquipmentTypeName = item.EquipmentTypeName;
          $scope.equipment.EquipmentSubTypeId = item.EquipmentSubTypeId;
          $scope.equipment.EquipmentSubTypeName = item.EquipmentSubTypeName;
          //沒人使用才能執行以下功能
          var modalInstance = $modal.open({
            templateUrl: "classCtr", //模板窗口的地址
            size: "sm", //大小配置不能自定义大小，只能是sm，md，lg等这些值
            controller: "classCtr", //$modal指定的控制器 寫在下面
            resolve: {
              options: {
                item: item,
                EquipmentTypeList: $scope.EquipmentTypeList,
                equipment: $scope.equipment,
                title: "小类启用",
                icon: "fa fa-refresh",
                iconColor: "text-green",
                setFeature: "EquipmentSubTypeStart"
              }
            }
          });
          modalInstance.result.then(function(result) {
            let request = {
              ActionType: 7,
              EquipmentType: {
                EquipmentTypeId: $scope.equipment.EquipmentTypeId,
                EquipmentTypeName: $scope.equipment.EquipmentTypeName,
                EquipmentSubTypeId: $scope.equipment.EquipmentSubTypeId,
                EquipmentSubTypeName: $scope.equipment.EquipmentSubTypeName,
                EquipmentTypeClass: $scope.equipment.EquipmentTypeClass
              },
              TypeChange: {}
            };
            $http({
              // 停用小類API
              url: ApiMapper.sApi + "/s/equipmenttypes/action",
              method: "POST",
              contentType: "application/json",
              headers: {
                Passport: $scope.Passport
              },
              data: request
            }).success(function(data) {
              if (data.StatusCode == 1) {
                $scope.$toastlast = toastr["success"]("启用成功", "");
                $scope.equipment.EquipmentTypeId = "";
                $scope.GetEquipmentType();
              } else {
                $scope.$toastlast = toastr["error"](data.Messages, "");
              }
            }).error(function(data) {
              $scope.$toastlast = toastr["error"]("启用失败", "");
            });
          });
        }
      });
    };
    // 啟用中類
    $scope.EquipmentTypeStart = function(item) {
      let checkPermission = $scope.checkPermission();
      checkPermission.then(function(data) {
        if ((data = "success")) {
          //沒人使用才能執行以下功能
          var modalInstance = $modal.open({
            templateUrl: "classCtr", //模板窗口的地址
            size: "sm", //大小配置不能自定义大小，只能是sm，md，lg等这些值
            controller: "classCtr", //$modal指定的控制器 寫在下面
            resolve: {
              options: {
                item: item,
                EquipmentTypeList: $scope.EquipmentTypeList,
                equipment: $scope.equipment,
                title: "中类启用",
                icon: "fa fa-refresh",
                iconColor: "text-green",
                setFeature: "EquipmentTypeStart"
              }
              //定義一個obj傳遞給$modal
            }
          });
          modalInstance.result.then(function(result) {
            // 傳給後端的資料
            let request = {
              ActionType: 6,
              EquipmentType: {
                EquipmentTypeId: $scope.equipment.EquipmentTypeId,
                EquipmentTypeName: $scope.equipment.EquipmentTypeName,
                EquipmentTypeClass: $scope.equipment.EquipmentTypeClass
              },
              TypeChange: {}
            };
            $http({
              // 新增小類API
              url: ApiMapper.sApi + "/s/equipmenttypes/action",
              method: "POST",
              contentType: "application/json",
              headers: {
                Passport: $scope.Passport
              },
              data: request
            }).success(function(data) {
              if (data.StatusCode == 1) {
                $scope.$toastlast = toastr["success"]("启用成功", "");
                $scope.equipment.EquipmentTypeId = "";
                $scope.GetEquipmentType();
              } else {
                $scope.$toastlast = toastr["error"](data.Messages, "");
              }
            }).error(function(data) {
              $scope.$toastlast = toastr["error"]("启用失败", "");
            });
          });
        }
      });
    };
    // 刪除小類
    $scope.EquipmentSubTypeDel = function(item) {
      let checkPermission = $scope.checkPermission();
      checkPermission.then(function(data) {
        if ((data = "success")) {
          //沒人使用才能執行以下功能
          $scope.equipment.EquipmentTypeClass = item.EquipmentTypeClass;
          $scope.equipment.EquipmentTypeId = item.EquipmentTypeId;
          $scope.equipment.EquipmentTypeName = item.EquipmentTypeName;
          $scope.equipment.EquipmentSubTypeId = item.EquipmentSubTypeId;
          $scope.equipment.EquipmentSubTypeName = item.EquipmentSubTypeName;
          var modalInstance = $modal.open({
            templateUrl: "classCtr", //模板窗口的地址
            size: "sm", //大小配置不能自定义大小，只能是sm，md，lg等这些值
            controller: "classCtr", //$modal指定的控制器 寫在下面
            resolve: {
              options: {
                item: item,
                EquipmentTypeList: $scope.EquipmentTypeList,
                equipment: $scope.equipment,
                title: "小类删除",
                icon: "fa fa-exclamation-triangle",
                iconColor: "text-red",
                setFeature: "EquipmentSubTypeDel"
              }
              //定義一個obj傳遞給$modal
            }
          });
          modalInstance.result.then(function(result) {
            // 傳給後端的資料
            let request = {
              ActionType: 3,
              EquipmentType: {
                EquipmentTypeId: $scope.equipment.EquipmentTypeId,
                EquipmentTypeName: $scope.equipment.EquipmentTypeName,
                EquipmentSubTypeId: $scope.equipment.EquipmentSubTypeId,
                EquipmentSubTypeName: $scope.equipment.EquipmentSubTypeName,
                EquipmentTypeClass: $scope.equipment.EquipmentTypeClass
              },
              TypeChange: {}
            };
            $http({
              // 停用小類API
              url: ApiMapper.sApi + "/s/equipmenttypes/action",
              method: "POST",
              contentType: "application/json",
              headers: {
                Passport: $scope.Passport
              },
              data: request
            }).success(function(data) {
              if (data.StatusCode == 1) {
                $scope.$toastlast = toastr["success"]("删除成功", "");
                $scope.GetEquipmentType();
              } else {
                $scope.$toastlast = toastr["error"](data.Messages, "");
              }
            }).error(function(data) {
              $scope.$toastlast = toastr["error"]("删除失败", "");
            });
          });
        }
      });
    };
    // 刪除中類
    $scope.EquipmentTypeDel = function(item) {
      let checkPermission = $scope.checkPermission();
      checkPermission.then(function(data) {
        if ((data = "success")) {
          //沒人使用才能執行以下功能
          var modalInstance = $modal.open({
            templateUrl: "classCtr", //模板窗口的地址
            size: "sm", //大小配置不能自定义大小，只能是sm，md，lg等这些值
            controller: "classCtr", //$modal指定的控制器 寫在下面
            resolve: {
              options: {
                item: item,
                EquipmentTypeList: $scope.EquipmentTypeList,
                equipment: $scope.equipment,
                title: "中类删除",
                icon: "fa fa-exclamation-triangle",
                iconColor: "text-red",
                setFeature: "EquipmentTypeDel"
              }
              //定義一個obj傳遞給$modal
            }
          });
          modalInstance.result.then(function(result) {
            // 傳給後端的資料
            let request = {
              ActionType: 2,
              EquipmentType: {
                EquipmentTypeId: $scope.equipment.EquipmentTypeId,
                EquipmentTypeName: $scope.equipment.EquipmentTypeName,
                EquipmentTypeClass: $scope.equipment.EquipmentTypeClass
              },
              TypeChange: {}
            };
            $http({
              //新增中類API
              url: ApiMapper.sApi + "/s/equipmenttypes/action",
              method: "POST",
              contentType: "application/json",
              headers: {
                Passport: $scope.Passport
              },
              data: request
            }).success(function(data) {
              if (data.StatusCode == 1) {
                $scope.equipment.EquipmentTypeId = "";
                $scope.$toastlast = toastr["success"]("删除成功", "");
                $scope.GetEquipmentType();
              } else {
                $scope.$toastlast = toastr["error"](data.Messages, "");
              }
            }).error(function(data) {
              $scope.$toastlast = toastr["error"]("删除失败", "");
            });
          });
        }
      });
    };
    // 轉移小類
    $scope.EquipmentSubTypeMove = function(item) {
      let checkPermission = $scope.checkPermission();
      checkPermission.then(function(data) {
        if ((data = "success")) {
          //沒人使用才能執行以下功能
          $scope.equipment.EquipmentSubTypeStatus = item.Status;
          $scope.equipment.EquipmentTypeClass = item.EquipmentTypeClass;
          $scope.equipment.EquipmentTypeId = item.EquipmentTypeId;
          $scope.equipment.EquipmentTypeName = item.EquipmentTypeName;
          $scope.equipment.EquipmentSubTypeId = item.EquipmentSubTypeId;
          $scope.equipment.EquipmentSubTypeName = item.EquipmentSubTypeName;
          var modalInstance = $modal.open({
            templateUrl: "classCtr", //模板窗口的地址
            size: "sm", //大小配置不能自定义大小，只能是sm，md，lg等这些值
            controller: "classCtr", //$modal指定的控制器 寫在下面
            resolve: {
              options: {
                item: item,
                EquipmentTypeList: $scope.EquipmentTypeList,
                equipment: $scope.equipment,
                title: "小类转换",
                icon: "fa fa-retweet",
                iconColor: "text-green",
                setFeature: "EquipmentSubTypeMove"
              }
              //定義一個obj傳遞給$modal
            }
          });
          modalInstance.result.then(function(result) {
            //傳給後端的資料
            let request = {
              ActionType: 11,
              EquipmentType: {},
              TypeChange: {
                TypeChange: {
                  OldEquipmentTypeClass: $scope.equipment.EquipmentTypeClass,
                  OldEquipmentTypeId: $scope.equipment.EquipmentTypeId,
                  OldEquipmentTypeName: $scope.equipment.EquipmentTypeName,
                  OldEquipmentSubTypeId: $scope.equipment.EquipmentSubTypeId,
                  OldEquipmentSubTypeName: $scope.equipment.EquipmentSubTypeName,
                  EquipmentTypeId: result.select.EquipmentTypeId,
                  EquipmentTypeName: result.select.EquipmentTypeName
                },
                Status: $scope.equipment.EquipmentSubTypeStatus
              }
            };
            $http({
              //新增中類API
              url: ApiMapper.sApi + "/s/equipmenttypes/action",
              method: "POST",
              contentType: "application/json",
              headers: {
                Passport: $scope.Passport
              },
              data: request
            }).success(function(data) {
              if (data.StatusCode == 1) {
                $scope.$toastlast = toastr["success"]("转换成功", "");
                $scope.GetEquipmentType();
              } else {
                $scope.$toastlast = toastr["error"](data.Messages, "");
              }
            }).error(function(data) {
              $scope.$toastlast = toastr["error"]("转换失败", "");
            });
          });
        }
      });
    };
    // 轉移中類
    $scope.EquipmentTypeMove = function(item) {
      let checkPermission = $scope.checkPermission();
      checkPermission.then(function(data) {
        if ((data = "success")) {
          //沒人使用才能執行以下功能
          var modalInstance = $modal.open({
            templateUrl: "classCtr", //模板窗口的地址
            size: "sm", //大小配置不能自定义大小，只能是sm，md，lg等这些值
            controller: "classCtr", //$modal指定的控制器 寫在下面
            resolve: {
              options: {
                item: item,
                EquipmentTypeList: $scope.EquipmentTypeList,
                equipment: $scope.equipment,
                title: "中类转换",
                icon: "fa fa-retweet",
                iconColor: "text-green",
                setFeature: "EquipmentTypeMove"
              }
            }
          });
          modalInstance.result.then(function(result) {
            //傳給後端的資料
            let request = {
              ActionType: 10,
              EquipmentType: {},
              TypeChange: {
                TypeChange: {
                  OldEquipmentTypeClass: $scope.equipment.EquipmentTypeClass,
                  OldEquipmentTypeId: $scope.equipment.EquipmentTypeId,
                  OldEquipmentTypeName: $scope.equipment.EquipmentTypeName,
                  EquipmentTypeClass: result.select.EquipmentClass
                },
                Status: $scope.equipment.EquipmentTypeStatus
              }
            };
            $http({
              //新增中類API
              url: ApiMapper.sApi + "/s/equipmenttypes/action",
              method: "POST",
              contentType: "application/json",
              headers: {
                Passport: $scope.Passport
              },
              data: request
            }).success(function(data) {
              if (data.StatusCode == 1) {
                $scope.$toastlast = toastr["success"]("转换成功", "");
                $scope.equipment.EquipmentTypeId = "";
                $scope.GetEquipmentType();
              } else {
                $scope.$toastlast = toastr["error"](data.Messages, "");
              }
            }).error(function(data) {
              $scope.$toastlast = toastr["error"]("转换失败", "");
            });
          });
        }
      });
    };
    // 獲取權限
    $scope.openForLockModal = function(item) {
      var modalInstance = $modal.open({
        templateUrl: "openForLockModal", //模板窗口的地址
        size: "md", //大小配置不能自定义大小，只能是sm，md，lg等这些值
        controller: "openForLockModal", //$modal指定的控制器 寫在下面
        resolve: {
          options: {
            item: item,
            EquipmentTypeList: $scope.EquipmentTypeList,
            equipment: $scope.equipment
          }
          //定義一個obj傳遞給$modal
        },
        backdrop: false
      });
      modalInstance.result.then(function(result) {
        if (result == true) {
          $scope.KeepForLock();
        } else {
          $scope.CancelForLock();
        }
      });
    };
  }
]);
/***************************/
/****    [控制] Modal    ***/
/***************************/
// classCtr 此頁面功能按鈕控制
angular.module("n580440").controller("classCtr", ["$scope", "$http", "$modalInstance", "options",
  function($scope, $http, $modalInstance, options) {
    $scope.options = options; // 從頁面傳送到modal的資料
    $scope.newEquipmentTypeName = ""; // 新中類名稱
    $scope.newEquipmentSubTypeName = ""; // 新小類名稱
    $scope.newName = ""; // 編輯中小類名稱用
    $scope.select = ""; // 所選擇的中or大類 轉移modal 使用
    // 小類只能在中類之間轉換 所以必須定義在相同大類之下的中類
    $scope.filterEquipmentTypeClass = function(item) {
      return ($scope.options.equipment.EquipmentTypeClass == item.EquipmentClass && !item.hide);
    };
    $scope.ok = function() {
      let result = {
        newName: $scope.newName,
        newEquipmentSubTypeName: $scope.newEquipmentSubTypeName,
        newEquipmentTypeName: $scope.newEquipmentTypeName,
        select: $scope.select
      };
      $modalInstance.close(result); // 關閉modal後傳給前面的資料
    };
    $scope.cancel = function() {
      $modalInstance.dismiss("cancel");
    };
  }
]);
// 重新獲取權限
angular.module("n580440").controller("openForLockModal", ["$scope", "$http", "$modalInstance", "options", "$timeout",
  function($scope, $http, $modalInstance, options, $timeout) {
    $scope.options = options; // 從頁面傳送到modal的資料
    $scope.TimesForRemind = 60; // 設定倒數秒數
    let Func_Timeouter_Remind; // 定義提醒計時器
    // 編輯權限提醒計時器
    $scope.Repair_LockForRemind_Time = function() {
      if ($scope.TimesForRemind == 0) {
        $timeout.cancel(Func_Timeouter_Remind); //取消提醒计时器
        //解锁 编辑权
        $scope.cancel();
      }
      $scope.TimesForRemind = $scope.TimesForRemind - 1;
      Func_Timeouter_Remind = $timeout(function() {
        $scope.Repair_LockForRemind_Time();
      }, 1000);
    };
    $scope.Repair_LockForRemind_Time(); //启动提醒计时器
    $scope.ok = function() {
      $modalInstance.close(true); // 關閉modal後傳給前面的資料
    };
    $scope.cancel = function() {
      $modalInstance.close(false); // 關閉modal後傳給前面的資料
    };
  }
]);