angular.module("n580440").controller("infoBoard4DockerCtrl", ["$scope", "$http", "$timeout", "$modal", "ngProgress", "$filter", "$sce", "$state", "$stateParams",
  function($scope, $http, $timeout, $modal, ngProgress, $filter, $sce, $state, $stateParams) {
    $scope.SigninType = Cookies.get("SigninType");
    var apiType = $scope.SigninType == 3 ? 's' : 'p';
    $scope.InfoBoard = {
      "CompanyCount": 0,
      "UnitCount": 0,
      "UserCount": 0,
      "EquipCount": 0
    };
    $scope.getContractedCount = function() {
      $http({
        url: `${ApiMapper.ccApi}/g/scontracted/count`,
        method: "GET",
        contentType: "application/json",
        headers: {
          Passport: Cookies.get("Passport")
        }
      }).success(function(res) {
        $scope.InfoBoard.CompanyCount = res.Count;
      }).error(res => {
        toastr["error"](res.Messages, "");
      });
    }();
    $scope.getUnitCount = function() {
      $http({
        url: `${ApiMapper.ccApi}/g/management/sunit/count`,
        method: "GET",
        contentType: "application/json",
        headers: {
          Passport: Cookies.get("Passport")
        }
      }).success(function(res) {
        $scope.InfoBoard.UnitCount = res.Count;
      }).error(res => {
        toastr["error"](res.Messages, "");
      });
    }();
    $scope.getUserCount = function() {
      $http({
        url: `${ApiMapper.ccApi}/g/management/suser/count`,
        method: "GET",
        contentType: "application/json",
        headers: {
          Passport: Cookies.get("Passport")
        }
      }).success(function(res) {
        $scope.InfoBoard.UserCount = res.Count;
      }).error(res => {
        toastr["error"](res.Messages, "");
      });
    }();
    $scope.getEquipCount = function() {
      $http({
        url: `${ApiMapper.ccApi}/g/management/equip/count`,
        method: "GET",
        contentType: "application/json",
        headers: {
          Passport: Cookies.get("Passport")
        }
      }).success(function(res) {
        $scope.InfoBoard.EquipCount = res.Count;
      }).error(res => {
        toastr["error"](res.Messages, "");
      });
    }();
    $scope.gotoList = function(v) {
      var urlStr = ApiMapper.ccApi.replace("/55497", "");
      var toURL = [`${urlStr}/center/#/related`, `${urlStr}/center/#/organize/unit`, `${urlStr}/center/#/equipment/list`, `${urlStr}/center/#/organize/user`];
      window.open(toURL[v]);
    };

    //
    $scope.syncDATA = function(tag) {
      var httpParames = {
        "unit": {
          "url": `/Api/dev/history`,
          "method": "POST",
          "data": {
            eventID: ["e0c3a37e-fa3c-4119-94b8-db2c97e0920a:111287", "e0c3a37e-fa3c-4119-94b8-db2c97e0920a:956942"]
          }
        },
        "equipment": {
          "url": `http://10.0.10.105:8090/dev/history`,
          "method": "POST",
          "data": {
            eventID: ["e0c3a37e-fa3c-4119-94b8-db2c97e0920a:111287", "e0c3a37e-fa3c-4119-94b8-db2c97e0920a:956942"]
          }
        },
        "error": {
          "url": `/dockerApi/dev/history`,
          "method": "POST",
          "data": {
            eventID: ["e0c3a37e-fa3c-4119-94b8-db2c97e0920a:111287", "e0c3a37e-fa3c-4119-94b8-db2c97e0920a:956942"]
          }
        }
      };
      // $http({
      //   url: httpParames[tag].url,
      //   method: httpParames[tag].method,
      //   contentType: "application/json",
      //   headers: {
      //     Passport: Cookies.get("Passport")
      //   },
      //   redirect: 'follow',
      //   data: httpParames[tag].data
      // }).success(res => {}).error(res => {});

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    var raw = {
      "eventID": ["e0c3a37e-fa3c-4119-94b8-db2c97e0920a:111287", "e0c3a37e-fa3c-4119-94b8-db2c97e0920a:956942"]
    };
    var requestOptions = {
      method: httpParames[tag].method,
      headers: myHeaders,
      body: JSON.stringify(httpParames[tag].data),
      redirect: 'follow'
    };
    fetch(httpParames[tag].url, requestOptions)
    .then(result => console.log(result))
    .catch(error => console.log('error', error));
    };
  }
]);