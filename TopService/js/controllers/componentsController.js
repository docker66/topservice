angular.module("n580440").controller("comptsCtrl", ["$scope", "$http", "$modal", "$timeout", "ngProgress", "i18n",
  function($scope, $http, $modal, $timeout, ngProgress, i18n) {
    ngProgress.start();
    $scope.items = ""; //打开选择可检视部门层
    $scope.SerachParames = {
      "Parameter": "",
      "IsMachine": -1
    };
    $scope.PcompanyList = [];
    $scope.PaginationisShow = false;
    $scope.tabeObj = {
      maxSize: 10,
      itemsPerPage: 20,
      totalItems: 0,
      currentPage: 1
    };
    $scope.getUnits = function() {
      $http({
        url: ApiMapper.sApi + "/s/punit/maps",
        method: "GET",
        cache: false,
        contentType: "application/json",
        headers: {
          Passport: $scope.userInfo.Passport
        }
      }).success(function(Unitdata) {
        $scope.PcompanyList = Unitdata[0];
        if ($scope.PcompanyList.length > 0) {
          $scope.getCompts4Store($scope.PcompanyList[0].CompanyId);
        }
      });
    };
    //获取设备列表
    $scope.getCompts4Store = function(companyId) {
      ngProgress.start();
      $scope.dataList = [];
      $http({
        url: `${ApiMapper.sApi}/s/compts/${companyId}`,
        method: "GET",
        contentType: "application/json",
        headers: {
          Passport: $scope.userInfo.Passport
        }
      }).success(function(res) {
        $scope.dataList = res;
        ngProgress.complete(100);
      }).error(function() {
        ngProgress.complete(100);
      });
    };
    //需求端获取设备列表
  $scope.ShowSpare = function (eId,idx) {
    $scope.selectedIDX = idx;
    ngProgress.start();
    $scope.SpareList = [];
    $http({
      url: ApiMapper.sApi + '/s/compts/' + eId,
      method: 'GET',
      contentType: 'application/json',
      headers: {
        'Passport': $scope.userInfo.Passport
      }
    }).success(function (data) {
      $scope.dataList = data;
      ngProgress.complete();
    }).error(function () {
      ngProgress.complete();
    });
  };
    //服务端获取零配件
    $scope.getCompts4Provider = function() {
      ngProgress.start();
      $scope.dataList = [];
      $http({
        url: `${ApiMapper.pApi}/p/compts/${$scope.tabeObj.currentPage}`,
        method: "POST",
        contentType: "application/json",
        headers: {
          Passport: $scope.userInfo.Passport
        },
        data: JSON.stringify($scope.SerachParames)
      }).success(function(res) {
        $scope.dataList = res[1];
        $scope.tabeObj.totalItems = res[2];
        $scope.PaginationisShow = res[0] > 1 ? true : false;
        ngProgress.complete(100);
      }).error(function(err) {
        ngProgress.complete(100);
      });
    };
    $scope.openDetail = function(MainItem) {
      var params = {
        ComponentName: MainItem.ComponentName,
        ComponentId: MainItem.ComponentId
      };
      var modalInstance = $modal.open({
        templateUrl: 'views/modal/components/componentDetailModal.html',
        controller: 'componentsDetailCtrl',
        size: 'xlg',
        resolve: {
          params: params
        }
      });
      modalInstance.result.then(function(result) {
        $scope.Search($scope.currentPage);
      }, function(reason) {
        // console.log('Modal dismissed at: ' + new Date());
      });
    };
    //init
    if ($scope.userInfo.SigninType == 3) {
      $scope.getCompts4Provider($scope.userInfo.CompanyId);
    } else {
      $scope.getUnits();
    }
    //分页变量赋值
    $scope.setPage = function(pageNo) {
      $scope.tabeObj.currentPage = pageNo;
    };
    $scope.pageChanged = function() {
      ngProgress.start();
      $timeout(function() {
        $scope.getCompts4Provider($scope.userInfo.CompanyId);
      }, 200);
    };
  }
]);