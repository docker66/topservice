function RObj(ea) { var LS = ""; var QS = new Object(); var un = "undefined"; var x = null; var f = "function"; var n = "number"; var r = "string"; var e1 = "ERROR:Index out of range in\r\nRequest.QueryString"; var e2 = "ERROR:Wrong number of arguments or invalid property assignment\r\nRequest.QueryString"; var e3 = "ERROR:Object doesn't support this property or method\r\nRequest.QueryString.Key"; var dU = window.decodeURIComponent ? 1 : 0; function Err(arg) { if (ea) { alert("Request Object:\r\n" + arg) } }; function URID(t) { var d = ""; if (t) { for (var i = 0; i < t.length; ++i) { var c = t.charAt(i); d += (c == "+" ? " " : c) } } return (dU ? decodeURIComponent(d) : unescape(d)) }; function OL(o) { var l = 0; for (var i in o) { if (typeof o[i] != f) { l++ } }; return l }; function AK(key) { var auk = true; for (var u in QS) { if (typeof QS[u] != f && u.toString().toLowerCase() == key.toLowerCase()) { auk = false; return u } } if (auk) { QS[key] = new Object(); QS[key].toString = function () { return TS(QS[key]) }; QS[key].Count = function () { return OL(QS[key]) }; QS[key].Count.toString = function () { return OL(QS[key]).toString() }; QS[key].Item = function (e) { if (typeof e == un) { return QS[key] } else { if (typeof e == n) { var a = QS[key][Math.ceil(e)]; if (typeof a == un) { Err(e1 + "(\"" + key + "\").Item(" + e + ")") }; return a } else { Err("ERROR:Expecting numeric input in\r\nRequest.QueryString(\"" + key + "\").Item(\"" + e + "\")") } } }; QS[key].Item.toString = function (e) { if (typeof e == un) { return QS[key].toString() } else { var a = QS[key][e]; if (typeof a == un) { Err(e1 + "(\"" + key + "\").Item(" + e + ")") }; return a.toString() } }; QS[key].Key = function (e) { var t = typeof e; if (t == r) { var a = QS[key][e]; return (typeof a != un && a && a.toString() ? e : "") } else { Err(e3 + "(" + (e ? e : "") + ")") } }; QS[key].Key.toString = function () { return x } }; return key }; function AVTK(key, val) { if (key != "") { var key = AK(key); var l = OL(QS[key]); QS[key][l + 1] = val } }; function TS(o) { var s = ""; for (var i in o) { var ty = typeof o[i]; if (ty == "object") { s += TS(o[i]) } else if (ty != f) { s += o[i] + ", " } }; var l = s.length; if (l > 1) { return (s.substring(0, l - 2)) } return (s == "" ? x : s) }; function KM(k, o) { var k = k.toLowerCase(); for (var u in o) { if (typeof o[u] != f && u.toString().toLowerCase() == k) { return u } } } if (window.location && window.location.search) { LS = window.location.search; var l = LS.length; if (l > 0) { LS = LS.substring(1, l); var preAmpAt = 0; var ampAt = -1; var eqAt = -1; var k = 0; var skip = false; for (var i = 0; i < l; ++i) { var c = LS.charAt(i); if (LS.charAt(preAmpAt) == "=" || (preAmpAt == 0 && i == 0 && c == "=")) { skip = true } if (c == "=" && eqAt == -1 && !skip) { eqAt = i } if (c == "&" && ampAt == -1) { if (eqAt != -1) { ampAt = i } if (skip) { preAmpAt = i + 1 }; skip = false } if (ampAt > eqAt) { AVTK(URID(LS.substring(preAmpAt, eqAt)), URID(LS.substring(eqAt + 1, ampAt))); preAmpAt = ampAt + 1; eqAt = ampAt = -1; ++k } } if (LS.charAt(preAmpAt) != "=" && (preAmpAt != 0 || i != 0 || c != "=")) { if (preAmpAt != l) { if (eqAt != -1) { AVTK(URID(LS.substring(preAmpAt, eqAt)), URID(LS.substring(eqAt + 1, l))) } else if (preAmpAt != l - 1) { AVTK(URID(LS.substring(preAmpAt, l)), "") } } if (l == 1) { AVTK(LS.substring(0, 1), "") } } } }; var TC = OL(QS); if (!TC) { TC = 0 }; QS.toString = function () { return LS.toString() }; QS.Count = function () { return (TC ? TC : 0) }; QS.Count.toString = function () { return (TC ? TC.toString() : "0") }; QS.Item = function (e) { if (typeof e == un) { return LS } else { if (typeof e == n) { var e = Math.ceil(e); var c = 0; for (var i in QS) { if (typeof QS[i] != f && ++c == e) { return QS[i] } }; Err(e1 + "().Item(" + e + ")") } else { return QS[KM(e, QS)] } }; return x }; QS.Item.toString = function () { return LS.toString() }; QS.Key = function (e) { var t = typeof e; if (t == n) { var e = Math.ceil(e); var c = 0; for (var i in QS) { if (typeof QS[i] != f && ++c == e) { return i } } } else if (t == r) { var e = KM(e, QS); var a = QS[e]; return (typeof a != un && a && a.toString() ? e : "") } else { Err(e2 + "().Key(" + (e ? e : "") + ")") }; Err(e1 + "().Item(" + e + ")") }; QS.Key.toString = function () { Err(e2 + "().Key") }; this.QueryString = function (k) { if (typeof k == un) { return QS } else { if (typeof k == n) { return QS.Item(k) }; var k = KM(k, QS); if (typeof QS[k] == un) { t = new Object(); t.Count = function () { return 0 }; t.Count.toString = function () { return "0" }; t.toString = function () { return x }; t.Item = function (e) { return x }; t.Item.toString = function () { return x }; t.Key = function (e) { Err(e3 + "(" + (e ? e : "") + ")") }; t.Key.toString = function () { return x }; return t } else { return QS[k] } } }; this.QueryString.toString = function () { return LS.toString() }; this.QueryString.Count = function () { return (TC ? TC : 0) }; this.QueryString.Count.toString = function () { return (TC ? TC.toString() : "0") }; this.QueryString.Item = function (e) { if (typeof e == un) { return LS.toString() } else { if (typeof e == n) { var e = Math.ceil(e); var c = 0; for (var i in QS) { if (typeof QS[i] != f && ++c == e) { return QS[i] } }; Err(e1 + ".Item(" + e + ")") } else { return QS[KM(e, QS)] } } if (typeof e == n) { Err(e1 + ".Item(" + e + ")") }; return x }; this.QueryString.Item.toString = function () { return LS.toString() }; this.QueryString.Key = function (e) { var t = typeof e; if (t == n) { var e = Math.ceil(e); var c = 0; for (var i in QS) { if (typeof QS[i] == "object" && (++c == e)) { return i } } } else if (t == r) { var e = KM(e, QS); var a = QS[e]; return (typeof a != un && a && a.toString() ? e : "") } else { Err(e2 + ".Key(" + (e ? e : "") + ")") }; Err(e1 + ".Item(" + e + ")") }; this.QueryString.Key.toString = function () { Err(e2 + ".Key") }; this.Version = 1.4; this.Author = "Andrew Urquhart (http://andrewu.co.uk)" };var Request = new RObj(false);

Base64 = { _0: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (b) { var a = ""; var d, c, h, j, i, f, g; var e = 0; b = Base64._1(b); while (e < b.length) { d = b.charCodeAt(e++); c = b.charCodeAt(e++); h = b.charCodeAt(e++); j = d >> 2; i = ((d & 3) << 4) | (c >> 4); f = ((c & 15) << 2) | (h >> 6); g = h & 63; if (isNaN(c)) { f = g = 64 } else if (isNaN(h)) { g = 64 } a = a + this._0.charAt(j) + this._0.charAt(i) + this._0.charAt(f) + this._0.charAt(g) } return a }, decode: function (b) { var a = ""; var d, c, h; var j, i, f, g; var e = 0; b = b.replace(/[^A-Za-z0-9\+\/\=]/g, ""); while (e < b.length) { j = this._0.indexOf(b.charAt(e++)); i = this._0.indexOf(b.charAt(e++)); f = this._0.indexOf(b.charAt(e++)); g = this._0.indexOf(b.charAt(e++)); d = (j << 2) | (i >> 4); c = ((i & 15) << 4) | (f >> 2); h = ((f & 3) << 6) | g; a = a + String.fromCharCode(d); if (f != 64) { a = a + String.fromCharCode(c) } if (g != 64) { a = a + String.fromCharCode(h) } } a = Base64._2(a); return a }, _1: function (b) { b = b.replace(/\r\n/g, "\n"); var a = ""; for (var d = 0; d < b.length; d++) { var c = b.charCodeAt(d); if (c < 128) { a += String.fromCharCode(c) } else if ((c > 127) && (c < 2048)) { a += String.fromCharCode((c >> 6) | 192); a += String.fromCharCode((c & 63) | 128) } else { a += String.fromCharCode((c >> 12) | 224); a += String.fromCharCode(((c >> 6) & 63) | 128); a += String.fromCharCode((c & 63) | 128) } } return a }, _2: function (b) { var a = ""; var d = 0; var c = c1 = c2 = 0; while (d < b.length) { c = b.charCodeAt(d); if (c < 128) { a += String.fromCharCode(c); d++ } else if ((c > 191) && (c < 224)) { c2 = b.charCodeAt(d + 1); a += String.fromCharCode(((c & 31) << 6) | (c2 & 63)); d += 2 } else { c2 = b.charCodeAt(d + 1); c3 = b.charCodeAt(d + 2); a += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63)); d += 3 } } return a } };

jQuery(document).ready(function() {    
    var $toastlast;
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-center",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    
    //延时12小时检查cookie是否存在
    setTimeout(function(){
        var sPassport = Cookies.get("Passport");
        if(sPassport === null || sPassport === undefined || sPassport ===""){
          location.href = '../../login_cc.html';
        }
    }, 12 * 60 * 60 * 1000);
});
//时间格式化
var FormatDATE = function (date) {
    var myDate = new Date(date);
    var _YY = myDate.getFullYear();
    var _MM = myDate.getMonth() + 1;
    var _DD = myDate.getDate();
    
    var _now = '' + _YY + '/' + _MM + '/' + _DD;
    return _now;
};

var FormatTIME = function (date) {
    var myDate = new Date(date);
    var _hh = '00' + myDate.getHours();
    var _mm = '00' + myDate.getMinutes();
    var _ss = '00' + myDate.getSeconds();
    var _time = '' + _hh.substr(_hh.length-2,2) + ':' + _mm.substr(_mm.length-2,2);
    return _time;
};

/**
 * 对Date的扩展，将 Date 转化为指定格式的String
 * 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
 * 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
 * 例子： 
 * (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
 * (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
 */
Date.prototype.Format = function (fmt = "yyyy/MM/dd hh:mm:ss") { 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
//获取当前时间
var GetNow = function () {
    var myDate = new Date();
    var _now = myDate.Format("yyyy/MM/dd");
    return _now;
};
//时间比较
var ValidtorTime = function (dt1, dt2) {
    var d1 = new Date(dt1).getTime();
    var d2 = new Date(dt2).getTime();
    if (d1 > d2) {
        return false;
    }
    return true;
};

var thousandBitSeparator = function (num) {
    num = parseFloat(num).toFixed(2); //天花板函数保留小数并四舍五入
　　var source = String(num).split(".");//按小数点分成2部分
　　source[0] = source[0].replace(new RegExp('(\\d)(?=(\\d{3})+$)', 'ig'), "$1,");//只将整数部分进行都好分割
　　return source.join(".");//再将小数部分合并进来 
}