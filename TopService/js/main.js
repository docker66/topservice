/* Metronic App */
var n580440 = angular.module("n580440", [
  "ui.router",
  "ui.bootstrap",
  "oc.lazyLoad",
  "ngSanitize",
  "ngProgress",
  "angularFileUpload",
  "ngMessages",
  "pascalprecht.translate"
]);
/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
n580440.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
  $ocLazyLoadProvider.config({
    // global configs go here
  });
}]);
/********************************************
 BEGIN: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/
//AngularJS v1.3.x workaround for old style controller declarition in HTML
n580440.config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);
n580440.config(['$translateProvider', function($translateProvider) {
  //var lang = window.localStorage.lang || 'zh-cn';
  var lang = navigator.language.toLowerCase() || 'zh-cn';
  // var lang ='zh-tw';console.log(lang)
  $translateProvider.preferredLanguage(lang);
  $translateProvider.useStaticFilesLoader({
    prefix: 'i10n/',
    suffix: '.json'
  });
}]);
/* Setup global settings */
n580440.factory('settings', ['$rootScope', function($rootScope) {
  // supported languages
  var settings = {
    layout: {
      pageSidebarClosed: false, // sidebar menu state
      pageContentWhite: true, // set page content layout
      pageBodySolid: false, // solid body color state
      pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
    },
    assetsPath: 'assets',
    globalPath: 'assets/global',
    layoutPath: 'assets/layouts/layout',
  };
  $rootScope.settings = settings;
  return settings;
}]);
/* Setup App Main Controller */
n580440.controller('AppController', ['$scope', '$rootScope', '$state', '$stateParams', 'i18n', function($scope, $rootScope, $state, $stateParams, i18n) {
  $scope.PageNoSidebar = [
  "modifyStoreUnit",
  "addStoreUnit", 
  "organizeUnitAdd",
  "organizeUnitModify",
  "modifyMaintainerUnit", 
  "addMaintainerUnit",
  "equipmentAdd",
  "equipmentModify",
  "relatedAdd",
  "infoBoard"
  ];
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    if ($scope.PageNoSidebar.indexOf($state.current.name) > -1) {
      $scope.hasSidebar = false;
    } else {
      $scope.hasSidebar = true;
    }
  });
  //接收父控制器广播，刷新列表
    $scope.$on("reloadList", function (event) {    
      console.log(1);  
      $scope.$broadcast('reloadList');
    });
  //$scope.$on('$viewContentLoaded', function() {
  $scope.userInfo = {
    "Passport": Cookies.get('Passport'),
    "uID": Cookies.get('uID'),
    "UnitId": Cookies.get('UnitId'),
    "Unit": Cookies.get('Unit'),
    "FullName": Cookies.get('FullName'),
    "CompanyId": Cookies.get('CompanyId'),
    "Dispatchable": Cookies.get('Dispatchable'),
    "SigninType": Cookies.get('SigninType'),
    "apiType": Cookies.get('SigninType') == 2 ? 's' : 'p'
  };
  if ($scope.userInfo.Passport == null || $scope.userInfo.Passport == "") {
    //location.href = '../../index.html';
  }
  $scope.Info = {
    'CompanyId': $scope.userInfo.CompanyId,
    'EquipmentTypeClass': null
  };
  $scope.$on("RepairListToChange", function(event) {
    $scope.$broadcast("RepairListChangeFromParrent");
  });
  $scope.$on("reloadDetail", function(event) {
    $scope.$broadcast("reloadDetailFromParrent");
  });
  $scope.$on("RepairDetailToChange", function(event, data) {
    $scope.$broadcast("RepairDetailToChangeFromParrent", data);
  });
  //});
}]);
/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial 
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/
/* Setup Layout Part - Header */
n580440.controller('HeaderController', ['$scope', '$http', '$modal', '$timeout', 'ngProgress', function($scope, $http, $modal, $timeout, ngProgress) {
  $scope.$on('$includeContentLoaded', function() {
    Layout.initHeader(); // init header
    $scope.UserAvator = Cookies.get("Avator") == '' ? './images/a1.png' : Cookies.get("Avator");
    $scope.Change4Password = Cookies.get("Change4Password");
    $scope.Unit = Cookies.get("Unit");
    $scope.StoreUser = Cookies.get("StoreUser");
    $scope.ExpiredTime = Cookies.get("ExpiredTime");
    //确认门店密码
    if (ValidtorTime($scope.ExpiredTime, GetNow()) && parseInt($scope.StoreUser) == 1) {
      var modalInstance = $modal.open({
        templateUrl: 'ConfirmCode.html',
        size: '',
        backdrop: 'static',
        controller: 'ConfirmCodeCtl'
      });
      modalInstance.result.then(function(AcceptanceParameters) {});
    }
    if (parseInt($scope.StoreUser) == 1) {
      $('[name=ChangeUnit]').show();
    } else {
      $('[name=ChangeUnit]').hide();
    }
    //修改密码
    // $scope.ChangePwd = function () {
    //     var modalInstance = $modal.open({
    //         templateUrl: 'ModalForChangePwd.html',
    //         size: '',
    //         backdrop: 'static',
    //         controller: 'ChangePwdCtl'
    //     });
    //     modalInstance.result.then(function (AcceptanceParameters) {
    //     });
    // }
    // if ($scope.Change4Password == "true") {
    //     $scope.ChangePwd(); 
    // }
    //调店或支援
    $scope.ChangeUnit = function() {
      var modalInstance = $modal.open({
        templateUrl: 'ChangeUnit.html',
        size: '',
        backdrop: 'static',
        controller: 'ChangeUnitCtl'
      });
      modalInstance.result.then(function(AcceptanceParameters) {
        //$scope.getMemberInfo();
      });
    }
    //登出系统
    $scope.logout = function() {
      Cookies.set("uID", null, { path: '/' });
      Cookies.set("FullName", null, { path: '/' });
      Cookies.set("UnitId", null, { path: '/' });
      Cookies.set("Unit", null, { path: '/' });
      Cookies.set("Avator", null, { path: '/' });
      Cookies.set("Passport", null, { path: '/' });
      Cookies.set("Change4Password", null, { path: '/' });
      Cookies.set("StoreUser", null, { path: '/' });
      Cookies.set("ExpiredTime", null, { path: '/' });
      location.href = '../../index.html';
    };
  });
}]);
/* Setup Layout Part - Sidebar */
n580440.controller('SidebarController', ['$scope', '$http', '$timeout', function($scope, $http, $timeout) {
  // $scope.SidebarTpl = 'tpl/sidebar.html';
  if (location.href.indexOf('#/data-maintenance/eq/') !== -1) {
    $scope.SidebarTpl = 'views/tpl/eq-sidebar.html';
  } else if (location.href.indexOf('#/data-maintenance/rd/') !== -1) {
    $scope.SidebarTpl = 'views/tpl/rd-sidebar.html';
  } else if (location.href.indexOf('#/data-maintenance/rs/') !== -1) {
    $scope.SidebarTpl = 'views/tpl/rs-sidebar.html';
  } else {
    $scope.SidebarTpl = 'views/tpl/sidebar.html';
  }
  $scope.$on('$includeContentLoaded', function() {
    Layout.initSidebar(); // init sidebar
  });
}]);
/* Setup Layout Part - Quick Sidebar */
n580440.controller('QuickSidebarController', ['$scope', function($scope) {
  $scope.$on('$includeContentLoaded', function() {
    setTimeout(function() {
      QuickSidebar.init(); // init quick sidebar        
    }, 2000)
  });
}]);
/* Setup Layout Part - Footer */
n580440.controller('FooterController', ['$scope', function($scope) {
  $scope.$on('$includeContentLoaded', function() {
    Layout.initFooter(); // init footer
  });
}]);
/* Setup Rounting For All Pages */
n580440.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  // Redirect any unmatched url
  $urlRouterProvider.otherwise("/organize/unit");
  $stateProvider
    /*数据维护*/
    //维护门店及人员资料
    .state("organizeUnit", {
      url: "/organize/unit",
      templateUrl: "views/organize-unit-list-page.html",
      data: {
        pageTitle: "单位列表"
      },
      controller: "organizeUnitListCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
              "plugins/select2/css/select2.css", 
              "plugins/select2/css/select2-bootstrap.css", 
              "plugins/select2/js/select2.min.js", 
              "js/services/dataMaintenanceFactory.js", 
              "js/controllers/organize/organizeUnitListController.js", 
              "js/controllers/domain/importCsvController.js"]
            });
          }
        ]
      }
    })
    //3.1 新增-報修單位
    .state("addStoreUnit", {
      url: "/organize/sunit/add",
      templateUrl: "views/pages/organize/rd/add-store-unit-page.html",
      data: {
        pageTitle: '新增门店信息'
      },
      controller: "addStoreUnitController",
      resolve: {
        deps: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'n580440',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: ['plugins/select2/css/select2.css', 'plugins/select2/css/select2-bootstrap.css', 'plugins/select2/js/select2.min.js', 'plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css', 'plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js', 'js/controllers/domain/ntUsersPicker.js', 'js/services/dataMaintenanceFactory.js', 'js/controllers/organize/rd/addStoreUnitController.js']
          });
        }]
      }
    })
    //3.2 編輯-報修單位
    .state("modifyStoreUnit", {
      url: "/organize/sunit/edit/:unitId",
      templateUrl: "views/pages/organize/rd/edit-store-unit-page.html",
      data: {
        pageTitle: '编辑门店信息'
      },
      controller: "editStoreUnitController",
      resolve: {
        deps: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'n580440',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: ['plugins/select2/css/select2.css', 'plugins/select2/css/select2-bootstrap.css', 'plugins/select2/js/select2.min.js', 'plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css', 'plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js', 'js/controllers/domain/ntUsersPicker.js', 'js/services/dataMaintenanceFactory.js', 'js/controllers/organize/rd/editStoreUnitController.js']
          });
        }]
      }
    })
    //5.2 新增-維修單位
    .state("addMaintainerUnit", {
      url: "/organize/punit/add",
      templateUrl: "views/pages/organize/rs/add-maintainer-unit-page.html",
      data: {
        pageTitle: '新增据点信息'
      },
      controller: "addMaintainerUnitController",
      resolve: {
        deps: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'n580440',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [
            'plugins/select2/css/select2.css', 
            'plugins/select2/css/select2-bootstrap.css', 
            'plugins/select2/js/select2.min.js', 
            'js/services/dataMaintenanceFactory.js', 
            'js/controllers/organize/rs/addMaintainerUnitController.js'
            ]
          });
        }]
      }
    })
    //5.3 編輯-維修單位
    .state("modifyMaintainerUnit", {
      url: "/organize/punit/edit/:unitId",
      templateUrl: "views/pages/organize/rs/edit-maintainer-unit-page.html",
      data: {
        pageTitle: '编辑据点信息'
      },
      controller: "editMaintainerUnitController",
      resolve: {
        deps: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            name: 'n580440',
            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
            files: [
            'plugins/select2/css/select2.css', 
            'plugins/select2/css/select2-bootstrap.css', 
            'plugins/select2/js/select2.min.js', 
            'js/controllers/domain/ntUsersPicker.js', 
            'js/services/dataMaintenanceFactory.js', 
            'js/controllers/organize/rs/editMaintainerUnitController.js']
          });
        }]
      }
    }).state("organizeUnitAdd", {
      url: "/organize/unit/add",
      templateUrl: "views/pages/organize/organize-unit-add.html",
      data: {
        pageTitle: "新增单位"
      },
      controller: "organizeUnitAddCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: ["plugins/select2/css/select2.css", "plugins/select2/css/select2-bootstrap.css", "plugins/select2/js/select2.min.js", "plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css", "plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js", "js/services/dataMaintenanceFactory.js", "js/controllers/organize/organizeUnitController.js"]
            });
          }
        ]
      }
    }).state("organizeUnitModify", {
      url: "/organize/unit/modify/{unitId}",
      templateUrl: "views/pages/organize/organize-unit-modify.html",
      data: {
        pageTitle: "单位信息维护"
      },
      controller: "organizeUnitModifyCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: ["plugins/select2/css/select2.css", "plugins/select2/css/select2-bootstrap.css", "plugins/select2/js/select2.min.js", "plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css", "plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js", "js/services/dataMaintenanceFactory.js", "js/controllers/organize/organizeUnitController.js"]
            });
          }
        ]
      }
    }).state("organizeUser", {
      url: "/organize/user",
      templateUrl: "views/organize-user-list-page.html",
      data: {
        pageTitle: "人员列表"
      },
      controller: "organizeUserListCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
              "plugins/select2/css/select2.css", 
              "plugins/select2/css/select2-bootstrap.css", 
              "plugins/select2/js/select2.min.js", 
              "js/services/dataMaintenanceFactory.js", 
              "js/controllers/organize/organizeUserController.js", 
              "js/controllers/domain/importCsvController.js"
              ]
            });
          }
        ]
      }
    }).state("organizeUserAdd", {
      url: "/organize/user/add",
      templateUrl: "views/pages/organize/organize-user-modify.html",
      data: {
        pageTitle: "新增人员"
      },
      controller: "organizeUserAddCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: ["plugins/select2/css/select2.css", "plugins/select2/css/select2-bootstrap.css", "plugins/select2/js/select2.min.js", "plugins/bootstrap-table/bootstrap-table.min.css", "plugins/bootstrap-table/bootstrap-table.min.js", "js/controllers/domain/importCsvController.js", "js/services/dataMaintenanceFactory.js", "js/controllers/organize/organizeUserController.js"]
            });
          }
        ]
      }
    }).state("organizeUserModify", {
      url: "/organize/user/modify/{userId}",
      templateUrl: "views/pages/organize/organize-user-modify.html",
      data: {
        pageTitle: "人员信息维护"
      },
      controller: "organizeUserModifyCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: ["plugins/select2/css/select2.css", "plugins/select2/css/select2-bootstrap.css", "plugins/select2/js/select2.min.js", "js/services/dataMaintenanceFactory.js", "js/controllers/organize/organizeUserController.js"]
            });
          }
        ]
      }
    })
    //关联公司列表
    .state("relatedList", {
      url: "/related",
      templateUrl: "views/related-list-page.html",
      data: {
        pageTitle: "关联企业列表"
      },
      controller: "relatedListCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                "plugins/select2/css/select2.css",
                "plugins/select2/css/select2-bootstrap.css",
                "plugins/select2/js/select2.min.js",
                "js/controllers/relatedListController.js"
                ]
            });
          }
        ]
      }
    })
    .state("relatedEquipmentList", {
      url: "/related/:companyId/:companyName",
      templateUrl: "views/pages/related/related-equipment-list-page.html",
      data: {
        pageTitle: "负责设备列表"
      },
      controller: "relatedEquipmentListCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                "plugins/select2/css/select2.css",
                "plugins/select2/css/select2-bootstrap.css",
                "plugins/select2/js/select2.min.js",
                "js/controllers/relatedListController.js"
                ]
            });
          }
        ]
      }
    })
    //新增关联企业(研华docker)
    .state("relatedAdd", {
      url: "/related/add",
      templateUrl: "views/pages/companys/company-add-docker.html",
      data: {
        pageTitle: "关联企业"
      },
      controller: "relatedCompanyAdd4DockerCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                "plugins/select2/css/select2.css",
                "plugins/select2/css/select2-bootstrap.css",
                "plugins/select2/js/select2.min.js",
                "js/controllers/relatedListController.js"
                ]
            });
          }
        ]
      }
    })
    //新增关联企业(研华docker)
    .state("infoBoard", {
      url: "/infoBoard",
      templateUrl: "views/board-docker.html",
      data: {
        pageTitle: "维保报修信息"
      },
      controller: "infoBoard4DockerCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                "plugins/select2/css/select2.css",
                "plugins/select2/css/select2-bootstrap.css",
                "plugins/select2/js/select2.min.js",
                "js/controllers/infoBoard4DockerController.js"
                ]
            });
          }
        ]
      }
    })
    //setting equipments
    .state("settingEquipments", {
      url: "/setting-equipments",
      templateUrl: "views/setting-equipments.html",
      data: {
        pageTitle: "数据维护/营业设备"
      },
      controller: "settingEquipmentCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: ["plugins/select2/css/select2.css", "plugins/select2/css/select2-bootstrap.css", "plugins/select2/js/select2.min.js", "plugins/select2/css/select2.css", "plugins/select2/css/select2-bootstrap.css", "plugins/select2/js/select2.min.js", "js/controllers/equipmentController.js"]
            });
          }
        ]
      }
    })
    //1.1 設備列表
    .state("equipmentList", {
      url: "/equipment/list",
      templateUrl: "views/equipment-list-page.html",
      data: {
        pageTitle: "设备列表"
      },
      controller: "equipmentListController",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                "plugins/select2/css/select2.css",
                "plugins/select2/css/select2-bootstrap.css",
                "plugins/select2/js/select2.min.js",
                "plugins/bootstrap-table/bootstrap-table.min.css",
                "plugins/bootstrap-table/bootstrap-table.min.js",
                "js/controllers/domain/importCsvController.js",
                "js/controllers/domain/storeModalController.js",
                "js/services/dataMaintenanceFactory.js",
                "js/controllers/equipmentListController.js"
              ]
            });
          }
        ]
      }
    })
    //1.2 新增設備
    .state("equipmentAdd", {
      url: "/equipment/add",
      templateUrl: "views/pages/equipment/equipement-add-page.html",
      data: {
        pageTitle: "新增设备讯息"
      },
      controller: "equipmentAddCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: ["plugins/select2/css/select2.css", "plugins/select2/css/select2-bootstrap.css", "plugins/select2/js/select2.min.js", "js/services/dataMaintenanceFactory.js", "js/controllers/equipment/equipmentAddController.js"]
            });
          }
        ]
      }
    })
    //1.3 編輯-設備 (需按编辑按钮)
    .state("equipmentModify", {
      url: "/equipment/detail/:equipmentId",
      templateUrl: "views/pages/equipment/equipement-modify-page.html",
      data: {
        pageTitle: "设备详情"
      },
      controller: "equipmentModifyController",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                "plugins/select2/css/select2.css",
                "plugins/select2/css/select2-bootstrap.css",
                "plugins/select2/js/select2.min.js",
                "js/services/dataMaintenanceFactory.js",
                "js/controllers/equipment/equipmentModifyController.js"
              ]
            });
          }
        ]
      }
    })
    //設定中小類維護
    .state("classModify", {
      url: "/sub-class/modify",
      templateUrl: "views/sub-class-modify-page.html",
      data: {
        pageTitle: "中小类维护"
      },
      controller: "subclassModifyCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: ["plugins/select2/css/select2.css", "plugins/select2/css/select2-bootstrap.css", "plugins/select2/js/select2.min.js", "js/controllers/subclassModifyController.js"]
            });
          }
        ]
      }
    })
    //品项维护
    .state("itemModify", {
      url: "/item/modify",
      templateUrl: "views/item-modify-page.html",
      data: {
        pageTitle: "品项维护"
      },
      controller: "itemModifyCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: ["plugins/select2/css/select2.css", "plugins/select2/css/select2-bootstrap.css", "plugins/select2/js/select2.min.js", "js/controllers/itemModifyController.js"]
            });
          }
        ]
      }
    })
    // 零配件维护
    .state("componentsList", {
      url: "/components",
      templateUrl: "views/components-list-page.html",
      data: {
        pageTitle: "零配件维护"
      },
      controller: "comptsCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
              "js/controllers/componentsController.js"
              ,"js/controllers/components/componentsDetailController.js"]
            });
          }
        ]
      }
    })
    //2.1  原因列表
    .state("causeList", {
      url: "/cause/list",
      templateUrl: "views/cause-list-page.html",
      data: {
        pageTitle: "原因及时限管理列表"
      },
      controller: "causeListCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: ["plugins/select2/css/select2.css", "plugins/select2/css/select2-bootstrap.css", "plugins/select2/js/select2.min.js", "plugins/bootstrap-table/bootstrap-table.min.css", "plugins/bootstrap-table/bootstrap-table.min.js", "js/controllers/domain/importCsvController.js", "js/services/dataMaintenanceFactory.js", "js/controllers/causeListController.js"]
            });
          }
        ]
      }
    })
    //2.2 新增-原因
    .state("causeAdd", {
      url: "/cause/add",
      templateUrl: "views/pages/equipment/cause-add-page.html",
      data: {
        pageTitle: "新增原因及时限"
      },
      controller: "causeAddCtrl",
      resolve: {
        deps: ["$ocLazyLoad",
          function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: "n580440",
              insertBefore: "#ng_load_plugins_before", // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: ["plugins/select2/css/select2.css", "plugins/select2/css/select2-bootstrap.css", "plugins/select2/js/select2.min.js", "plugins/bootstrap-table/bootstrap-table.min.css", "plugins/bootstrap-table/bootstrap-table.min.js", "js/services/dataMaintenanceFactory.js", "js/controllers/equipment/causeAddController.js"]
            });
          }
        ]
      }
    });
}]);
/* Init global settings and run the app */
n580440.run(["$rootScope", "settings", "$state", function($rootScope, settings, $state) {
  $rootScope.$state = $state; // state to be accessed from view
  $rootScope.$settings = settings; // state to be accessed from view
}]);
/* ng-repeat 过滤  | unique: 'id' IE8无效 */
angular.module('n580440').filter('unique', function() {
  return function(collection, keyname) {
    var output = [],
      keys = [];
    angular.forEach(collection, function(item) {
      var key = item[keyname];
      if (keys.indexOf(key) === -1) {
        keys.push(key);
        output.push(item);
      }
    });
    return output;
  };
});