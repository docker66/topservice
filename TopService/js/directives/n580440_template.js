n580440
  // 人员新增/维护
  .directive("organizeUserModal", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/modals/organize/organize-user-modal.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  })
  // 关联企业列表table
  .directive("relatedCompanyTableListStore", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/pages/related/related-company-table-list-store.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  })
  .directive("relatedCompanyTableListProvider", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/pages/related/related-company-table-list-provider.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  })
  // 关联企业负责设备列表
  .directive("relatedCompanyModify", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/modals/related/related-company-modify.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  }).directive("equipmentSearchTop", function($templateRequest, $compile) {
    return {
      scope: true,
      link: function($scope, element, attrs) {
        $templateRequest("./views/pages/equipment/equipment-search-top.html").then(function(html) {
          element.append($compile(html)($scope));
        });
      }
    };
  }).directive("equipmentListTable", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/pages/equipment/equipment-list-table.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  })
  //设备报修、调拨历程
  .directive('equipmentRepairListTable', function() {
    return {
      restrict: 'EA',
      scope: true,
      templateUrl: './views/pages/equipment/equipment-repair-list-table.html',
      link: function($scope, element, attrs) {} //DOM manipulation
    }
  })
  // 中小类维护相关
  .directive("subClassModal", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/modals/sub-class/sub-class-modal.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  }).directive("openForLockModal", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/modals/sub-class/open-for-lock-modal.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  })
  // 批量新增设备设施
  .directive("batchAddEquipment", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/modals/equipment/batch-add-equipment.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  })
  // 零配件相关page
  .directive("storeComponentsListTable", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/pages/components/store-components-list-table.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  })
  .directive("providerComponentsListTable", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/pages/components/provider-components-list-table.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  })
  // 汇入excel相关
  .directive("importCsv", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/modals/equipment/import-csv.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  })
  //数据维护
  .directive("modifyUnit", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/modals/common/modify-unit.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  }).directive("modifyEquipment", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/modals/common/modify-equipment.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  }).directive("ntOrgPicker", function() {
    return {
      restrict: "EA",
      scope: true,
      //template: '<div>{{ myVal }}</div>',
      templateUrl: "./views/modals/common/nt-org-picker.html",
      //controller: "ntOrgPicker",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  }).directive("ntDivisionsPicker", function() {
    return {
      restrict: "EA",
      scope: true,
      //template: '<div>{{ myVal }}</div>',
      templateUrl: "./views/modals/common/nt-divisions-picker.html",
      //controller: "ntOrgPicker",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  }).directive("ngThumb", ["$window",
    function($window) {
      var helper = {
        support: !!($window.FileReader && $window.CanvasRenderingContext2D),
        isFile: function(item) {
          return angular.isObject(item) && item instanceof $window.File;
        },
        isImage: function(file) {
          var type = "|" + file.type.slice(file.type.lastIndexOf("/") + 1) + "|";
          return "|jpg|png|jpeg|bmp|gif|".indexOf(type) !== -1;
        }
      };
      return {
        restrict: "A",
        template: "<canvas/>",
        link: function(scope, element, attributes) {
          if (!helper.support) return;
          var params = scope.$eval(attributes.ngThumb);
          if (!helper.isFile(params.file)) return;
          if (!helper.isImage(params.file)) return;
          var canvas = element.find("canvas");
          var reader = new FileReader();
          reader.onload = onLoadFile;
          reader.readAsDataURL(params.file);

          function onLoadFile(event) {
            var img = new Image();
            img.onload = onLoadImage;
            img.src = event.target.result;
          }

          function onLoadImage() {
            var width = params.width || (this.width / this.height) * params.height;
            var height = params.height || (this.height / this.width) * params.width;
            canvas.attr({
              width: width,
              height: height
            });
            canvas[0].getContext("2d").drawImage(this, 0, 0, width, height);
          }
        }
      };
    }
  ]).directive("ntUsersPicker", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/modals/common/nt-users-picker.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  }).directive("storeCompanyModal", function() {
    return {
      restrict: "EA",
      scope: true,
      templateUrl: "./views/modals/common/store-modal.html",
      link: function($scope, element, attrs) {} //DOM manipulation
    };
  });