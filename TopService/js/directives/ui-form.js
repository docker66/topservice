﻿n580440.directive("checkBoxToArray", [function () {
    return {
        restrict: "A",
        require: "?^ngModel",
        link: function (scope, element, attrs, ctrl) {
            var value = scope.$eval(attrs.checkBoxToArray);
            ctrl.$parsers.push(function (viewValue) {
                var modelValue = ctrl.$modelValue ? angular.copy(ctrl.$modelValue) : [];
                if (viewValue === true && modelValue.indexOf(value) === -1) {
                    modelValue.push(value);
                }

                if (viewValue !== true && modelValue.indexOf(value) != -1) {
                    modelValue.splice(modelValue.indexOf(value), 1);
                }
                
                return modelValue.sort();
            });
            ctrl.$formatters.push(function (modelValue) {
                return modelValue && modelValue.indexOf(value) != -1;
            });

            ctrl.$isEmpty = function ($modelValue) {
                return !$modelValue || $modelValue.length === 0;
            };
        }
    }
}])
;