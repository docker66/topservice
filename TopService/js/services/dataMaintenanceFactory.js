n580440.factory("DataMaintenanceFactory", ["$http",
  function($http) {
    return {
      downloadExcel: function(Passport, tmpl) {
        $http({
          url: ApiMapper.ccApi + "/g/management/DownloadTmpl/" + tmpl,
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: Passport
          }
        }).success(function(data, status, headers) {
          headers = headers();
          var FileName = decodeURIComponent(headers["x-filename"]);
          var Data = "\ufeff" + data;
          var csvData = new Blob([Data], {
            type: "text/csv;charset=utf-8;"
          });
          //IE11 & Edge
          if (navigator.msSaveBlob) {
            navigator.msSaveBlob(csvData, FileName);
          } else {
            var link = document.createElement("a");
            link.href = window.URL.createObjectURL(csvData);
            link.setAttribute("download", FileName);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
          }
        }).error(function(error) {
          alert(error);
        });
      },
      // 設備列表查詢
      getEquipList: function(Passport, pageNumber, ReCaseParameterStr) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/equip/" + pageNumber,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(ReCaseParameterStr)
        });
      },
      // 导出设备csv
      getEquipListCsv: function(Passport, ReCaseParameterStr) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/readToCSV/equip/" + 0,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(ReCaseParameterStr)
        });
      },
      // 獲取設備-大中小類
      getEquipType: function(Passport) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/equip/getEquipType",
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: Passport
          }
        });
      },
      // 獲取設備-品牌
      getBrand: function(Passport) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/equip/getBrand",
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: Passport
          }
        });
      },
      // 獲取設備-型號
      getModel: function(Passport) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/equip/getModel",
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: Passport
          }
        });
      },
      // 獲取設備-摆放位置
      getEquipmentPos: function(Passport) {
        return $http({
          url: ApiMapper.sApi + "/s/equipmentpos",
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: Passport
          }
        });
      },
      // 获取公司-公司别
      getCompanyList: function(passport, apiType) {
        return $http({
          url: `${ApiMapper.ccApi}/g/${apiType}company`,
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: passport
          }
        });
      },
      // 获取公司四层架构
      getUnits: function(passport, apiType, companyId) {
        return $http({
          url: `${ApiMapper.ccApi}/g/${apiType}unit/${companyId}`,
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: passport
          }
        });
      },
      // 获取设备详情
      getDetailEquipment: function(Passport, equipmentId) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/equip/getDetailEquipment/" + equipmentId,
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: Passport
          }
        });
      },
      // 获取设备编码
      getEquipCode: function(Passport, unitId, equipmentTypeClass) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/equip/GetEquipCode/" + unitId + "/" + equipmentTypeClass,
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: Passport
          }
        });
      },
      // 新增设备
      createEquip: function(Passport, ReCaseParameterStr) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/equip/Create",
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(ReCaseParameterStr)
        });
      },
      // 获取原因及时限
      getCauseAndPriorities: function(Passport, params) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/equip/GetCauseAndPriorities",
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(params)
        });
      },
      // 更新设备资料
      updateEquip: function(Passport, ReCaseParameterStr) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/equip/update",
          method: "PATCH",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(ReCaseParameterStr)
        });
      },
      // 获取公司职称(厂商)
      getTitles: function(Passport, apiType, CompanyId) {
        return $http({
          url: `${ApiMapper.ccApi}/g/${apiType}titles/${CompanyId}`,
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: Passport
          }
        });
      },
      // 获取公司人員
      getUserList: function(Passport, apiType, CompanyId) {
        return $http({
          url: `${ApiMapper.ccApi}/g/management/${apiType}unit/User/${CompanyId}`,
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: Passport
          }
        });
      },
      // 获取原因与时限列表
      getCausePrioritieList: function(Passport, pageNumber, ReCaseParameterStr) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/CausesPriorities/" + pageNumber,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(ReCaseParameterStr)
        });
      },
      // 导出原因与时限csv
      getCausesPrioritiesCsv: function(Passport, ReCaseParameterStr) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/CausesPriorities/Export/" + 1,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(ReCaseParameterStr)
        });
      },
      // 获取设备编码
      getSubEquipCodes: function(Passport, ReCaseParameterStr) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/readCSV/CausesPriorities/Sub/EquipCodes",
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(ReCaseParameterStr)
        });
      },
      // 获取门店列表
      getUnit: function(passport, apiType, pageNumber, parameterStr) {
        return $http({
          url: `${ApiMapper.ccApi}/g/management/${tag}unit/${pageNumber}`,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: passport
          },
          data: JSON.stringify(parameterStr)
        })
      },
      // 新增报修门店
      createUnit: function(passport, apiType, parameterStr) {
        return $http({
          url: `${ApiMapper.ccApi}/g/management/${apiType}unit/create`,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: passport
          },
          data: JSON.stringify(parameterStr)
        });
      },
      // 门店明细
      getUnitDetail: function(passport, apiType, unitId) {
        return $http({
          url: `${ApiMapper.ccApi}/g/management/${apiType}unit/detail/${unitId}`,
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: passport
          }
        });
      },
      // 更新门店
      updateUnit: function(passport, apiType, parameterStr) {
        return $http({
          url: `${ApiMapper.ccApi}/g/management/${apiType}unit/Update`,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: passport
          },
          data: JSON.stringify(parameterStr)
        });
      },
      // 导出门店csv
      getUnitCsv: {
        s: function(Passport, ReCaseParameterStr, tag) {
          return $http({
            url: `${ApiMapper.ccApi}/g/management/sunit/Export/2`,
            method: "POST",
            contentType: "application/json",
            headers: {
              Passport: Passport
            },
            data: JSON.stringify(ReCaseParameterStr)
          });
        },
        p: function(Passport, ReCaseParameterStr) {
          return $http({
            url: `${ApiMapper.ccApi}/g/management/punit/Export/4`,
            method: "POST",
            contentType: "application/json",
            headers: {
              Passport: Passport
            },
            data: JSON.stringify(ReCaseParameterStr)
          });
        }
      },
      // 查询单位人员列表
      getUsers: function(passport, apiType, pageNumber, parameterStr) {
        return $http({
          url: `${ApiMapper.ccApi}/g/management/${apiType}user/${pageNumber}`,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: passport
          },
          data: JSON.stringify(parameterStr)
        });
      },
      // 人员明细
      getUserDetail: function(passport, apiType, userid) {
        return $http({
          url: `${ApiMapper.ccApi}/g/management/${apiType}user/detail/${userid}`,
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: passport
          }
        });
      },
      // 新增人员
      createUser: function(passport, apiType, parameterStr) {
        return $http({
          url: `${ApiMapper.ccApi}/g/management/${apiType}user/Create`,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: passport
          },
          data: JSON.stringify(parameterStr)
        });
      },
      //修改人员
      updateUser: function(passport, apiType, parameterStr) {
        return $http({
          url: `${ApiMapper.ccApi}/g/management/${apiType}user/Update`,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: passport
          },
          data: JSON.stringify(parameterStr)
        });
      },
      // 导出人員csv
      getUserCsv: function(passport, apiType, parameterStr) {
        return $http({
          url: `${ApiMapper.ccApi}/g/management/${apiType}user/Export/${apiType=='s'?3:5}`,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: passport
          },
          data: JSON.stringify(parameterStr)
        });
      },
      // 获取據點列表
      getUnit: function(passport, apiType, pageNumber, parameterStr) {
        return $http({
          url: `${ApiMapper.ccApi}/g/management/${apiType}unit/${pageNumber}`,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: passport
          },
          data: JSON.stringify(parameterStr)
        });
      },
      // 新增維修據點
      createPUnit: function(Passport, ReCaseParameterStr) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/punit/Create",
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(ReCaseParameterStr)
        });
      },
      // 更新据点明细
      updatePUnit: function(Passport, ReCaseParameterStr) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/punit/Update",
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(ReCaseParameterStr)
        });
      },
      // 查询维修单位人员
      getPUser: function(Passport, pageNumber, ReCaseParameterStr) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/puser/" + pageNumber,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(ReCaseParameterStr)
        });
      },
      // 维修门店人员明细
      getPUserDetail: function(Passport, userId) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/puser/detail/" + userId,
          method: "Get",
          contentType: "application/json",
          headers: {
            Passport: Passport
          }
        });
      },
      // 新增维修人员
      createPUser: function(Passport, ReCaseParameterStr) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/puser/Create",
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(ReCaseParameterStr)
        });
      },
      // 更新维修人员
      updatePUser: function(Passport, ReCaseParameterStr) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/puser/Update",
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(ReCaseParameterStr)
        });
      },
      // 导出维修人员
      getPUserCsv: function(Passport, ReCaseParameterStr) {
        return $http({
          url: ApiMapper.ccApi + "/g/management/puser/Export/" + 5,
          method: "POST",
          contentType: "application/json",
          headers: {
            Passport: Passport
          },
          data: JSON.stringify(ReCaseParameterStr)
        });
      }
    };
  }
]);