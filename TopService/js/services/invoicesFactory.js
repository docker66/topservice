n580440
.factory('Invoices',['$http','$q',function($http,$q){
    var service = {};
    //开票通知列表
    service.getLists = function(Passport,dateParameters,Parameters,currentPage){
        //makeUrl();
        var deferred = $q.defer();
        $http({
            url: ApiMapper.ApiUrl + '/'+dateParameters.YY+'/'+dateParameters.MM+'/invoices/' + currentPage,
            method: 'POST',
            contentType: 'application/json',
            headers: { 'Passport':Passport },
            data: Parameters
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data.Message);
        });
        return deferred.promise;
    };
    //开票通知详情
    service.getDetails = function(Passport,dateParameters,Parameters,currentPage){
        //makeUrl();
        var deferred = $q.defer();
        $http({
            url: ApiMapper.ApiUrl + '/'+dateParameters.YY+'/'+dateParameters.MM+'/invoiceDetails/' + currentPage,
            method: 'POST',
            contentType: 'application/json',
            headers: { 'Passport':Passport },
            data: Parameters
        }).success(function (data) {           
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data.Message);
        });
        return deferred.promise;
    };
    return service;
}])
;