n580440
  .filter('i18n', ['$translate', function ($translate) {
    return function (key) {
      if (key) {
        return $translate.instant(key);
      }
    }
  }])
  .factory('i18n', ['$translate', function ($translate) {
    var T = {
      T: function (key) {
        if (key) {
          return $translate.instant(key);
        }
        return key;
      }
    }
    return T;
  }]);
